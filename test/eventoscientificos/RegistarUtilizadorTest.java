/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sofia
 */
public class RegistarUtilizadorTest {
    
    public RegistarUtilizadorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of novoUtilizador method, of class RegistarUtilizador.
     */
    @Test (expected = NullPointerException.class)
    public void testNovoUtilizador() {
        System.out.println("novoUtilizador");
        RegistarUtilizador instance = new RegistarUtilizador();
        Utilizador expResult = instance.novoUtilizador();
        Utilizador result = instance.novoUtilizador();
        assertEquals(expResult, result);
    }

    /**
     * Test of registaUtilizador method, of class RegistarUtilizador.
     */
    @Test (expected = NullPointerException.class)
    public void testRegistaUtilizador() {
        System.out.println("registaUtilizador");
        Utilizador u = new Utilizador();
        RegistarUtilizador instance = new RegistarUtilizador();
        boolean expResult = false;
        boolean result = instance.registaUtilizador(u);
        assertEquals(expResult, result);
    }

}
