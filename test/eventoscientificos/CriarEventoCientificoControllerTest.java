/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import eventoscientificos.CriarEventoCientificoController;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sofia
 */
public class CriarEventoCientificoControllerTest {
    
    public CriarEventoCientificoControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of novoEvento method, of class CriarEventoCientificoController.
     */
    @Test
    public void testNovoEvento() {
        System.out.println("novoEvento");
        CriarEventoCientificoController instance = new CriarEventoCientificoController (new Empresa());
        instance.novoEvento();
    }

    /**
     * Test of addOrganizador method, of class CriarEventoCientificoController.
     */
    @Test
    public void testAddOrganizador() {
        System.out.println("addOrganizador");
        String strId = "";
        CriarEventoCientificoController instance = new CriarEventoCientificoController (new Empresa());
        boolean expResult = instance.addOrganizador(strId);
        boolean result = instance.addOrganizador(strId);
        assertEquals(expResult, result);

    }

    /**
     * Test of registaEvento method, of class CriarEventoCientificoController.
     */
    @Test
    public void testRegistaEvento() {
        System.out.println("registaEvento");
        Empresa empresa = new Empresa();
        CriarEventoCientificoController instance = new CriarEventoCientificoController (empresa);
        instance.novoEvento();
        boolean expResult = true;
        boolean result = instance.registaEvento();
        assertEquals(expResult, result);

    }
    
}
