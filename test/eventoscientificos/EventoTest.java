/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1130429, Rita
 */
public class EventoTest {
    
    public EventoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSetTitulo() {
        System.out.println("setTitulo");
        String strTitulo = "Titulo Teste";
        Evento instance = new Evento();
        instance.setTitulo(strTitulo);

    }

    @Test
    public void testSetDescricao() {
        System.out.println("setDescricao");
        String strDescricao = "Descricao Teste";
        Evento instance = new Evento();
        instance.setDescricao(strDescricao);

    }

    @Test
    public void testSetDataInicio() {
        System.out.println("setDataInicio");
        String strDataInicio = "Data Teste";
        Evento instance = new Evento();
        instance.setDataInicio(strDataInicio);

    }

    @Test
    public void testSetDataFim() {
        System.out.println("setDataFim");
        String strDataFim = "Data Teste";
        Evento instance = new Evento();
        instance.setDataFim(strDataFim);

    }

    @Test
    public void testSetDataLimiteSubmissão() {
        System.out.println("setDataLimiteSubmissao");
        String strDataLimiteSubmissao = "Data Teste";
        Evento instance = new Evento();
        instance.setDataLimiteSubmissao(strDataLimiteSubmissao);

    }

    @Test
    public void testSetLocal() {
        System.out.println("setLocal");
        String strLocal = "Local Teste";
        Evento instance = new Evento();
        instance.setLocal(strLocal);
    }

    @Test 
    public void testGetListaOrganizadores() {
        System.out.println("getListaOrganizadores");
        Evento instance = new Evento();
        Utilizador u = new Utilizador();
        Organizador o = new Organizador("ID Teste", u);
        instance.addOrganizador(o);
        List<Organizador> lo = new ArrayList<Organizador>();
        lo.add(o);
        List<Organizador> expResult = lo;
        List<Organizador> result = instance.getListaOrganizadores();
        assertEquals(expResult, result);

    }

    @Test
    public void testAddOrganizador() {
        System.out.println("addOrganizador");
        String strId = "ID Teste";
        Utilizador u = new Utilizador();
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.addOrganizador(strId, u);
        assertEquals(expResult, result);

    }

    @Test
    public void testValida() {
        System.out.println("valida");
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);

    }

    @Test
    public void testSetCP() {
        System.out.println("setCP");
        CP cp = new CP();
        Evento instance = new Evento();
        instance.setCP(cp);

    }

    @Test
    public void testToString() {
        System.out.println("toString");
        Evento instance = new Evento();
        instance.setTitulo("Titulo Teste");
        String expResult = "Titulo Teste";
        String result = instance.toString();
        assertEquals(expResult, result);

    }

    @Test
    public void testAceitaSubmissoes() {
        System.out.println("aceitaSubmissoes");
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.aceitaSubmissoes();
        assertEquals(expResult, result);

    }

    @Test
    public void testNovaSubmissao() {
        System.out.println("novaSubmissao");
        Evento instance = new Evento();
        Submissao sub = instance.novaSubmissao();
        Submissao expResult = sub;
        Submissao result = sub;
        assertEquals(expResult, result);

    }

    @Test
    public void testAddSubmissao() {
        System.out.println("addSubmissao");
        Submissao submissao = new Submissao();
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.addSubmissao(submissao);
        assertEquals(expResult, result);

    }

    @Test
    public void testGetListaSubmissoes() {
        System.out.println("getListaSubmissoes");
        Evento instance = new Evento();
        List<Submissao> sub = new ArrayList<Submissao>();
        Submissao sub1 = new Submissao();
        sub.add(sub1);
        instance.addSubmissao(sub1);
        List<Submissao> expResult = sub;
        List<Submissao> result = instance.getListaSubmissoes();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetCP() {
        System.out.println("getCP");
        Evento instance = new Evento();
        CP cp = new CP();
        instance.setCP(cp);
        CP expResult = cp;
        CP result = instance.getCP();
        assertEquals(expResult, result);

    }

    @Test
    public void testGetTopicos() {
        System.out.println("getTopicos");
        Evento instance = new Evento();
        Topico t = new Topico ();
        List<Topico> top = new ArrayList<Topico>();
        top.add(t);
        instance.addTopico(t);
        List<Topico> expResult = top;
        List<Topico> result = instance.getTopicos();
        assertEquals(expResult, result);

    }

    @Test
    public void testNovoTopico() {
        System.out.println("novoTopico");
        Evento instance = new Evento();
        Topico t = instance.novoTopico();
        Topico expResult = t;
        Topico result = t;
        assertEquals(expResult, result);

    }

    @Test
    public void testAddTopico() {
        System.out.println("addTopico");
        Topico t = new Topico();
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.addTopico(t);
        assertEquals(expResult, result);

    }

    @Test
    public void testValidaTopico() {
        System.out.println("validaTopico");
        Topico t = new Topico();
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.validaTopico(t);
        assertEquals(expResult, result);

    }

    @Test
    public void testSetCriado() {
        System.out.println("SetCriado");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.SetCriado();
        assertEquals(expResult, result);

    }

    @Test
    public void testSetRegistado() {
        System.out.println("SetRegistado");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.SetRegistado();
        assertEquals(expResult, result);

    }

    @Test
    public void testSetSubmissao() {
        System.out.println("setSubmissao");
        Submissao s = new Submissao();
        Evento instance = new Evento();
        instance.setSubmissao(s);

    }

    @Test
    public void testNovaCP() {
        System.out.println("novaCP");
        Evento instance = new Evento();
        CP cp = instance.novaCP();
        CP expResult = cp;
        CP result = cp;
        assertEquals(expResult, result);

    }

    @Test
    public void testSetDataLimiteSubmissao() {
        System.out.println("setDataLimiteSubmissao");
        String strDataLimiteSubmissao = "Data Teste";
        Evento instance = new Evento();
        instance.setDataLimiteSubmissao(strDataLimiteSubmissao);

    }

    @Test
    public void testAddOrganizador_String_Utilizador() {
        System.out.println("addOrganizador");
        String strId = "ID Teste";
        Utilizador u = new Utilizador();
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.addOrganizador(strId, u);
        assertEquals(expResult, result);

    }

    @Test
    public void testAddOrganizador_Organizador() {
        System.out.println("addOrganizador");
        Utilizador u = new Utilizador();
        Organizador o = new Organizador("ID Teste", u);
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.addOrganizador(o);
        assertEquals(expResult, result);

    }

   

    /**
     * Test of validaDatas method, of class Evento.
     */
    @Test
    public void testValidaDatas() {
        System.out.println("validaDatas");
        String m_strDataInicio = "22/5/2000";
        String m_strDataFim = "22/5/2000";
        String dataRevisao = "20/5/2000";
        String dataLimiteSubmissaoFinal = "10/5/2000";
        String dataLimiteRegisto = "7/5/2000";
        String m_strDataLimiteSubmissao = "5/4/2000";
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.validaDatas(m_strDataInicio, m_strDataFim, dataRevisao, dataLimiteSubmissaoFinal, dataLimiteRegisto, m_strDataLimiteSubmissao);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of isAnoBissexto method, of class Evento.
     */
    @Test
    public void testIsAnoBissexto() {
        System.out.println("isAnoBissexto");
        int ano = 0;
        boolean expResult = true;
        boolean result = Evento.isAnoBissexto(ano);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getArtigos method, of class Evento.
     */
    @Test
    public void testGetArtigos() {
        System.out.println("getArtigos");
        Evento instance = new Evento();
        instance.getArtigos();
        
    }

    /**
     * Test of setNrMaxTopicos method, of class Evento.
     */
    @Test
    public void testSetNrMaxTopicos() {
        System.out.println("setNrMaxTopicos");
        int nrMaxTopicos = 0;
        Evento instance = new Evento();
        instance.setNrMaxTopicos(nrMaxTopicos);
        
    }

    /**
     * Test of setDataRevisao method, of class Evento.
     */
    @Test
    public void testSetDataRevisao() {
        System.out.println("setDataRevisao");
        String dataRevisao = "";
        Evento instance = new Evento();
        instance.setDataRevisao(dataRevisao);
        
    }

    /**
     * Test of setDataLimiteSubmissaoFinal method, of class Evento.
     */
    @Test
    public void testSetDataLimiteSubmissaoFinal() {
        System.out.println("setDataLimiteSubmissaoFinal");
        String dataLimiteSubmissaoFinal = "";
        Evento instance = new Evento();
        instance.setDataLimiteSubmissaoFinal(dataLimiteSubmissaoFinal);
        
    }

    /**
     * Test of setDataLimiteRegisto method, of class Evento.
     */
    @Test
    public void testSetDataLimiteRegisto() {
        System.out.println("setDataLimiteRegisto");
        String dataLimiteRegisto = "";
        Evento instance = new Evento();
        instance.setDataLimiteRegisto(dataLimiteRegisto);
        
    }

    /**
     * Test of getListaRevisores method, of class Evento.
     */
    @Test (expected=NullPointerException.class)
    public void testGetListaRevisores() {
        System.out.println("getListaRevisores");
        Evento instance = new Evento();
        List<Revisor> expResult = instance.getListaRevisores();
        List<Revisor> result = instance.getListaRevisores();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getListaArtigosRevisor method, of class Evento.
     */
    @Test(expected=NullPointerException.class)
    public void testGetListaArtigosRevisor() {
        System.out.println("getListaArtigosRevisor");
        Utilizador u = new Utilizador();
        Evento instance = new Evento();
        List<Artigo> expResult = instance.getListaArtigosRevisor(u);
        List<Artigo> result = instance.getListaArtigosRevisor(u);
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setCameraReady method, of class Evento.
     */
    @Test
    public void testSetCameraReady() {
        System.out.println("setCameraReady");
        Evento instance = new Evento();
        boolean expResult = false;
        boolean result = instance.setCameraReady();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setState method, of class Evento.
     */
    @Test
    public void testSetState() {
        System.out.println("setState");
        
        Evento instance = new Evento();
        EventoState estado= instance.getState();
        instance.setState(estado);
        
    }

    /**
     * Test of getListaAutores method, of class Evento.
     */
    @Test(expected=NullPointerException.class)
    public void testGetListaAutores() {
        System.out.println("getListaAutores");
        Utilizador u = null;
        Evento instance = new Evento();
        List<Autor> expResult = instance.getListaAutores(u);
        List<Autor> result = instance.getListaAutores(u);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setIDEvento method, of class Evento.
     */
    @Test
    public void testSetIDEvento() {
        System.out.println("setIDEvento");
        String idEvento = "";
        Evento instance = new Evento();
        instance.setIDEvento(idEvento);
        
    }

    /**
     * Test of getIDEvento method, of class Evento.
     */
    @Test
    public void testGetIDEvento() {
        System.out.println("getIDEvento");
        Evento instance = new Evento();
        String expResult = instance.getIDEvento();
        String result = instance.getIDEvento();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setValorFull method, of class Evento.
     */
    @Test
    public void testSetValorFull() {
        System.out.println("setValorFull");
        String F = "";
        Evento instance = new Evento();
        instance.setValorFull(F);
        
    }

    /**
     * Test of setValorShort method, of class Evento.
     */
    @Test
    public void testSetValorShort() {
        System.out.println("setValorShort");
        String S = "";
        Evento instance = new Evento();
        instance.setValorShort(S);
    }

    /**
     * Test of setValorPoster method, of class Evento.
     */
    @Test
    public void testSetValorPoster() {
        System.out.println("setValorPoster");
        String P = "";
        Evento instance = new Evento();
        instance.setValorPoster(P);
        
    }

    /**
     * Test of setListaArtigos method, of class Evento.
     */
    @Test(expected=NullPointerException.class)
    public void testSetListaArtigos() {
        System.out.println("setListaArtigos");
        List<Artigo> listaArtigos = null;
        Evento instance = new Evento();
        instance.setListaArtigos(listaArtigos);
        
    }

    /**
     * Test of setHost method, of class Evento.
     */
    @Test
    public void testSetHost() {
        System.out.println("setHost");
        String Host = "";
        Evento instance = new Evento();
        instance.setHost(Host);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getHost method, of class Evento.
     */
    @Test
    public void testGetHost() {
        System.out.println("getHost");
        Evento instance = new Evento();
        String expResult = "";
        String result = instance.getHost();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setCidade method, of class Evento.
     */
    @Test
    public void testSetCidade() {
        System.out.println("setCidade");
        String Cidade = "";
        Evento instance = new Evento();
        instance.setCidade(Cidade);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCidade method, of class Evento.
     */
    @Test
    public void testGetCidade() {
        System.out.println("getCidade");
        Evento instance = new Evento();
        String expResult = "";
        String result = instance.getCidade();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setPais method, of class Evento.
     */
    @Test
    public void testSetPais() {
        System.out.println("setPais");
        String Pais = "";
        Evento instance = new Evento();
        instance.setPais(Pais);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPais method, of class Evento.
     */
    @Test
    public void testGetPais() {
        System.out.println("getPais");
        Evento instance = new Evento();
        String expResult = "";
        String result = instance.getPais();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setWebsite method, of class Evento.
     */
    @Test
    public void testSetWebsite() {
        System.out.println("setWebsite");
        String Website = "";
        Evento instance = new Evento();
        instance.setWebsite(Website);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getWebsite method, of class Evento.
     */
    @Test
    public void testGetWebsite() {
        System.out.println("getWebsite");
        Evento instance = new Evento();
        String expResult = "";
        String result = instance.getWebsite();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setEmail method, of class Evento.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        String Email = "";
        Evento instance = new Evento();
        instance.setEmail(Email);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEmail method, of class Evento.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        Evento instance = new Evento();
        String expResult = "";
        String result = instance.getEmail();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDataLimiteRegistoAutor method, of class Evento.
     */
    @Test
    public void testSetDataLimiteRegistoAutor() {
        System.out.println("setDataLimiteRegistoAutor");
        String strDataLimiteRegistoAutor = "";
        Evento instance = new Evento();
        instance.setDataLimiteRegistoAutor(strDataLimiteRegistoAutor);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDataLimiteDataRegistoAutor method, of class Evento.
     */
    @Test
    public void testGetDataLimiteDataRegistoAutor() {
        System.out.println("getDataLimiteDataRegistoAutor");
        Evento instance = new Evento();
        String expResult = "";
        String result = instance.getDataLimiteDataRegistoAutor();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDataLimiteRevisao method, of class Evento.
     */
    @Test
    public void testSetDataLimiteRevisao() {
        System.out.println("setDataLimiteRevisao");
        String strDataLimiteRevisao = "";
        Evento instance = new Evento();
        instance.setDataLimiteRevisao(strDataLimiteRevisao);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDataLimiteDataRevisao method, of class Evento.
     */
    @Test
    public void testGetDataLimiteDataRevisao() {
        System.out.println("getDataLimiteDataRevisao");
        Evento instance = new Evento();
        String expResult = "";
        String result = instance.getDataLimiteDataRevisao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDuracao method, of class Evento.
     */
    @Test
    public void testSetDuracao() {
        System.out.println("setDuracao");
        String Duracao = "";
        Evento instance = new Evento();
        instance.setDuracao(Duracao);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDuracao method, of class Evento.
     */
    @Test
    public void testGetDuracao() {
        System.out.println("getDuracao");
        Evento instance = new Evento();
        String expResult = "";
        String result = instance.getDuracao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setOrganizador method, of class Evento.
     */
    @Test
    public void testSetOrganizador() {
        System.out.println("setOrganizador");
        String Organizador = "";
        Evento instance = new Evento();
        instance.setOrganizador(Organizador);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getOrganizador method, of class Evento.
     */
    @Test
    public void testGetOrganizador() {
        System.out.println("getOrganizador");
        Evento instance = new Evento();
        String expResult = "";
        String result = instance.getOrganizador();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getState method, of class Evento.
     */
    @Test
    public void testGetState() {
        System.out.println("getState");
        Evento instance = new Evento();
        EventoState expResult = null;
        EventoState result = instance.getState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getArtigoID method, of class Evento.
     */
    @Test
    public void testGetArtigoID() {
        System.out.println("getArtigoID");
        String strId = "";
        Evento instance = new Evento();
        Artigo expResult = null;
        Artigo result = instance.getArtigoID(strId);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getValorFull method, of class Evento.
     */
    @Test
    public void testGetValorFull() {
        System.out.println("getValorFull");
        Evento instance = new Evento();
        String expResult = instance.getValorFull();
        String result = instance.getValorFull();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getValorShort method, of class Evento.
     */
    @Test
    public void testGetValorShort() {
        System.out.println("getValorShort");
        Evento instance = new Evento();
        String expResult = instance.getValorShort();
        String result = instance.getValorShort();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getValorPoster method, of class Evento.
     */
    @Test
    public void testGetValorPoster() {
        System.out.println("getValorPoster");
        Evento instance = new Evento();
        String expResult = instance.getValorPoster();
        String result = instance.getValorPoster();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setFormula method, of class Evento.
     */
    @Test
    public void testSetFormula() {
        System.out.println("setFormula");
        String F = "1";
        Evento instance = new Evento();
        instance.setFormula(F);
       
    }

    /**
     * Test of getFormula method, of class Evento.
     */
    @Test
    public void testGetFormula() {
        System.out.println("getFormula");
        Evento instance = new Evento();
        int expResult =instance.getFormula();
        int result = instance.getFormula();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getListaArtigosAutor method, of class Evento.
     */
    @Test (expected=NullPointerException.class)
    public void testGetListaArtigosAutor() {
        System.out.println("getListaArtigosAutor");
        Utilizador u = new Utilizador();
        Evento instance = new Evento();
        List<Artigo> expResult =instance.getListaArtigosAutor(u);
        List<Artigo> result = instance.getListaArtigosAutor(u);
        assertEquals(expResult, result);
    }
}
