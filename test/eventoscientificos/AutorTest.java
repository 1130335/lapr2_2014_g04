/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rita
 */
public class AutorTest {
    
    public AutorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setNome method, of class Autor.
     */
    @Test
    public void testSetNome() {
        System.out.println("setNome");
        String strNome = "Nome Teste";
        Autor instance = new Autor();
        instance.setNome(strNome);

    }

    /**
     * Test of setAfiliacao method, of class Autor.
     */
    @Test
    public void testSetAfiliacao() {
        System.out.println("setAfiliacao");
        String strAfiliacao = "Afiliacao Teste";
        Autor instance = new Autor();
        instance.setAfiliacao(strAfiliacao);

    }

    /**
     * Test of setEMail method, of class Autor.
     */
    @Test
    public void testSetEMail() {
        System.out.println("setEMail");
        String strEMail = "emailteste@isep.ipp.pt";
        Autor instance = new Autor();
        instance.setEMail(strEMail);

    }

    /**
     * Test of setUtilizador method, of class Autor.
     */
    @Test
    public void testSetUtilizador() {
        System.out.println("setUtilizador");
        Utilizador utilizador = new Utilizador();
        Autor instance = new Autor();
        instance.setUtilizador(utilizador);

    }

    /**
     * Test of valida method, of class Autor.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Autor instance = new Autor();
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);

    }

    /**
     * Test of podeSerCorrespondente method, of class Autor.
     */
    @Test
    public void testPodeSerCorrespondente() {
        System.out.println("podeSerCorrespondente");
        Autor instance = new Autor();
        Utilizador u = new Utilizador("usernameteste@isep.ipp.pt","passwordteste","Nome Teste","emailteste@isep.ipp.pt","passwordteste");
        instance.setUtilizador(u);
        boolean expResult = true;
        boolean result = instance.podeSerCorrespondente();
        assertEquals(expResult, result);

    }

    /**
     * Test of toString method, of class Autor.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Autor instance = new Autor();
        instance.setAfiliacao("Afiliacao Teste");
        instance.setEMail("emailteste@isep.ipp.pt");
        instance.setNome("Nome Teste");
        String expResult = "Nome Teste - Afiliacao Teste - emailteste@isep.ipp.pt";
        String result = instance.toString();
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class Autor.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Autor obj = new Autor();
        obj.setEMail("emailteste@isep.ipp.pt");
        Autor instance = new Autor();
        instance.setEMail("emailteste@isep.ipp.pt");
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);

    }

    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Autor instance = new Autor();
        String expResult = "Nome Teste";
        instance.setNome("Nome Teste");
        String result = instance.getNome();
        assertEquals(expResult, result);

    }
}
