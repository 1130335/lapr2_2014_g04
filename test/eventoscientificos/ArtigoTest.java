/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author amartins
 */
public class ArtigoTest {

    public ArtigoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setTitulo method, of class Artigo.
     */
    @Test
    public void testSetTitulo() {
        System.out.println("setTitulo");
        String strTitulo = "Artigo Teste";
        Artigo instance = new Artigo();
        instance.setTitulo(strTitulo);
        assertEquals(strTitulo, instance.getTitulo());
    }

    /**
     * Test of setResumo method, of class Artigo.
     */
    @Test
    public void testSetResumo() {
        System.out.println("setResumo");
        String strResumo = "Resumo Teste";
        Artigo instance = new Artigo();
        instance.setResumo(strResumo);

    }

    /**
     * Test of novoAutor method, of class Artigo.
     */
    @Test
    public void testNovoAutorString() {
        System.out.println("novoAutor");
        String strNome = "Nome Teste";
        String strAfiliacao = "Afiliacao Teste";
        Artigo instance = new Artigo();
        Artigo instance1 = new Artigo();
        String expResult = instance1.novoAutor(strNome, strAfiliacao).getNome();
        String result = instance.novoAutor(strNome, strAfiliacao).getNome();
        assertEquals(expResult, result);

    }

    /**
     * Test of novoAutor method, of class Artigo.
     */
    @Test
    public void testNovoAutor_4args() {
        System.out.println("novoAutor");
        String strNome = "Nome Teste";
        String strAfiliacao = "Afiliacao Teste";
        String strEmail = "Email Teste";
        Utilizador u1 = new Utilizador();
        Utilizador utilizador = u1;
        Artigo instance = new Artigo();
        Artigo instance1 = new Artigo();
        Autor expResult = instance1.novoAutor(strNome, strAfiliacao, strEmail, utilizador);
        Autor result = instance.novoAutor(strNome, strAfiliacao, strEmail, utilizador);
        assertEquals(expResult, result);

    }

    /**
     * Test of addAutor method, of class Artigo.
     */
    @Test
    public void testAddAutor() {
        System.out.println("addAutor");
        Autor a1 = new Autor();
        Autor autor = a1;
        Artigo instance = new Artigo();
        boolean expResult = true;
        boolean result = instance.addAutor(autor);
        assertEquals(expResult, result);

    }

    /**
     * Test of getPossiveisAutoresCorrespondentes method, of class Artigo.
     */
    @Test
    public void testGetPossiveisAutoresCorrespondentes() {
        System.out.println("getPossiveisAutoresCorrespondentes");
        Artigo instance = new Artigo();
        Artigo instance1 = new Artigo();
        Autor a1 = new Autor();
        Autor a2 = new Autor();
        instance1.addAutor(a1);
        instance1.addAutor(a2);
        instance.addAutor(a1);
        instance.addAutor(a2);
        List<Autor> expResult = instance1.getPossiveisAutoresCorrespondentes();
        List<Autor> result = instance.getPossiveisAutoresCorrespondentes();
        assertEquals(expResult, result);

    }

    /**
     * Test of setAutorCorrespondente method, of class Artigo.
     */
    @Test
    public void testSetAutorCorrespondente() {
        System.out.println("setAutorCorrespondente");
        Autor autor = new Autor();
        Artigo instance = new Artigo();
        instance.setAutorCorrespondente(autor);

    }

    /**
     * Test of setFicheiro method, of class Artigo.
     */
    @Test
    public void testSetFicheiro() {
        System.out.println("setFicheiro");
        String strFicheiro = "Ficheiro Teste";
        Artigo instance = new Artigo();
        instance.setFicheiro(strFicheiro);

    }

    /**
     * Test of setListaTopicos method, of class Artigo.
     */
    @Test
    public void testSetListaTopicos() {
        System.out.println("setListaTopicos");
        Topico t1 = new Topico();
        Topico t2 = new Topico();
        List<Topico> listaTopicos = new ArrayList<Topico>();
        listaTopicos.add(t1);
        listaTopicos.add(t2);
        Artigo instance = new Artigo();
        instance.setListaTopicos(listaTopicos);

    }

    /**
     * Test of getInfo method, of class Artigo.
     */
    @Test
    public void testGetInfo() {
        System.out.println("getInfo");
        Artigo instance = new Artigo();
        instance.setTitulo("Titulo Teste");
        String expResult = "Titulo Teste";
        String result = instance.getInfo();
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class Artigo.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Artigo instance = new Artigo();
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);

    }

    /**
     * Test of toString method, of class Artigo.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Artigo instance = new Artigo();
        instance.setTitulo("Titulo Teste");
        String expResult = "Titulo Teste";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Artigo.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Utilizador myUt = new Utilizador();
        myUt.setUsername("expert@isep.ipp.pt");
        myUt.setEmail("expert@isep.ipp.pt");
        Autor act = new Autor();
        act.setUtilizador(myUt);

        Artigo obj = new Artigo();
        obj.setTitulo("A beleza do JUnit");
        obj.setAutorCorrespondente(act);
        Artigo instance = new Artigo();
        instance.setTitulo("A beleza do JUnit");
        instance.setAutorCorrespondente(act);

        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of getTitulo method, of class Artigo.
     */
    @Test
    public void testGetTitulo() {
        System.out.println("getTitulo");
        Artigo instance = new Artigo();
        instance.setTitulo("Teste Titulo");
        String expResult = "Teste Titulo";
        String result = instance.getTitulo();
        assertEquals(expResult, result);
    }

    /**
     * Test of getResumo method, of class Artigo.
     */
    @Test
    public void testGetResumo() {
        System.out.println("getResumo");
        Artigo instance = new Artigo();
        instance.setResumo("Resumo Teste");
        String expResult = "Resumo Teste";
        String result = instance.getResumo();
        assertEquals(expResult, result);

    }

    /**
     * Test of getListaRevisores method, of class Artigo.
     */
    @Test
    public void testGetListaRevisores() {
        System.out.println("getListaRevisores");
        Artigo instance = new Artigo();
        List<Revisor> expResult = null;
        List<Revisor> result = instance.getListaRevisores();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setListaRevisores method, of class Artigo.
     */
    @Test
    public void testSetListaRevisores() {
        System.out.println("setListaRevisores");
        List<Revisor> listaRevisores = null;
        Artigo instance = new Artigo();
        instance.setListaRevisores(listaRevisores);
       
    }
    

    @Test
    public void testSetTipo() {
        System.out.println("setTipo");
        String m_strTipo = "Tipo Teste";
        Artigo instance = new Artigo();
        instance.setTipo(m_strTipo);

    }

    @Test
    public void testGetTipo() {
        System.out.println("getTipo");
        Artigo instance = new Artigo();
        instance.setTipo("Tipo Teste");
        String expResult = "Tipo Teste";
        String result = instance.getTipo();
        assertEquals(expResult, result);

    }

  
    @Test
    public void testAddTopico() {
        System.out.println("addTopico");
        Topico topico = new Topico();
        Artigo instance = new Artigo();
        boolean expResult = true;
        boolean result = instance.addTopico(topico);
        assertEquals(expResult, result);
    }
}
