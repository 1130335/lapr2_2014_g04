/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rita
 */
public class SubmissaoTest {

    public SubmissaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testNovoArtigo() {
        System.out.println("novoArtigo");
        Submissao instance = new Submissao();
        Artigo a = instance.novoArtigo();
        Artigo expResult = a;
        Artigo result = a;
        assertEquals(expResult, result);

    }

    @Test
    public void testGetInfo() {
        System.out.println("getInfo");
        Submissao instance = new Submissao();
        String info = instance.toString();
        String expResult = info;
        String result = instance.getInfo();
        assertEquals(expResult, result);

    }

    @Test
    public void testSetArtigo() {
        System.out.println("setArtigo");
        Artigo artigo = new Artigo();
        Submissao instance = new Submissao();
        instance.setArtigo(artigo);

    }

    @Test
    public void testGetArtigo() {
        System.out.println("getArtigo");
        Submissao instance = new Submissao();
        Artigo artigo = new Artigo();
        instance.setArtigo(artigo);
        Artigo expResult = artigo;
        Artigo result = instance.getArtigo();
        assertEquals(expResult, result);

    }

    @Test
    public void testValida() {
        System.out.println("valida");
        Submissao instance = new Submissao();
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);

    }

    @Test
    public void testToString() {
        System.out.println("toString");
        Submissao instance = new Submissao();
        String expResult = instance.getInfo();
        String result = instance.toString();
        assertEquals(expResult, result);

    }

}
