/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import eventoscientificos.controller.DecidirSobreArtigoController;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rita
 */
public class DecidirSobreArtigoControllerTest {

    public DecidirSobreArtigoControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testNovaDecisao() {
        System.out.println("novaDecisao");
        String orgId = "orgID Teste";
        Empresa empresa = new Empresa();
        DecidirSobreArtigoController instance = new DecidirSobreArtigoController(empresa);
        List<Evento> expResult = new ArrayList<Evento>();
        List<Evento> result = instance.novaDecisao(orgId);
        assertEquals(expResult, result);

    }

    @Test
    public void testSelectEvento() {
        System.out.println("selectEvento");
        Evento e = new Evento();
        Empresa empresa = new Empresa();
        DecidirSobreArtigoController instance = new DecidirSobreArtigoController(empresa);
        instance.selectEvento(e);

    }

    @Test
    public void testGetSubmissoesEvento() {
        System.out.println("getSubmissoesEvento");
        Evento e = new Evento();
        Empresa empresa = new Empresa();
        DecidirSobreArtigoController instance = new DecidirSobreArtigoController(empresa);
        instance.selectEvento(e);
        List<Submissao> expResult = new ArrayList<Submissao>();
        List<Submissao> result = instance.getSubmissoesEvento();
        assertEquals(expResult, result);

    }

    @Test
    public void testSetSubmissao() {
        System.out.println("setSubmissao");
        Submissao s = new Submissao();
        Evento e = new Evento();
        Empresa empresa = new Empresa();
        DecidirSobreArtigoController instance = new DecidirSobreArtigoController(empresa);
        instance.selectEvento(e);
        instance.setSubmissao(s);
    }

    @Test
    public void testGetArtigoSubmissao() {
        System.out.println("getArtigoSubmissao");
        Evento e = new Evento();
        Submissao s = new Submissao();
        Empresa empresa = new Empresa();
        DecidirSobreArtigoController instance = new DecidirSobreArtigoController(empresa);
        instance.selectEvento(e);
        instance.setSubmissao(s);
        instance.getArtigoSubmissao();

    }

    @Test
    public void testGetMecanismosDecisao() {
        System.out.println("getMecanismosDecisao");
        Empresa empresa = new Empresa();
        DecidirSobreArtigoController instance = new DecidirSobreArtigoController(empresa);
        List<MecanismosDecisao> expResult = instance.getMecanismosDecisao();
        List<MecanismosDecisao> result = instance.getMecanismosDecisao();
        assertEquals(expResult, result);

    }

    @Test
    public void testSetMecanismo() {
        System.out.println("setMecanismo");
        MecanismosDecisao m = null;
        Empresa empresa = new Empresa();
        DecidirSobreArtigoController instance = new DecidirSobreArtigoController(empresa);
        instance.setMecanismo(m);

    }

    @Test
    public void testRegistaDecisao() {
        System.out.println("registaDecisao");
        Evento e = new Evento();
        Empresa empresa = new Empresa();
        DecidirSobreArtigoController instance = new DecidirSobreArtigoController(empresa);
        instance.selectEvento(e);
        boolean expResult = false;
        boolean result = false;
        assertEquals(expResult, result);

    }

    @Test
    public void testGetEvento() {
        System.out.println("getEvento");
        Empresa empresa = new Empresa();
        DecidirSobreArtigoController instance = new DecidirSobreArtigoController(empresa);
        Evento e = new Evento();
        instance.selectEvento(e);
        Evento expResult = e;
        Evento result = instance.getEvento();
        assertEquals(expResult, result);

    }

    @Test 
    public void testMecanismoDecide() {
        System.out.println("mecanismoDecide");
        Empresa empresa = new Empresa();
        DecidirSobreArtigoController instance = new DecidirSobreArtigoController(empresa);
        Evento e = new Evento();
        instance.selectEvento(e);
        Submissao s = new Submissao();
        instance.setSubmissao(s);
        instance.getArtigoSubmissao();
        boolean expResult = true;
        boolean result = true;
        assertEquals(expResult, result);

    }

}
