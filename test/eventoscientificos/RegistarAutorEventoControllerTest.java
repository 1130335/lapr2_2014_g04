/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import eventoscientificos.controller.RegistarAutorEventoController;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ASUS
 */
public class RegistarAutorEventoControllerTest {
    
    public RegistarAutorEventoControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of selectEvento method, of class RegistarAutorEventoController.
     */
    @Test
    public void testSelectEvento() {
        System.out.println("selectEvento");
        Evento e = new Evento();
        RegistarAutorEventoController instance = new RegistarAutorEventoController(new Empresa());
        instance.selectEvento(e);
        
    }

    /**
     * Test of getEventosAutorSubmeteu method, of class RegistarAutorEventoController.
     */
    @Test (expected=NullPointerException.class)
    public void testGetEventosAutorSubmeteu() {
        System.out.println("getEventosAutorSubmeteu");
        String id = "";
        RegistarAutorEventoController instance = new RegistarAutorEventoController(new Empresa());
        List<Evento> expResult = instance.getEventosAutorSubmeteu(id);
        List<Evento> result = instance.getEventosAutorSubmeteu(id);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of valorPagar method, of class RegistarAutorEventoController.
     */
    @Test (expected=NullPointerException.class)
    public void testValorPagar() {
        System.out.println("valorPagar");
        String id = "";
        RegistarAutorEventoController instance = new RegistarAutorEventoController(new Empresa());
        double expResult = instance.valorPagar(id);
        double result = instance.valorPagar(id);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of formula1 method, of class RegistarAutorEventoController.
     */
    @Test (expected=NullPointerException.class)
    public void testFormula1() {
        System.out.println("formula1");
        String id = "";
        RegistarAutorEventoController instance = new RegistarAutorEventoController(new Empresa());
        double expResult = instance.formula1(id);
        double result = instance.formula1(id);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of formula2 method, of class RegistarAutorEventoController.
     */
    @Test (expected=NullPointerException.class)
    public void testFormula2() {
        System.out.println("formula2");
        String id = "";
        RegistarAutorEventoController instance = new RegistarAutorEventoController(new Empresa());
        double expResult = instance.formula2(id);
        double result = instance.formula2(id);
        assertEquals(expResult, result);
        
    }
    
}
