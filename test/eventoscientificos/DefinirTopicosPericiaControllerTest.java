/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import eventoscientificos.controller.DefinirTopicosPericiaController;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vaio
 */
public class DefinirTopicosPericiaControllerTest {
    
    public DefinirTopicosPericiaControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListaEventos method, of class DefinirTopicosPericiaController.
     */
    @Test
    public void testGetListaEventos() {
        System.out.println("getListaEventos");
        DefinirTopicosPericiaController instance = new DefinirTopicosPericiaController(new Empresa());
        List<Evento> expResult = instance.getListaEventos();
        List<Evento> result = instance.getListaEventos();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of selectEvento method, of class DefinirTopicosPericiaController.
     */
    @Test
    public void testSelectEvento() {
        System.out.println("selectEvento");
        Evento e = new Evento();
        DefinirTopicosPericiaController instance = new DefinirTopicosPericiaController(new Empresa());
        instance.selectEvento(e);
        
    }

    /**
     * Test of getRevisor method, of class DefinirTopicosPericiaController.
     */
    @Test
    public void testGetRevisor() {
        System.out.println("getRevisor");
        String nome = "";
        DefinirTopicosPericiaController instance = new DefinirTopicosPericiaController(new Empresa());
        instance.getRevisor(nome);
        
    }

    /**
     * Test of getListaTopicosAtuais method, of class DefinirTopicosPericiaController.
     */
    @Test
    public void testGetListaTopicosAtuais() {
        System.out.println("getListaTopicosAtuais");
        DefinirTopicosPericiaController instance = new DefinirTopicosPericiaController(new Empresa());
        List<Topico> expResult = instance.getListaTopicosAtuais();
        List<Topico> result = instance.getListaTopicosAtuais();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of removerTopico method, of class DefinirTopicosPericiaController.
     */
    @Test
    public void testRemoverTopico() {
        System.out.println("removerTopico");
        Topico t = new Topico();
        DefinirTopicosPericiaController instance = new DefinirTopicosPericiaController(new Empresa());
        instance.removerTopico(t);
        
    }

    /**
     * Test of getListaTopicosEvento method, of class DefinirTopicosPericiaController.
     */
    @Test
    public void testGetListaTopicosEvento() {
        System.out.println("getListaTopicosEvento");
        DefinirTopicosPericiaController instance = new DefinirTopicosPericiaController(new Empresa());
        List<Topico> expResult = null;
        List<Topico> result = instance.getListaTopicosEvento();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of addTopico method, of class DefinirTopicosPericiaController.
     */
    @Test
    public void testAddTopico() {
        System.out.println("addTopico");
        Topico t = new Topico();
        DefinirTopicosPericiaController instance = new DefinirTopicosPericiaController(new Empresa());
        instance.addTopico(t);
        
    }
    
}
