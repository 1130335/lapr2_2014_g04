/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import eventoscientificos.controller.SubmeterArtigoFinalController;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sofia
 */
public class SubmeterArtigoFinalControllerTest {
    
    public SubmeterArtigoFinalControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of iniciarSubmissaoFinal method, of class SubmeterArtigoFinalController.
     */
    @Test (expected = NullPointerException.class)
    public void testIniciarSubmissaoFinal() {
        System.out.println("iniciarSubmissaoFinal");
        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController(new Empresa());
        List<Evento> expResult = instance.iniciarSubmissaoFinal();
        List<Evento> result = instance.iniciarSubmissaoFinal();
        assertEquals(expResult, result);

    }

    /**
     * Test of selectEvento method, of class SubmeterArtigoFinalController.
     */
    @Test (expected = NullPointerException.class)
    public void testSelectEvento() {
        System.out.println("selectEvento");
        Evento e = null;
        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController(new Empresa());
        instance.selectEvento(e);
    }

    /**
     * Test of novoAutor method, of class SubmeterArtigoFinalController.
     */
    @Test (expected = NullPointerException.class)
    public void testNovoAutor_String_String() {
        System.out.println("novoAutor");
        String strNome = "";
        String strAfiliacao = "";
        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController(new Empresa());
        Autor expResult = instance.novoAutor(strNome, strAfiliacao);
        Autor result = instance.novoAutor(strNome, strAfiliacao);
        assertEquals(expResult, result);
    }

    /**
     * Test of novoAutor method, of class SubmeterArtigoFinalController.
     */
    @Test (expected = NullPointerException.class)
    public void testNovoAutor_3args() {
        System.out.println("novoAutor");
        String strNome = "";
        String strAfiliacao = "";
        String strEmail = "";
        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController(new Empresa());
        Autor expResult = instance.novoAutor(strNome, strAfiliacao, strEmail);
        Autor result = instance.novoAutor(strNome, strAfiliacao, strEmail);
        assertEquals(expResult, result);
    }

    /**
     * Test of addAutor method, of class SubmeterArtigoFinalController.
     */
    @Test (expected = NullPointerException.class)
    public void testAddAutor() {
        System.out.println("addAutor");
        Autor autor = null;
        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController (new Empresa());
        boolean expResult = false;
        boolean result = instance.addAutor(autor);
        assertEquals(expResult, result);
    }

    /**
     * Test of registarSubmissao method, of class SubmeterArtigoFinalController.
     */
    @Test (expected = NullPointerException.class)
    public void testRegistarSubmissao() {
        System.out.println("registarSubmissao");
        SubmeterArtigoFinalController instance = new SubmeterArtigoFinalController(new Empresa());
        boolean expResult = false;
        boolean result = instance.registarSubmissao();
        assertEquals(expResult, result);
    }

    
}
