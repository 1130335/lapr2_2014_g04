/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.Empresa;
import eventoscientificos.Evento;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Sofia
 */
public class NotificarAutorControllerTest {
    
    public NotificarAutorControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test (expected = NullPointerException.class)
    public void testCriarNotificacao() {
        System.out.println("criarNotificacao");
        NotificarAutorController instance = new NotificarAutorController(new Empresa());
        instance.criarNotificacao();

    }
    
}
