/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import eventoscientificos.controller.DefinirValoresPagarController;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1130429
 */
public class DefinirValoresPagarControllerTest {
    
    public DefinirValoresPagarControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of iniciarDefinirValores method, of class DefinirValoresPagarController.
     */
    @Test(expected=NullPointerException.class)
    public void testIniciarDefinirValores() {
        System.out.println("iniciarDefinirValores");
        String strId = "";
        DefinirValoresPagarController instance = new DefinirValoresPagarController(new Empresa());
        List<Evento> expResult = instance.iniciarDefinirValores(strId);
        List<Evento> result = instance.iniciarDefinirValores(strId);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of selectEvento method, of class DefinirValoresPagarController.
     */
    @Test
    public void testSelectEvento() {
        System.out.println("selectEvento");
        Evento e = new Evento();
        DefinirValoresPagarController instance = new DefinirValoresPagarController(new Empresa());
        instance.selectEvento(e);
        
    }

    /**
     * Test of setValorFull method, of class DefinirValoresPagarController.
     */
    @Test(expected=NullPointerException.class)
    public void testSetValorFull() {
        System.out.println("setValorFull");
        String F = "";
        DefinirValoresPagarController instance = new DefinirValoresPagarController(new Empresa());
        instance.setValorFull(F);
        
        
    }

    /**
     * Test of setValorShort method, of class DefinirValoresPagarController.
     */
    @Test(expected=NullPointerException.class)
    public void testSetValorShort() {
        System.out.println("setValorShort");
        String S = "";
        DefinirValoresPagarController instance = new DefinirValoresPagarController(new Empresa());
        instance.setValorShort(S);
       
    }

    /**
     * Test of setValorPoster method, of class DefinirValoresPagarController.
     */
    @Test(expected=NullPointerException.class)
    public void testSetValorPoster() {
        System.out.println("setValorPoster");
        String P = "";
        DefinirValoresPagarController instance = null;
        instance.setValorPoster(P);
        
    }

    /**
     * Test of setFormula method, of class DefinirValoresPagarController.
     */
    @Test(expected=NullPointerException.class)
    public void testSetFormula() {
        System.out.println("setFormula");
        String formula = "";
        DefinirValoresPagarController instance = new DefinirValoresPagarController(new Empresa());
        instance.setFormula(formula);
    }
    
}
