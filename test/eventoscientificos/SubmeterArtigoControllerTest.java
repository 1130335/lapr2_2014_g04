/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import eventoscientificos.controller.SubmeterArtigoController;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sofia
 */
public class SubmeterArtigoControllerTest {
    
    public SubmeterArtigoControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of iniciarSubmissao method, of class SubmeterArtigoController.
     */
    @Test (expected = NullPointerException.class)
    public void testIniciarSubmissao() {
        System.out.println("iniciarSubmissao");
        SubmeterArtigoController instance = new SubmeterArtigoController(new Empresa());
        List<Evento> expResult = instance.iniciarSubmissao();
        List<Evento> result = instance.iniciarSubmissao();
        assertEquals(expResult, result);

    }


    /**
     * Test of selectEvento method, of class SubmeterArtigoController.
     */
    @Test
    public void testSelectEvento() {
        System.out.println("selectEvento");
        Evento e = new Evento();
        SubmeterArtigoController instance = new SubmeterArtigoController(new Empresa());
        instance.selectEvento(e);
    }


    /**
     * Test of selectTopico method, of class SubmeterArtigoController.
     */
    @Test
    public void testSelectTopico() {
        System.out.println("selectTopico");
        Topico t = new Topico();
        SubmeterArtigoController instance = new SubmeterArtigoController (new Empresa());
        instance.selectTopico(t);
    }

    /**
     * Test of novoAutor method, of class SubmeterArtigoController.
     */
    @Test (expected = NullPointerException.class)
    public void testNovoAutor_String_String() {
        System.out.println("novoAutor");
        String strNome = "";
        String strAfiliacao = "";
        SubmeterArtigoController instance = new SubmeterArtigoController(new Empresa());
        Autor expResult = instance.novoAutor(strNome, strAfiliacao);
        Autor result = instance.novoAutor(strNome, strAfiliacao);
        assertEquals(expResult, result);
    }

    /**
     * Test of novoAutor method, of class SubmeterArtigoController.
     */
    @Test (expected = NullPointerException.class)
    public void testNovoAutor_3args() {
        System.out.println("novoAutor");
        String strNome = "";
        String strAfiliacao = "";
        String strEmail = "";
        SubmeterArtigoController instance = new SubmeterArtigoController(new Empresa());
        Autor expResult = instance.novoAutor(strNome, strAfiliacao, strEmail);
        Autor result = instance.novoAutor(strNome, strAfiliacao, strEmail);
        assertEquals(expResult, result);
    }

    /**
     * Test of addAutor method, of class SubmeterArtigoController.
     */
    @Test (expected = NullPointerException.class)
    public void testAddAutor() {
        System.out.println("addAutor");
        Autor autor = new Autor();
        SubmeterArtigoController instance = new SubmeterArtigoController(new Empresa());
        boolean expResult = false;
        boolean result = instance.addAutor(autor);
        assertEquals(expResult, result);
    }

    /**
     * Test of registarSubmissao method, of class SubmeterArtigoController.
     */
    @Test (expected = NullPointerException.class)
    public void testRegistarSubmissao() {
        System.out.println("registarSubmissao");
        SubmeterArtigoController instance = new SubmeterArtigoController(new Empresa());
        boolean expResult = false;
        boolean result = instance.registarSubmissao();
        assertEquals(expResult, result);
    }
    
}
