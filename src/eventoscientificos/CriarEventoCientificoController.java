package eventoscientificos;

/**
 *
 * @author Nuno Silva
 */

public class CriarEventoCientificoController
{
    private Empresa m_empresa;
    private Evento m_evento;
    private RegistarEvento m_registarEvento;
    private RegistarUtilizador m_registarUtilizador;
    private int nrMaxTopicos;
    private String dataRevisao;
    private String dataLimiteSubmissaoFinal;
    private String dataLimiteRegisto;

    /**
     * Cria um construtor de criarEventoCientificoController que recebe a empresa como paramentro
     * @param empresa 
     */
    public CriarEventoCientificoController(Empresa empresa)
    {
        m_empresa = empresa;
        m_registarEvento = new RegistarEvento(empresa);
    }
/**
 * Cria um novo evento
 */
    public void novoEvento()
    {
        m_evento = m_registarEvento.novoEvento();
    }
    
/**
 * Consulta os evento
 * @return evento
 */
    public String getEventoString()
    {
        return m_evento.toString();
    }
/**
 * Altera o titulo
 * @param strTitulo 
 */
    public void setTitulo(String strTitulo)
    {
        m_evento.setTitulo(strTitulo);
    }
    /**
     * Altera a descrição
     * @param strDescricao 
     */
    public void setDescricao(String strDescricao)
    {
        m_evento.setDescricao(strDescricao);
    }
/**
 * Altera o local
 * @param strLocal 
 */
    public void setLocal(String strLocal)
    {
        m_evento.setLocal(strLocal);
    }
    /**
    Altera a data de inicio
    */
    public void setDataInicio(String strDataInicio)
    {
        m_evento.setDataInicio(strDataInicio);
    }
/**
 * Altera a data de fim
 * @param strDataFim 
 */
    public void setDataFim(String strDataFim)
    {
        m_evento.setDataFim(strDataFim);
    }
    
    /**
     * Altera a data de submissao
     * @param strDataLimiteSubmissao 
     */
    // adicionado na iteração 2
    public void setDataLimiteSubmissão(String strDataLimiteSubmissao)
    {
        m_evento.setDataLimiteSubmissao(strDataLimiteSubmissao);
    }
    
    /**
     * Altera o nr max de topicos
     * @param nrMaxTopicos 
     */
     public void setNrMaxTopicos(int nrMaxTopicos) {
        m_evento.setNrMaxTopicos(nrMaxTopicos);
    }

     /**
      * Altera a data limite de revisao
      * @param dataLimiteRevisao 
      */
    public void setDataLimiteRevisao(String dataLimiteRevisao) {
        m_evento.setDataLimiteRevisao(dataLimiteRevisao);
    }

    /**
     * Altera a data limite de submissao final
     * @param dataLimiteSubmissaoFinal 
     */
    public void setDataLimiteSubmissaoFinal(String dataLimiteSubmissaoFinal) {
        m_evento.setDataLimiteSubmissaoFinal(dataLimiteSubmissaoFinal);
    }

    /**
     * Altera a data de limite de registo
     * @param dataLimiteRegisto 
     */
    public void setDataLimiteRegisto(String dataLimiteRegisto) {
        m_evento.setDataLimiteRegisto(dataLimiteRegisto);
    }
    
/**
 * Adiciona um organizador ao evento
 * @param strId
 * @return organizador adicionado caso o utilizador não seja igual a null
 */
    public boolean addOrganizador(String strId)
    {
        Utilizador u = m_empresa.getRegistarUtilizadores().getUtilizador(strId);
        
        if( u!=null)
            return m_evento.addOrganizador( strId, u );
        else
            return false;
    }
    /**
     * Regista o evento
     * @return evento registado
     */
    public boolean registaEvento()
    {
        return m_registarEvento.registaEvento(m_evento);
    }

    }


