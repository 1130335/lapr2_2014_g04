/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rita
 */
public class MecanismosDecisao implements MecanismosDecisaoInterface {

    private String titulo;

    /**
     * Construtor de um Mecanismo de Decisão, para testes, sem parâmetros
     */
    public MecanismosDecisao() {
        this.titulo = "Mecanismo por Recomendação";
    }

    /**
     * Devolve o título dado ao mecanismo
     *
     * @return titulo do mecanismo
     */
    public String getTitulo() {
        return this.titulo;
    }

    /**
     * Altera o titulo do mecanismo para aquele que é passado como parametro
     *
     * @param titulo novo titulo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * Mecanismo de Decisao que decide se um artigo deve ser aceite se tiver 3
     * ou mais recomendaçoes positivas
     *
     * @param pd processo de decisao
     * @return decisao tomada pelo mecanismo
     */
    @Override
    public Decisao decide(ProcessoDecisao pd) {
        Decisao d = pd.novaDecisao();
        Artigo artigo = pd.getArtigo();
        List<Revisao> lr = artigo.getListaRevisoes();
        int decisao = 0;
        for (Revisao r : lr) {
            if (r.getRecomendacao()) {
                decisao++;
            }
        }
        if (decisao >= 3) {
            d.setArtigo(artigo);
            d.setDecisao(true);
        }

        return d;
    }

}
