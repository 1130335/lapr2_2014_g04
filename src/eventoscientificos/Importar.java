

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;


import java.io.FileNotFoundException;
import java.util.List;

/**
 *
 * @author 1130429
 */
public interface Importar {
    public abstract List<String> ImportarAno() throws FileNotFoundException;
    public abstract List<String> ImportarNome()throws FileNotFoundException;
    public abstract List<String> ImportHost()throws FileNotFoundException;
    public abstract List<String> ImportarCidade()throws FileNotFoundException;
    public abstract List<String> ImportarPais()throws FileNotFoundException;
    public abstract List<String> ImportarDataInicio()throws FileNotFoundException;
    public abstract List<String> ImportarDataFim()throws FileNotFoundException;
    public abstract List<String> ImportarWebsite()throws FileNotFoundException;
    public abstract List<Organizador> ImportarOrganizador()throws FileNotFoundException;
    public abstract List<Integer> ImportNorgs() throws FileNotFoundException;
    
}
