/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 * Excepção criada caso a ordem das datas não seja introduzida corretamente
 * @author Sofia
 */
class OrdemDatasInvalidoException extends IllegalArgumentException {

    public OrdemDatasInvalidoException() {
        super("Ordem das datas introduzidas inválida!");
    }
    
    public OrdemDatasInvalidoException(String mensagem) {
        super(mensagem);
    }
    
}
