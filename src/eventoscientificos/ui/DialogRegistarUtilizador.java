/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.Empresa;
import eventoscientificos.Utilizador;
import eventoscientificos.controller.RegistarUtilizadorController;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Sofia
 */
public class DialogRegistarUtilizador extends JDialog {

    private JTextField txtUsername, txtEmail, txtNome;
    private JPasswordField txtPassword, txtConfirmaPassword;
    private Empresa empresa;

    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private static final Dimension LABEL_TAMANHO = new JLabel("Confirmar Password:").getPreferredSize();
   

    public DialogRegistarUtilizador(Frame pai, Empresa empresa) {

        super(pai, "Registar Utilizador", true);

        

        this.empresa = empresa;

        JPanel p1 = painelPrincipal();
        JPanel p2 = criarPainelBotoes();
        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.SOUTH);
        

        setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);
        

    }

    private JPanel criarPainelUsername() {
        JLabel lbl = new JLabel("Username:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtUsername = new JTextField(CAMPO_LARGURA);
        txtUsername.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtUsername);

        return p;
    }

    private JPanel criarPainelNome() {
        JLabel lbl = new JLabel("Nome:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtNome = new JTextField(CAMPO_LARGURA);
        txtNome.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtNome);

        return p;
    }

    private JPanel criarPainelEmail() {
        JLabel lbl = new JLabel("Email:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtEmail = new JTextField(CAMPO_LARGURA);
        txtEmail.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtEmail);

        return p;
    }

    private JPanel criarPainelPassword() {
        JLabel lbl = new JLabel("Password:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtPassword = new JPasswordField(CAMPO_LARGURA);
        txtPassword.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtPassword);

        return p;
    }

    private JPanel criarPainelConfirmarPassword() {
        JLabel lbl = new JLabel("Confirmar Password:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtConfirmaPassword = new JPasswordField(CAMPO_LARGURA);
        txtConfirmaPassword.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtConfirmaPassword);

        return p;
    }

    private JPanel painelPrincipal() {
        JPanel p = new JPanel(new GridLayout(5, 1));

        JPanel p1 = criarPainelUsername();
        JPanel p2 = criarPainelNome();
        JPanel p3 = criarPainelEmail();
        JPanel p4 = criarPainelPassword();
        JPanel p5 = criarPainelConfirmarPassword();

        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        p.add(p5);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String username = txtUsername.getText();
                    if (username.isEmpty()) {
                        throw new UsernameInvalidoException();
                    }
                    String nome = txtNome.getText();
                    if (nome.isEmpty()) {
                        throw new NomeInvalidoException();
                    }
                    String email = txtEmail.getText();
                    if (email.isEmpty()) {
                        throw new EmailInvalidoException();
                    }
                    String password = txtPassword.getText();
                    if (password.isEmpty()) {
                        throw new PasswordInvalidoException();
                    }
                    String confirmaPassword = txtConfirmaPassword.getText();
                    if (confirmaPassword.isEmpty()) {
                        throw new ConfirmaPasswordInvalidoException();
                    }
                    if (!password.equals(confirmaPassword)) {
                        throw new ConfirmaPasswordInvalidoException();

                    }
                    if (!validarMail(email)) {
                        throw new EmailInvalidoException();

                    }

                    RegistarUtilizadorController ruc = new RegistarUtilizadorController(empresa);
                    ruc.novoUtilizador();

                  Utilizador u =  ruc.setDados(username, password, nome, email);
                    JOptionPane.showMessageDialog(null,"Utilizador registado com sucesso!");
                    dispose();
                    




                } catch (UsernameInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogRegistarUtilizador.this,
                            "Tem que introduzir um Username válido.",
                            "Registo do Utilizador.",
                            JOptionPane.WARNING_MESSAGE);
                    txtUsername.requestFocus();
                } catch (NomeInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogRegistarUtilizador.this,
                            "Tem que introduzir um Nome válido.",
                            "Registo do Utilizador.",
                            JOptionPane.WARNING_MESSAGE);
                    txtNome.requestFocus();
                } catch (EmailInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogRegistarUtilizador.this,
                            "Tem que introduzir um Email válido.",
                            "Registo do Utilizador.",
                            JOptionPane.WARNING_MESSAGE);
                    txtEmail.requestFocus();
                } catch (PasswordInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogRegistarUtilizador.this,
                            "Tem que introduzir uma Password válida.",
                            "Registo do Utilizador.",
                            JOptionPane.WARNING_MESSAGE);
                    txtPassword.requestFocus();
                } catch (ConfirmaPasswordInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogRegistarUtilizador.this,
                            "Tem que introduzir uma Password igual à anteriormente introduzida.",
                            "Registo do Utilizador.",
                            JOptionPane.WARNING_MESSAGE);
                    txtConfirmaPassword.requestFocus();
                }

            }
        });
        return btn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

    public static boolean validarMail(String email) {
        boolean isEmailIdValid = false;
        if (email != null && email.length() > 0) {
            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(email);
            if (matcher.matches()) {
                isEmailIdValid = true;
            }
        }
        return isEmailIdValid;
    }

}
