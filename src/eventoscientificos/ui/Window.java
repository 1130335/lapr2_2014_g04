/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.*;
import eventoscientificos.controller.CriarCPController;
import eventoscientificos.controller.DecidirSobreArtigoController;
import eventoscientificos.controller.DefinirTopicosPericiaController;
import eventoscientificos.controller.DefinirValoresPagarController;
import eventoscientificos.controller.NotificarAutorController;
import eventoscientificos.controller.RegistarAutorEventoController;
import eventoscientificos.controller.SubmeterArtigoController;
import eventoscientificos.controller.SubmeterArtigoFinalController;
import eventoscientificos.controller.SubmeterRevisaoArtigoController;
import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;

/**
 *
 * @author 1130429
 */
public class Window extends JFrame {

    private DecidirSobreArtigoController m_controllerDSA;
    private SubmeterArtigoController saController;
    private SubmeterArtigoFinalController safController;
    private DefinirTopicosPericiaController m_controllerDTP;
    private ImportacaoCSV m_icsv;
    private RegistarEvento m_registarEvento;
    private static Empresa m_empresa;
    private SubmeterRevisaoArtigoController raController;
    private DefinirValoresPagarController m_Controller;
    private RegistarAutorEventoController RAController;
    private CriarCPController cpController;
    private Dimension minSize;

    public Window(Empresa empresa) {

        super("Gestão De Eventos Científicos");
        this.m_empresa = empresa;
        m_icsv = new ImportacaoCSV(m_empresa);

        Rectangle winSize = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        minSize = new Dimension(winSize.width - 660, winSize.height - 450);
        this.setJMenuBar(criarMenuBar());
        add(new JLabel(new ImageIcon("imagem.png")));

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                fechar();
            }
        });
        this.setPreferredSize(minSize);
        this.setLocation(0, 0);
        this.setResizable(false);
        this.setVisible(true);
        this.pack();
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

    }

    private JMenuBar criarMenuBar() {

        JMenuBar m = new JMenuBar();
        m.add(criarMenuFicheiro());
        m.add(criarMenuAdmin());
        m.add(criarMenuRev());
        m.add(criarMenuAutor());
        m.add(criarMenuOrg());
        m.add(criarMenuUtilizador());

        return m;
    }

    private JMenu criarMenuFicheiro() {
        JMenu f = new JMenu("Ficheiro");
        f.add(criarItemImpEComp());
        f.add(criarItemImpArt());
        f.add(criarItemImpRev());
        f.add(criarItemSair());

        return f;
    }

    private JMenuItem criarItemSair() {
        JMenuItem item = new JMenuItem("Sair", KeyEvent.VK_S);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fechar();
            }
        });

        return item;
    }

    /*
     * Permite a importação de eventos.
     */
    private JMenuItem criarItemImpEComp() {
        JMenuItem item = new JMenuItem("Importar Eventos Completos", KeyEvent.VK_I);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String caminho = JOptionPane.showInputDialog("Indique o caminho para o ficheiro CSV do qual pretende importar os eventos:");
                try {
                    List<Evento> eventos = m_icsv.ImportarEventoCompleto(caminho);
                    if (eventos != null) {
                        JOptionPane.showMessageDialog(null, "Importação feita com sucesso!");
                        for (Evento evento : eventos) {
                            m_empresa.getRegistarEventos().addEvento(evento);
                            System.out.println(evento.getIDEvento());
                            
                        }
                    }
                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(null, "Caminho inválido!!!");
                }

            }
        });

        return item;
    }

    private JMenuItem criarItemImpArt() {
        JMenuItem item = new JMenuItem("Importar Artigos", KeyEvent.VK_L);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String caminho = JOptionPane.showInputDialog("Indique o caminho para o ficheiro CSV do qual pretende importar os artigos:");
                try {
                    List<Artigo> artigos = m_icsv.ImportarArtigoCSV(caminho,m_empresa);
                    if (artigos != null) {
                        JOptionPane.showMessageDialog(null, "Importação feita com sucesso!");
                    }
                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(null, "Caminho inválido!!!");
                }

            }
        });

        return item;
    }

    private JMenuItem criarItemImpRev() {
        JMenuItem item = new JMenuItem("Importar Revisoes", KeyEvent.VK_Z);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String caminho = JOptionPane.showInputDialog("Indique o caminho para o ficheiro CSV do qual pretende importar os revisoes:");
                try {
                    List<Revisao> revisoes = m_icsv.ImportarRevisaoCSV(caminho);
                    if (revisoes != null) {
                        JOptionPane.showMessageDialog(null, "Importação feita com sucesso!");
                    }
                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(null, "Caminho inválido!!!");
                }

            }
        });

        return item;
    }

    private JMenu criarMenuAdmin() {
        JMenu menu = new JMenu("Administrador");
        menu.add(criarItemCriarEvento());

        return menu;
    }

    private JMenu criarMenuUtilizador() {
        JMenu menu = new JMenu("Utilizador");
        menu.add(criarItemRegistarUtilizador());

        return menu;
    }

    /**
     * Cria opção de registar utilizador
     *
     * @return item
     */
    private JMenuItem criarItemRegistarUtilizador() {
        JMenuItem item = new JMenuItem("Registar Utilizador", KeyEvent.VK_R);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogRegistarUtilizador dialog = new DialogRegistarUtilizador(Window.this, m_empresa);
            }
        });
        return item;
    }

    /**
     * Cria opção de criar evento cientifico
     *
     * @return item
     */
    private JMenuItem criarItemCriarEvento() {
        JMenuItem item = new JMenuItem("Criar evento Científico", KeyEvent.VK_C);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogCriarEvento dialog = new DialogCriarEvento(Window.this, m_empresa);
            }
        });
        return item;
    }

    private JMenu criarMenuRev() {
        JMenu menu = new JMenu("Revisor");
        menu.add(criarItemSubRev());
        menu.add(criarItemTopPer());

        return menu;
    }
    

    private JMenuItem criarItemSubRev() {
        JMenuItem item = new JMenuItem("Submeter revisão de artigo", KeyEvent.VK_V);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String id = introduzID();
                List<Evento> listaEventos = raController.iniciarRevisao(id);
                if (listaEventos != null) {
                    Evento ev = selecionaEvento(RegistarEvento.listaNomesDeEventos(listaEventos));
                    if (ev != null) {
                        raController.selectEvento(ev);
                        List<Artigo> listaArtigos = raController.iniciarRevisaoArtigo(id);
                        Artigo a = selecionaArtigo(ev.listaNomesDeArtigos(listaArtigos));
                        if (a != null) {
                            raController.selectArtigo(a);
                            DialogReverArtigo dialog = new DialogReverArtigo(Window.this, m_empresa);
                        }
                    }
                }
            }
        });
        return item;
    }

    private JMenuItem criarItemTopPer() {
        JMenuItem item = new JMenuItem("Definir Tópicos de Perícia", KeyEvent.VK_P);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                m_controllerDTP = new DefinirTopicosPericiaController(m_empresa);
                String idRev = JOptionPane.showInputDialog("Introduza o ID do Revisorr:");
                List<Evento> le = m_controllerDTP.getListaEventos();
                DialogListaEvento dle = new DialogListaEvento(Window.this, le);
                Evento evento = dle.getEvento();
                m_controllerDTP.selectEvento(evento);
                if (evento != null) {
                    List<Topico> lta = new ArrayList<>();
                    List<Topico> lte = saController.getTopicosEvento();
                    boolean opcao = false;
                    while (opcao == false) {
                        selecionaTopicos(lta, lte);
                        m_controllerDTP.setTopico(lta);
                        if (m_controllerDTP.addTopico(lta)) {
                            JOptionPane.showMessageDialog(null, "Tópicos de Pericia definidos com sucesso!!!");
                        }

                    }
                }
            }

        });
        return item;
    }

    private JMenu criarMenuAutor() {
        JMenu menu = new JMenu("Autor");
        menu.add(criarItemRegAutor());
        menu.add(criarItemSubArt());
        menu.add(criarItemSubArtFin());
        return menu;
    }

    private JMenuItem criarItemRegAutor() {
        JMenuItem item = new JMenuItem("Registar autor", KeyEvent.VK_R);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RAController = new RegistarAutorEventoController(m_empresa);
                String id = JOptionPane.showInputDialog("Introduza o ID do Autor:");
                List<Evento> le = RAController.getEventosAutorSubmeteu(id);
                DialogListaEvento dsa = new DialogListaEvento(Window.this, le);
                Evento evento = dsa.getEvento();
                if (evento != null) {
                    RAController.selectEvento(evento);
                    float vp = RAController.valorPagar(id);
                    int p = selecionaModoPagamento();
//                    RAController.selecionaServico(p);

                    DialogRegistarAutorEvento dialog = new DialogRegistarAutorEvento(Window.this, m_empresa, vp);

                }

            }

        });

        return item;

    }

    /*
    * Cria a opção de submeter artigo para submissao
    * return item
    */
    
    private JMenuItem criarItemSubArt() {
        JMenuItem item = new JMenuItem("Submeter artigo científico", KeyEvent.VK_K);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saController = new SubmeterArtigoController(m_empresa);
                List<Evento> le = saController.iniciarSubmissao();
                DialogListaEvento dsa = new DialogListaEvento(Window.this, le);
                Evento evento = dsa.getEvento();
                if (evento != null) {
                    saController.selectEvento(evento);
                    DialogDadosArtigo dda = new DialogDadosArtigo(Window.this);
                    String titulo = dda.getTituto();
                    String tipo = dda.getTipo();
                    String resumo = dda.getResumo();
                    String ficheiro = dda.getFicheiro();
                    saController.setDados(titulo, resumo, tipo, ficheiro);
                    boolean op = false;
                    while (op == false) {
                        op = criarAutor();

                    }
                    List<Autor> la = saController.getPossiveisAutoresCorrespondentes();
                    DialogACorrespondente dac = new DialogACorrespondente(Window.this, la);
                    saController.setCorrespondente(dac.getAutor());
                    List<Topico> lta = new ArrayList<>();
                    List<Topico> lte = saController.getTopicosEvento();
                    boolean opcao = false;
                    while (opcao == false || lta.size() == evento.nrMaxTopicos) {
                        selecionaTopicos(lta, lte);
                        String[] opSimNao = {"Sim", "Não"};
                        int resposta = JOptionPane.showOptionDialog(Window.this,
                                "Deseja escolher outro tópico?",
                                "Escolher Tópico",
                                0,
                                JOptionPane.QUESTION_MESSAGE,
                                null,
                                opSimNao,
                                opSimNao[1]);
                        if (resposta == 1) {
                            opcao = true;
                        }
                    }
                    saController.setListaTopicosArtigo(lta);
                    if (saController.registarSubmissao()) {
                        JOptionPane.showMessageDialog(null, "Submissao feita com sucesso!!!");
                    }

                }
            }
        });
        return item;
    }

    /*
    * Cria autores de artigos
    */
    public boolean criarAutor() {
        DialogCAutores dca = new DialogCAutores(Window.this);
        Autor a = saController.novoAutor(dca.getM_nome(), dca.getM_afiliacao(), dca.getM_email());
        for (Utilizador u : m_empresa.getRegistarUtilizadores().getListaUtilizadores()) {
            if (a.getEmail().equalsIgnoreCase(u.getEmail())) {
                a.setUtilizador(u);

            }
        }
        saController.addAutor(a);

        boolean opcao = dca.getOpcao();

        return opcao;

    }

    /*
    * Seleciona tópicos de evento para serem tópicos de artigo
    */
    public boolean selecionaTopicos(List<Topico> lta, List<Topico> lte) {
        DialogTopArtigo dta = new DialogTopArtigo(Window.this, lte);
        Topico t = dta.getTopicoArtigo();
        return lta.add(t);
    }

    private JMenuItem criarItemSubArtFin() {
        JMenuItem item = new JMenuItem("Submeter artigo científico final", KeyEvent.VK_F);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                safController = new SubmeterArtigoFinalController(m_empresa);
                List<Evento> le = safController.iniciarSubmissaoFinal();
                DialogListaEvento dsa = new DialogListaEvento(Window.this, le);
                Evento evento = dsa.getEvento();
                if (evento != null) {
                    safController.selectEvento(evento);
                    DialogDadosArtigoFinal ddaf = new DialogDadosArtigoFinal(Window.this);
                    String titulo = ddaf.getTituto();
                    String tipo = ddaf.getTipo();
                    String resumo = ddaf.getResumo();
                    safController.setDados(titulo, resumo, tipo);
                    boolean op = false;
                    while (op == false) {
                        op = criarAutor();
                        String[] opSimNao = {"Sim", "Não"};
                        int resposta = JOptionPane.showOptionDialog(Window.this,
                                "Deseja introduzir outro autor?",
                                "Cria Autor",
                                0,
                                JOptionPane.QUESTION_MESSAGE,
                                null,
                                opSimNao,
                                opSimNao[1]);
                        if (resposta == 1) {
                            op = true;
                        }
                    }

                }
                List<Autor> la = safController.getPossiveisAutoresCorrespondentes();
                DialogACorrespondente dac = new DialogACorrespondente(Window.this, la);
                safController.setCorrespondente(dac.getAutor());
                List<Topico> lta = new ArrayList<>();
                boolean opcao = false;
                List<Topico> lte = saController.getTopicosEvento();
                while (opcao == false) {
                    selecionaTopicos(lta, lte);
                    String[] opSimNao = {"Sim", "Não"};
                    int resposta = JOptionPane.showOptionDialog(Window.this,
                            "Deseja escolher outro tópico?",
                            "Escolher Tópico",
                            0,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            opSimNao,
                            opSimNao[1]);
                    if (resposta == 1) {
                        opcao = true;
                    }
                }
                safController.setListaTopicosArtigo(lta);
                if (safController.registarSubmissao()) {
                    JOptionPane.showMessageDialog(null, "Submissao Final concluída com sucesso!!!");
                }

            }

        });
        return item;
    }

    private JMenu criarMenuOrg() {
        JMenu menu = new JMenu("Organizador");
        menu.add(criarItemImpEvento());
        menu.add(criarItemDefFormOrg());
        menu.add(criarItemRevArt());
        menu.add(criarItemDecArt());
        menu.add(criarItemNotAut());
        menu.add(criarItemTopEvento());
        menu.add(criarItemCriarCP());
        return menu;
    }

    private JMenuItem criarItemImpEvento() {
        JMenuItem item = new JMenuItem("Importar evento científico de ficheiro", KeyEvent.VK_I);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Codigo aqui..
            }
        });
        return item;
    }

    private JMenuItem criarItemDefFormOrg() {
        JMenuItem item = new JMenuItem("Definir formula de pagamento de artigo", KeyEvent.VK_D);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                m_Controller = new DefinirValoresPagarController(m_empresa);
                String idOrg = JOptionPane.showInputDialog("Introduza o ID de Organizador:");
                List<Evento> le = m_Controller.iniciarDefinirValores(idOrg);
                DialogListaEvento dle = new DialogListaEvento(Window.this, le);
                Evento evento = dle.getEvento();
                m_Controller.selectEvento(evento);
                if (evento != null) {
                    DialogDefinirValoresPagar dialog = new DialogDefinirValoresPagar(Window.this, m_empresa);
                }
            }
        });
        return item;
    }

    private JMenuItem criarItemRevArt() {
        JMenuItem item = new JMenuItem("Distribuir Revisões de Artigo", KeyEvent.VK_E);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Codigo aqui...
            }
        });
        return item;
    }

    private JMenuItem criarItemDecArt() {
        JMenuItem item = new JMenuItem("Decidir sobre Artigo", KeyEvent.VK_A);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MecanismosDecisao m;
                m_controllerDSA = new DecidirSobreArtigoController(m_empresa);
                String idOrg = JOptionPane.showInputDialog("Introduza o ID de Organizador:");
                List<Evento> le = m_controllerDSA.novaDecisao(idOrg);
                DialogListaEvento dle = new DialogListaEvento(Window.this, le);
                Evento evento = dle.getEvento();
                m_controllerDSA.selectEvento(evento);
                if (evento != null) {
                    List<Submissao> ls = m_controllerDSA.getSubmissoesEvento();
                    DialogListaSubmissoes dls = new DialogListaSubmissoes(Window.this, ls);
                    Submissao s = dls.getSubmissao();
                    m_controllerDSA.setSubmissao(s);
                    Artigo a = m_controllerDSA.getArtigoSubmissao();
                    boolean op = false;
                    while (op == false) {
                        m = selecionaMecanismo();
                        Decisao decisao = m_controllerDSA.setMecanismoDecisao(m, a);
                        String strDec;
                        if (decisao.getDecisao() == true) {
                            strDec = "Decisao: Aceite. Deseja guardar a decisao?";
                        } else {
                            strDec = "Decisao: Rejeitado. Deseja guardar a decisao?";
                        }

                        String[] opSimNao = {"Sim", "Não"};
                        int resposta = JOptionPane.showOptionDialog(Window.this,
                                strDec,
                                "Decidir sobre Artigo",
                                0,
                                JOptionPane.QUESTION_MESSAGE,
                                null,
                                opSimNao,
                                opSimNao[1]);

                        final int SIM = 0;
                        if (resposta == SIM) {
                            m_controllerDSA.registaDecisao(decisao);
                            if (a.getDecisao() == decisao.getDecisao()) {
                                JOptionPane.showMessageDialog(null, "Decisão guardada com sucesso!");
                            }
                            op = true;
                        }

                    }

                }
            }
        });
        return item;
    }

    private MecanismosDecisao selecionaMecanismo() {
        List<MecanismosDecisao> lm = m_controllerDSA.getMecanismosDecisao();
        DialogListaMecanismos dsm = new DialogListaMecanismos(Window.this, lm);
        MecanismosDecisao m = dsm.getMecanismo();
        return m;
    }

    /**
     * Cria opção de administrador de notificar autores
     *
     * @return item
     */
    private JMenuItem criarItemNotAut() {
        JMenuItem item = new JMenuItem("Notificar Autores", KeyEvent.VK_N);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RegistarUtilizador m_ru = new RegistarUtilizador();
                try {
                    if (!m_ru.getListaUtilizadores().isEmpty()) {
                        DialogIdOrganizador dialog = new DialogIdOrganizador(Window.this);
                        NotificarAutorController m_controllerNA = new NotificarAutorController(m_empresa);
                        RegistarEvento m_re = new RegistarEvento(m_empresa);
                        try {
                            List<Evento> listaEventos = m_re.getListaEventosOrganizador(DialogIdOrganizador.getIdOrg());
                            if (!listaEventos.isEmpty()) {
                                Evento[] a = new Evento[listaEventos.size()];
                                Evento[] opcao = listaEventos.toArray(a);
                                Evento ev = (Evento) JOptionPane.showInputDialog(Window.this,
                                        "Escolha um evento:", "Notificar Autor(es)",
                                        JOptionPane.PLAIN_MESSAGE,
                                        null,
                                        opcao,
                                        opcao[0]);

                                if (ev != null) {
                                    m_controllerNA.setEvento(ev);
                                    int resposta = 0;
                                    final int SIM = 0;

                                    if (DialogIdOrganizador.getIdOrg() != null
                                            && DialogIdOrganizador.verificarIDOrganizador(DialogIdOrganizador.getIdOrg())) {

                                        String[] opSimNao = {"Sim", "Não"};
                                        resposta = JOptionPane.showOptionDialog(Window.this,
                                                "Confirma o envio de notificações para os autores correspondestes?",
                                                "Notificar Autor(es)",
                                                0,
                                                JOptionPane.QUESTION_MESSAGE,
                                                null,
                                                opSimNao,
                                                opSimNao[1]);

                                        if (resposta == SIM) {

                                            m_controllerNA.criarNotificacao();

                                        }

                                    }
                                }

                            } else {
                                throw new IDInvalidoException();
                            }
                        } catch (IDInvalidoException excecao) {

                        }
                    } else {
                        throw new ListaUtilizadoresInvalidoException();
                    }

                } catch (ListaUtilizadoresInvalidoException execao) {
                    JOptionPane.showMessageDialog(Window.this,
                            "Não existem Organizadores",
                            "Notificar Autor",
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        return item;
    }

    private JMenuItem criarItemTopEvento() {
        JMenuItem item = new JMenuItem("Criar Tópicos de Evento", KeyEvent.VK_W);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Codigo aqui..
            }
        });
        return item;
    }

    private void fechar() {
        String[] opSimNao = {"Sim", "Não"};
        int resposta = JOptionPane.showOptionDialog(this,
                "Deseja fechar a aplicação?",
                "Gestão De Eventos Científicos",
                0,
                JOptionPane.QUESTION_MESSAGE,
                null,
                opSimNao,
                opSimNao[1]);

        final int SIM = 0;
        if (resposta == SIM) {
            dispose();
        }
    }

    /**
     * Consulta a empresa
     *
     * @return empresa
     */
    public static Empresa getEmpresa() {
        return m_empresa;
    }

    /**
     * Mostra ao utilizador uma JOptionPane com uma combo box onde são
     * apresentados eventos e devolve o evento escolhido pelo utilizador.
     *
     * @param nomesEventos vetor com a descrição dos eventos
     * @return evento escolhido pelo utilizador
     */
    private Evento selecionaEvento(String[] nomesEventos) {

        if (nomesEventos.length > 0) {
            // altera o texto do botão Cancelar da classe JOptionPane
            UIManager.put("OptionPane.cancelButtonText", "Cancelar");

            String titulo = (String) JOptionPane.showInputDialog(Window.this,
                    "Escolha o Evento:", "Rever Artigo",
                    JOptionPane.PLAIN_MESSAGE, null, nomesEventos, nomesEventos[0]);
            if (titulo != null) {
                titulo = titulo.replace("Evento:", "").trim();
                return raController.getEvento(titulo);
            }
        } else {
            JOptionPane.showMessageDialog(Window.this,
                    "Não há eventos que onde seja possível rever artigos.",
                    "Rever Artigo",
                    JOptionPane.WARNING_MESSAGE);
        }

        return null;
    }

    /**
     * Mostra ao utilizador uma JOptionPane com uma combo box onde são
     * apresentados artigos e devolve o artigo escolhido pelo utilizador.
     *
     * @param nomesArtigos vetor com a descrição dos artigos
     * @return artigo escolhido pelo utilizador
     */
    private Artigo selecionaArtigo(String[] nomesArtigos) {

        if (nomesArtigos.length > 0) {
            // altera o texto do botão Cancelar da classe JOptionPane
            UIManager.put("OptionPane.cancelButtonText", "Cancelar");

            String titulo = (String) JOptionPane.showInputDialog(Window.this,
                    "Escolha o Artigo:", "Rever Artigo",
                    JOptionPane.PLAIN_MESSAGE, null, nomesArtigos, nomesArtigos[0]);
            if (titulo != null) {
                titulo = titulo.replace("Artigo:", "").trim();
                return raController.getArtigo(titulo);
            }
        } else {
            JOptionPane.showMessageDialog(Window.this,
                    "Não há artigos possíveis de rever.",
                    "Rever Artigo",
                    JOptionPane.WARNING_MESSAGE);
        }

        return null;
    }

    private JMenuItem criarItemCriarCP() {
        JMenuItem item = new JMenuItem("Criar CP:", KeyEvent.VK_P);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String strId = introduzID();
                List<Evento> listaEventos = cpController.getEventosOrganizador(strId);
                if (listaEventos != null) {
                    Evento ev = selecionaEvento(RegistarEvento.listaNomesDeEventos(listaEventos));
                    if (ev != null) {
                        cpController.selectEvento(ev);
                        DialogCriarCP dialog = new DialogCriarCP(Window.this, m_empresa);

                    }
                }
            }
        });
        return item;
    }

    /**
     * Solicita o id ao utilizador e devolve os eventos em que este é revisor.
     *
     * @return eventos em que o utilizador é revisor
     */
    private String introduzID() {
        try {
            // altera o texto do botão Cancelar da classe JOptionPane
            UIManager.put("OptionPane.cancelButtonText", "Cancelar");
            String id = JOptionPane.showInputDialog(Window.this, "Intoduza ID: ",
                    "Rever Artigo", JOptionPane.PLAIN_MESSAGE);
            if (id != null) {
                return id;
            }
        } catch (ElementoInvalidoException excecao) {
            JOptionPane.showMessageDialog(Window.this, excecao.getMessage(),
                    "INtroduza um id valido !", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    private int selecionaModoPagamento() {
        String n = JOptionPane.showInputDialog("Introduza o método de pagamento (1.CanadaExpress ou 2.VisaoLight) :");
        return Integer.parseInt(n);

    }
}
