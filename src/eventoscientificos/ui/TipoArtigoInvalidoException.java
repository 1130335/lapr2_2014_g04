/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 *
 * @author Vaio
 */
class TipoArtigoInvalidoException extends Exception {

    public TipoArtigoInvalidoException() {
        super("Tipo de Artigo inválido!");
    }
    public TipoArtigoInvalidoException(String mensagem) {
        super(mensagem);
    }
    
}
