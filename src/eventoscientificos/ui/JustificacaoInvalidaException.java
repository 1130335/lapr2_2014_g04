/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

/**
 *
 * @author ASUS
 */
class JustificacaoInvalidaException extends IllegalArgumentException {

    public JustificacaoInvalidaException() {
        super("Erro.");
    }

    public JustificacaoInvalidaException(String mensagem) {
        super(mensagem);
    }
}
