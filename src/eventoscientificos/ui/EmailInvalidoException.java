/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 *Excepção criada caso não seja introduzido nenhum email
 * @author Sofia
 */
class EmailInvalidoException extends IllegalArgumentException {

    public EmailInvalidoException(){
        super("Email introduzido inválido!");
    }
    
    public EmailInvalidoException (String mensagem) {
        super(mensagem);
    }
    
}
