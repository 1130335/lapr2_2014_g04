/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 * Excepção criada caso não seja introduzido titulo
 * @author Sofia
 */
class TituloInvalidoException extends IllegalArgumentException {

    public TituloInvalidoException() {
        super("Título Inválido!");
    }
    
    public TituloInvalidoException (String mensagem) {
       super(mensagem);
    }
    
}
