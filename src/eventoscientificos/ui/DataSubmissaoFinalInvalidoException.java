/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 *  *Excepção criada caso não seja introduzida data de submissão final

 * @author Sofia
 */
class DataSubmissaoFinalInvalidoException extends IllegalArgumentException {

    public DataSubmissaoFinalInvalidoException() {
        super("Data de Submissão do Artigo Final inválida!");
    }
    
    public DataSubmissaoFinalInvalidoException(String mensagem) {
        super(mensagem);
    }
}
