/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Vaio
 */
class DialogDadosArtigoFinal extends JDialog{
    private String m_titulo;
    private String m_tipo;
    private String m_resumo;
    private JTextField jtf1;
    private JTextField jtf2;
    private JComboBox jcb;
    
    public DialogDadosArtigoFinal(JFrame frame) {
        super(frame, "Submeter Artigo Final", true);
        JPanel p1 = preenchimentoDados();
        JPanel p2 = painelBotoes();
        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);
        setPreferredSize(new Dimension(500, 300));
        setLocation(0, 0);
        setVisible(true);
        pack();
    }

    DialogDadosArtigoFinal(Window aThis) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    public JPanel preenchimentoDados() {
        JPanel p = new JPanel(new GridLayout(4, 1));
        JPanel p1 = painelTitulo();
        JPanel p2 = painelTipo();
        JPanel p3 = painelResumo();
        p.add(p1);
        p.add(p2);
        p.add(p3);
        return p;
    }
    
     public JPanel painelTitulo() {
        JPanel p = new JPanel();
        this.jtf1 = new JTextField(10);
        JLabel jl = new JLabel("Título do Artigo: ");
        p.add(jl);
        p.add(jtf1);
        return p;
    }
      public JPanel painelResumo() {
        JPanel p = new JPanel();
        this.jtf2 = new JTextField(20);
        JLabel jl = new JLabel("Resumo do Artigo: ");
        p.add(jl);
        p.add(jtf2);
        return p;
    }
      
      public JPanel painelTipo() {
        JPanel p = new JPanel();
        String[] tipo = {"Full", "Short", "Poster"};
        this.jcb = new JComboBox(tipo);
        JLabel jl = new JLabel("Tipo: ");
        p.add(jl);
        p.add(jcb);
        return p;
    }
    
      public JPanel painelBotoes() {
        JPanel p = new JPanel();
        JButton bOK = botaoOK();
        JButton bCan = botaoCancelar();
        p.add(bOK);
        p.add(bCan);
        return p;
    }

    public JButton botaoOK() {
        JButton b1 = new JButton("OK");
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                m_titulo = jtf1.getText();
                jcb.setSelectedIndex(-1);
                jcb.setMaximumRowCount(3);
                jcb.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        m_tipo = (String) jcb.getSelectedItem();

                    }
                });
                m_tipo = (String) jcb.getSelectedItem();
                m_resumo = jtf2.getText();
                
                dispose();
            }
        });
        return b1;
    }

    public JButton botaoCancelar() {
        JButton b1 = new JButton("Cancelar");
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return b1;

    }
    
    public String getTituto () {
        return this.m_titulo;
    }
    
    public String getTipo () {
        return this.m_tipo;
    }
    
    public String getResumo () {
        return this.m_resumo;
    }
    
}
