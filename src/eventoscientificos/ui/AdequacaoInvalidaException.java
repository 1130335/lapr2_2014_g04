/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

/**
 *
 * @author 1130429
 */
public class AdequacaoInvalidaException extends IllegalArgumentException {

    public AdequacaoInvalidaException() {
        super("Erro.");
    }

    public AdequacaoInvalidaException(String mensagem) {
        super(mensagem);
    }
}
