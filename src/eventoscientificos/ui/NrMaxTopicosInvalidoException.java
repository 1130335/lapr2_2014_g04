/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 * Excepção criada caso não seja introduzid número máximo de topicos
 * @author Sofia
 */
class NrMaxTopicosInvalidoException extends IllegalArgumentException {

    public NrMaxTopicosInvalidoException() {
        super("Número máximo de Tópicos inválido!");
    }
    
    public NrMaxTopicosInvalidoException(String mensagem) {
        super(mensagem);
    }
}
