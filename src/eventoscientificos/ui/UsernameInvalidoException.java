/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 * Excepção criada caso não seja introduzido username
 * @author Sofia
 */
class UsernameInvalidoException extends IllegalArgumentException {

    public UsernameInvalidoException() {
        super("Username Inválido!");
    }
    
    public UsernameInvalidoException (String mensagem){
        super(mensagem);
    }
    
}
