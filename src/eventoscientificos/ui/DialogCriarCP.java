/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

import eventoscientificos.Empresa;
import eventoscientificos.Revisor;
import eventoscientificos.controller.CriarCPController;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author 1130429
 */
public class DialogCriarCP extends JDialog{
    private JTextField txtIdRevisor;
    private JTextArea txtJustificacao;
    private JButton btnOk;
    private Empresa empresa;

    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private static final Dimension LBL_TAMANHO = new JLabel("Id do revisor : ").getPreferredSize();

    public DialogCriarCP(Frame pai, Empresa empresa) {
        super(pai, "Criar CP", true);
       
        this.empresa = empresa;
 
        JPanel p2 = criarPainelCentro();
        JPanel p3 = criarPainelBotoes();
        add(p2, BorderLayout.CENTER);
        add(p3, BorderLayout.SOUTH);
        
        getRootPane().setDefaultButton(btnOk);
        setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);
        getRootPane().setDefaultButton(btnOk);

    }

   

    private JPanel criarPainelCentro() {
        JPanel painel = new JPanel(new BorderLayout());

       
        painel.add(criarPainelId(), BorderLayout.CENTER);
        

        return painel;
    }

    private JPanel criarPainelId() {
        final int CAMPO_LARGURA = 20;
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;

        JLabel lbl = new JLabel("ID do revisor: ", FlowLayout.RIGHT);
        lbl.setPreferredSize(LBL_TAMANHO);

        this.txtIdRevisor = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtIdRevisor);

        return p;
    }


    private JPanel criarPainelBotao() {
        btnOk = criarBotaoOK();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOk);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    public JButton criarBotaoOK() {
        btnOk = new JButton("OK");
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String id = txtIdRevisor.getText();
                    if (id.isEmpty()) {
                        throw new ElementoInvalidoException();
                    }
                    CriarCPController cpController = new CriarCPController(empresa);
                    Revisor r = cpController.addMembroCP(id);
                    
                } catch (ElementoInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogCriarCP.this,
                            "Tem que introduzir o id do Revisor.",
                            "Rever Artigo",
                            JOptionPane.WARNING_MESSAGE);
                    txtIdRevisor.requestFocus();
                } 
            }
        });
        return btnOk;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

}
