/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 * *Excepção criada caso não seja introduzida data de inicio

 * @author Sofia
 */
class DataInicioInvalidoException extends IllegalArgumentException {

    public DataInicioInvalidoException() {
        super("Data de Início Inválida!");
    }
    
    public DataInicioInvalidoException(String mensagem) {
        super(mensagem);
    }
    
}
