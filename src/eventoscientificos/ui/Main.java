package eventoscientificos.ui;

import eventoscientificos.*;
import eventoscientificos.controller.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {

            Empresa empresa = new Empresa();
            Utilizador r1 = empresa.getRegistarUtilizadores().novoUtilizador();
            r1.setEmail("mm@bd.com");
            r1.setUsername("mightymouse");
            r1.setNome("Mighty Mouse");
            Utilizador r2 = empresa.getRegistarUtilizadores().novoUtilizador();
            r2.setEmail("rr@bd.com");
            r2.setUsername("rogerrabit");
            r2.setNome("Roger Rabit");
            Utilizador r3 = empresa.getRegistarUtilizadores().novoUtilizador();
            r3.setEmail("bb@bd.com");
            r3.setUsername("bugsbunny");
            r3.setNome("Bugs Bunny");
            Utilizador o1 = empresa.getRegistarUtilizadores().novoUtilizador();
            o1.setEmail("sotsdirlabs@etsetb.upc.edu");
            o1.setUsername("ramonbragos");
            o1.setNome("Ramon Bragos");
            empresa.getRegistarUtilizadores().addUtilizador(r1);
            empresa.getRegistarUtilizadores().addUtilizador(r2);
            empresa.getRegistarUtilizadores().addUtilizador(r3);
            empresa.getRegistarUtilizadores().addUtilizador(o1);
            Organizador o = new Organizador("sotsdirlabs@etsetb.upc.edu", o1);
            Evento e = empresa.getRegistarEventos().novoEvento();
            e.setTitulo("10th International CDIO Conference");
            e.setLocal("UPC, Universitat Politecnica de Catalunya");
            e.setCidade("Barcelona");
            e.setPais("Espanha");
            e.setDataInicio("15/06/2014");
            e.setDataFim("19/06/2014");
            e.addOrganizador(o);
            e.setDataLimiteSubmissao("02/02/2014");
            e.setDataLimiteRevisao("21/03/2014");
            e.setDataLimiteRegisto("25/04/2014");
            e.setDataLimiteSubmissaoFinal("20/04/2014");
            CP cp = e.novaCP();
            cp.addMembroCP("mm@bd.com", r1);
            cp.addMembroCP("rr@bd.com", r2);
            cp.addMembroCP("bb@bd.com", r3);
            e.setCP(cp);
            Artigo a1 = new Artigo();
            Utilizador autor1 = empresa.getRegistarUtilizadores().novoUtilizador();
            autor1.setEmail("C.Ortega@curtin.edu.au");
            autor1.setUsername("cosanchez");
            autor1.setNome("Cesar Ortega-Sanchez");
            empresa.getRegistarUtilizadores().addUtilizador(autor1);
            a1.setTitulo("CURTIN ROBOTICS CLUB: CONCEIVING, DESIGNING,IMPLEMENTING AND OPERATING ROBOTS FOR FUN!");
            Autor aut1 = a1.novoAutor("Cesar Ortega-Sanchez", "Curtin University, Western Australia\n"
                    + "K.3.2 COMPUTERS AND EDUCATION/Computer and Information Science Education ", "C.Ortega@curtin.edu.au", autor1);
            a1.addAutor(aut1);
            a1.setListaRevisores(e.getCP().getListaRevisores());
            Revisao rev1 = new Revisao();
            rev1.setRevisor("mm@bd.com", a1);
            rev1.setRecomendacao("Aceite");
            rev1.setConfianca(4);
            rev1.setAdequacao(5);
            rev1.setOriginalidade(4);
            rev1.setQualidade(4);
            rev1.setJustificacao("It's a nice guy with a fun paper!");
            Revisao rev2 = new Revisao();
            rev2.setRevisor("rr@bd.com", a1);
            rev2.setRecomendacao("Aceite");
            rev2.setConfianca(5);
            rev2.setAdequacao(4);
            rev2.setOriginalidade(3);
            rev2.setQualidade(3);
            rev2.setJustificacao("It's too much fun for a serious paper!");
            Revisao rev3 = new Revisao();
            rev3.setRevisor("bb@bd.com", a1);
            rev3.setRecomendacao("Aceite");
            rev3.setConfianca(5);
            rev3.setAdequacao(5);
            rev3.setOriginalidade(4);
            rev3.setQualidade(5);
            rev3.setJustificacao("A funny paper!");
            a1.addRevisao(rev1);
            a1.addRevisao(rev2);
            a1.addRevisao(rev3);
            Submissao s1 = e.novaSubmissao();
            s1.setArtigo(a1);
            e.addSubmissao(s1);
            Artigo a2 = new Artigo();
            a2.setTitulo("INTEGRATING MULTI-DISCIPLINARY ENGINEERING PROJECTS WITH ENGLISH ON A STUDY-ABROAD PROGRAM");
            Utilizador autor2 = empresa.getRegistarUtilizadores().novoUtilizador();
            autor2.setEmail("avindaw@op.ac.nz");
            autor2.setUsername("avindaweerakoon");
            autor2.setNome("Avinda Weerakoon");
            empresa.getRegistarUtilizadores().addUtilizador(autor2);
            Autor aut2 = a2.novoAutor("Avinda Weerakoon", "Architecture, Building and Engineering, Otago Polytechnic\n"
                    + "K.3.2 COMPUTERS AND EDUCATION/Computer and Information Science ", "avindaw@op.ac.nz", autor2);
            a2.addAutor(aut2);
            Autor aut3 = a2.novoAutor("Nathan Dunbar", "Architecture, Building and Engineering, Otago Polytechnic\n"
                    + "K.3.2 COMPUTERS AND EDUCATION/Computer and Information Science ", "dunbar@op.ac.nz");
            a2.addAutor(aut3);
            Autor aut4 = a2.novoAutor("John Findlay", "Architecture, Building and Engineering, Otago Polytechnic\n"
                    + "K.3.2 COMPUTERS AND EDUCATION/Computer and Information Science ", "findlay@op.ac.nz");
            a2.addAutor(aut4);
            a2.setAutorCorrespondente(aut2);
            a2.setListaRevisores(e.getCP().getListaRevisores());
            Submissao s2 = e.novaSubmissao();
            s2.setArtigo(a2);
            e.addSubmissao(s2);
            Topico t1 = new Topico();
            t1.setDescricao("Descricao1");
            Topico t2 = new Topico();
            t2.setDescricao("Descricao2");
            Topico t3 = new Topico();
            t3.setDescricao("Descricao3");
            e.addTopico(t1);
            e.addTopico(t2);
            e.addTopico(t3);
            empresa.getRegistarEventos().addEvento(e);

            Window j = new Window(empresa);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
