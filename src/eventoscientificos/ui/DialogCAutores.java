/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.Autor;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Rita
 */
public class DialogCAutores extends JDialog {

    private String m_nome;
    private String m_afiliacao;
    private String m_email;
    private JTextField m_text1;
    private JTextField m_text2;
    private JTextField m_text3;
    private boolean m_opcao;
    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;

    public DialogCAutores(Frame frame) {
        super(frame, "Cria Autor", true);
        JPanel p1 = preenchimentoDados();
        JPanel p2 = painelBotoes();
        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);
        setLocation(frame.getX() + DIALOGO_DESVIO_X, frame.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);
    }

    public JPanel preenchimentoDados() {
        JPanel p = new JPanel(new GridLayout(3, 1));
        JPanel p1 = painelNome();
        JPanel p2 = painelAfiliacao();
        JPanel p3 = painelEmail();
        p.add(p1);
        p.add(p2);
        p.add(p3);
        return p;
    }

    public JPanel painelNome() {
        JPanel p = new JPanel();
        JLabel lb = new JLabel("Nome do Autor: ");
        this.m_text1 = new JTextField(20);
        p.add(lb);
        p.add(m_text1);
        return p;
    }

    public JPanel painelAfiliacao() {
        JPanel p = new JPanel();
        JLabel lb = new JLabel("Afiliação do Autor: ");
        this.m_text2 = new JTextField(20);
        p.add(lb);
        p.add(this.m_text2);
        return p;
    }

    public JPanel painelEmail() {
        JPanel p = new JPanel();
        JLabel lb = new JLabel("Email do Autor: ");
        this.m_text3 = new JTextField(20);
        p.add(lb);
        p.add(this.m_text3);
        return p;
    }

    public JPanel painelBotoes() {
        JPanel p = new JPanel();
        JButton bOK = botaoOK();
        JButton bCan = botaoCancelar();
        p.add(bOK);
        p.add(bCan);
        return p;
    }

    public JButton botaoOK() {
        JButton b1 = new JButton("OK");
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                m_nome = m_text1.getText();
                m_afiliacao = m_text2.getText();
                m_email = m_text3.getText();
                String[] opSimNao = {"Sim", "Não"};
                int resposta = JOptionPane.showOptionDialog(DialogCAutores.this,
                        "Deseja introduzir outro autor?",
                        "Cria Autor",
                        0,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        opSimNao,
                        opSimNao[1]);
                if (resposta == 0) {
                    m_opcao = false;
                    m_text1.setText("");
                    m_text2.setText("");
                    m_text3.setText("");
                } else {
                    m_opcao = true;
                    dispose();
                }
            }
        });
        return b1;
    }

    public boolean getOpcao() {
        return m_opcao;
    }

    public JButton botaoCancelar() {
        JButton b1 = new JButton("Cancelar");
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return b1;

    }

    public String getM_nome() {
        return m_nome;
    }

    public String getM_afiliacao() {
        return m_afiliacao;
    }

    public String getM_email() {
        return m_email;
    }

}
