/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 *Excepção criada caso não seja introduzida local
 * @author Sofia
 */
class LocalInvalidoException extends IllegalArgumentException {

    public LocalInvalidoException() {
        super("Local Inválido!");
    }
    
    public LocalInvalidoException (String mensagem) {
        super(mensagem);
    }
    
}
