/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.CriarEventoCientificoController;
import eventoscientificos.Empresa;
import eventoscientificos.Evento;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Sofia
 */
public class DialogCriarEvento extends JDialog {

    private JTextField txtTitulo, txtDescrição, txtDataInicio, txtLocal, txtDataFim,
            txtDataLimiteSubmissao, txtNrMaxTopicos, txtDataRevisao, txtDataLimiteSubmissaoFinal,
            txtDataLimiteRegisto,txtUsername, txtEmail;

    private Empresa empresa;

    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private static final Dimension LABEL_TAMANHO = new JLabel("Data Limite de Submissão Final (dd/mm/aaaa): ").getPreferredSize();
    private Empresa m_empresa;
    private Dimension LABEL_TAMANHO_3;

    public DialogCriarEvento(Frame pai, Empresa empresa) {

        super(pai, "Criar Evento Cientifico", true);
        this.empresa = empresa;
        JPanel p1 = painelPrincipal();
        JPanel p2 = criarPainelBotoes();
        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.SOUTH);

        setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);
        this.m_empresa = empresa;

    }

    /***
     * Cria Painel referente ao titulo
     * @return painel
     */
    private JPanel criarPainelTítulo() {
        JLabel lbl = new JLabel("Título:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtTitulo = new JTextField(CAMPO_LARGURA);
        txtTitulo.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtTitulo);

        return p;
    }
/**
 * Cria painel referente à descrição
 * @return painel
 */
    private JPanel criarPainelDescrição() {
        JLabel lbl = new JLabel("Descrição:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtDescrição = new JTextField(CAMPO_LARGURA);
        txtDescrição.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtDescrição);

        return p;
    }

    /**
     * Cria painel referente ao local
     * @return painel
     */
    private JPanel criarPainelLocal() {
        JLabel lbl = new JLabel("Local:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtLocal = new JTextField(CAMPO_LARGURA);
        txtLocal.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtLocal);

        return p;
    }
/**
     * Cria painel referente à data de inicio
     * @return painel
     */
    private JPanel criarPainelDataInicio() {
        JLabel lbl = new JLabel("Data Inicio (dd/mm/aaaa):", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtDataInicio = new JTextField(CAMPO_LARGURA);
        txtDataInicio.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtDataInicio);

        return p;
    }

    /**
     * Cria painel referente a data fim
     * @return painel
     */
    private JPanel criarPainelDataFim() {
        JLabel lbl = new JLabel("Data Fim (dd/mm/aaaa):", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtDataFim = new JTextField(CAMPO_LARGURA);
        txtDataFim.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtDataFim);

        return p;
    }
/**
     * Cria painel referente a data limite submissao
     * @return painel
     */
    private JPanel criarPainelDataLimiteSubmissao() {
        JLabel lbl = new JLabel("Data Limite de Submissão (dd/mm/aaaa):", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtDataLimiteSubmissao = new JTextField(CAMPO_LARGURA);
        txtDataLimiteSubmissao.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtDataLimiteSubmissao);

        return p;
    }

    /**
     * Cria painel referente ao nr max de topicos
     * @return painel
     */
    private JPanel criarPainelNrMaxTopicos() {
        JLabel lbl = new JLabel("Número Máximo de Tópicos:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtNrMaxTopicos = new JTextField(CAMPO_LARGURA);
        txtNrMaxTopicos.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtNrMaxTopicos);

        return p;
    }

   /**
     * Cria painel referente a data de revisao
     * @return painel
     */
    private JPanel criarPainelDataRevisao() {
        JLabel lbl = new JLabel("Data Limite de Revisão (dd/mm/aaaa):", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtDataRevisao = new JTextField(CAMPO_LARGURA);
        txtDataRevisao.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtDataRevisao);

        return p;
    }

    /**
     * Cria painel referente a data limite submissao final
     * @return painel
     */
    private JPanel criarPainelDataLimiteSubmissaoFinal() {
        JLabel lbl = new JLabel("Data Limite Submissão Final (dd/mm/aaaa):", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtDataLimiteSubmissaoFinal = new JTextField(CAMPO_LARGURA);
        txtDataLimiteSubmissaoFinal.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtDataLimiteSubmissaoFinal);

        return p;
    }
/**
     * Cria painel referente data limite de registo
     * @return painel
     */
    private JPanel criarPainelDataLimiteRegisto() {
        JLabel lbl = new JLabel("Data Limite de Registo (dd/mm/aaaa):", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtDataLimiteRegisto = new JTextField(CAMPO_LARGURA);
        txtDataLimiteRegisto.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtDataLimiteRegisto);

        return p;
    }

   /**
    * Adiciona botoes ao painel principal
    * @return painel
    */
    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    /**
     * Junção de todos os paineis
     * @return painel 
     */
    private JPanel painelPrincipal() {
        JPanel p = new JPanel(new GridLayout(10, 1));

        JPanel p1 = criarPainelTítulo();
        JPanel p2 = criarPainelDescrição();
        JPanel p3 = criarPainelLocal();
        JPanel p4 = criarPainelDataInicio();
        JPanel p5 = criarPainelDataFim();
        JPanel p6 = criarPainelDataLimiteSubmissao();
        JPanel p7 = criarPainelNrMaxTopicos();
        JPanel p8 = criarPainelDataRevisao();
        JPanel p9 = criarPainelDataLimiteSubmissaoFinal();
        JPanel p10 = criarPainelDataLimiteRegisto();


        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        p.add(p5);
        p.add(p6);
        p.add(p8);
        p.add(p9);
        p.add(p10);
        p.add(p7);

        return p;
    }

    /**
     * Cria o botão ok
     * @return botão ok
     */
    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String titulo = txtTitulo.getText();
                    if (titulo.isEmpty()) {
                        throw new TituloInvalidoException();
                    }
                    String descricao = txtDescrição.getText();
                    if (descricao.isEmpty()) {
                        throw new DescricaoInvalidoException();
                    }
                    String local = txtLocal.getText();
                    if (local.isEmpty()) {
                        throw new LocalInvalidoException();
                    }
                    String dataInicio = txtDataInicio.getText();
                    if (dataInicio.isEmpty()) {
                        throw new DataInicioInvalidoException();
                    }
                    String dataFim = txtDataFim.getText();
                    if (dataFim.isEmpty()) {
                        throw new DataFimInvalidoException();
                    }
                    String dataLimiteSubmissao = txtDataLimiteSubmissao.getText();
                    if (dataLimiteSubmissao.isEmpty()) {
                        throw new DataLimiteSubmissaoInvalidoException();
                    }
                    String dataRevisao = txtDataRevisao.getText();
                    if (dataRevisao.isEmpty()) {
                        throw new DataRevisaoInvalidoException();
                    }
                    String dataLimiteSubmissaoFinal = txtDataLimiteSubmissaoFinal.getText();
                    if (dataLimiteSubmissaoFinal.isEmpty()) {
                        throw new DataSubmissaoFinalInvalidoException();
                    }
                    String dataLimiteRegisto = txtDataLimiteRegisto.getText();
                    if (dataLimiteRegisto.isEmpty()) {
                        throw new DataLimiteRegistoInvalidoException();
                    }
                    if (txtNrMaxTopicos.getText().isEmpty()) {
                        throw new NrMaxTopicosInvalidoException();
                    }

                    int nrMaxTopicos = Integer.parseInt(txtNrMaxTopicos.getText());

                    if (!validarDatas(dataInicio)) {
                        throw new DataInicioInvalidoException();
                    }
                    if (!validarDatas(dataFim)) {
                        throw new DataFimInvalidoException();
                    }
                    if (!validarDatas(dataLimiteSubmissao)) {
                        throw new DataLimiteSubmissaoInvalidoException();
                    }
                    if (!validarDatas(dataRevisao)) {
                        throw new DataRevisaoInvalidoException();
                    }
                    if (!validarDatas(dataLimiteSubmissaoFinal)) {
                        throw new DataSubmissaoFinalInvalidoException();
                    }
                    if (!validarDatas(dataLimiteRegisto)) {
                        throw new DataLimiteRegistoInvalidoException();
                    }
                    Evento ev = new Evento();
                    if (!ev.validaDatas(dataInicio, dataFim, dataRevisao, dataLimiteSubmissaoFinal,
                            dataLimiteRegisto, dataLimiteSubmissao)) {
                        throw new OrdemDatasInvalidoException();
                    }

                    CriarEventoCientificoController cec = new CriarEventoCientificoController(empresa);
                    cec.novoEvento();
                    cec.setTitulo(titulo);
                    cec.setDescricao(descricao);
                    cec.setLocal(local);
                    cec.setDataInicio(dataInicio);
                    cec.setDataFim(dataFim);
                    cec.setDataLimiteSubmissão(dataLimiteSubmissao);
                    cec.setDataLimiteRevisao(dataLimiteRegisto);
                    cec.setDataLimiteSubmissaoFinal(dataLimiteSubmissaoFinal);
                    cec.setDataLimiteRegisto(dataLimiteRegisto);
                    cec.setNrMaxTopicos(nrMaxTopicos);

                     String org= null;
                    int cont=0;
                    int resposta= 0;
                    do{
                        org= JOptionPane.showInputDialog(DialogCriarEvento.this, "Organizador a adicionar:",
                                "Adicionar Organizador", JOptionPane.INFORMATION_MESSAGE);
                        if(org==null) {
                            throw new EscolhaCanceladaException();
                        }
                        if(org.isEmpty()) {
                            throw new IDInvalidoException();
                        }
                        boolean f=cec.addOrganizador(org);
                        
                        if(f) {
                            JOptionPane.showMessageDialog(DialogCriarEvento.this,"Organizador adicionado com sucesso!", "Adicionar Organizador",
                                    JOptionPane.INFORMATION_MESSAGE);
                            cont++;
                        } else {
                            JOptionPane.showMessageDialog(DialogCriarEvento.this,"Organizador não adicionado!", "Adicionar Organizador",
                                    JOptionPane.INFORMATION_MESSAGE);
                        }
                        String[] opSimNao= {"Sim","Não"};
                        resposta= JOptionPane.showOptionDialog(DialogCriarEvento.this,
                                "Deseja adicionar mais algum Organizador?",
                                "Adicionar Organizador",
                                0,
                                JOptionPane.QUESTION_MESSAGE,
                                null,
                                opSimNao,
                                opSimNao[1]);
                    }while (resposta ==0);
                    if(cont>0) {
                        cec.registaEvento();
                    }
                    dispose();
                    
                

                } catch (TituloInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogCriarEvento.this,
                            "Tem que introduzir um Título válido.",
                            "Criação de Evento Cientifico.",
                            JOptionPane.WARNING_MESSAGE);
                    txtTitulo.requestFocus();
                } catch (DescricaoInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogCriarEvento.this,
                            "Tem que introduzir uma Descrição válida.",
                            "Criação de Evento Cientifico.",
                            JOptionPane.WARNING_MESSAGE);
                    txtDescrição.requestFocus();
                } catch (LocalInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogCriarEvento.this,
                            "Tem que introduzir um Local válido.",
                            "Criação de Evento Cientifico.",
                            JOptionPane.WARNING_MESSAGE);
                    txtLocal.requestFocus();
                } catch (DataInicioInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogCriarEvento.this,
                            "Tem que introduzir uma Data de Início válida.",
                            "Criação de Evento Cientifico.",
                            JOptionPane.WARNING_MESSAGE);
                    txtDataInicio.requestFocus();
                } catch (DataFimInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogCriarEvento.this,
                            "Tem que introduzir uma Data de Fim válida.",
                            "Criação de Evento Cientifico.",
                            JOptionPane.WARNING_MESSAGE);
                    txtDataFim.requestFocus();
                } catch (DataLimiteSubmissaoInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogCriarEvento.this,
                            "Tem que introduzir uma Data de Submissão do Artigo para Revisão válida.",
                            "Criação de Evento Cientifico.",
                            JOptionPane.WARNING_MESSAGE);
                    txtDataLimiteSubmissao.requestFocus();
                } catch (DataRevisaoInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogCriarEvento.this,
                            "Tem que introduzir uma Data de Revisão válida.",
                            "Criação de Evento Cientifico.",
                            JOptionPane.WARNING_MESSAGE);
                    txtDataRevisao.requestFocus();
                } catch (DataSubmissaoFinalInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogCriarEvento.this,
                            "Tem que introduzir uma Data Limite de Submissão do Artigo Final válida.",
                            "Criação de Evento Cientifico.",
                            JOptionPane.WARNING_MESSAGE);
                    txtDataLimiteSubmissaoFinal.requestFocus();
                } catch (DataLimiteRegistoInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogCriarEvento.this,
                            "Tem que introduzir uma Data de Registo no Evento válida.",
                            "Criação de Evento Cientifico.",
                            JOptionPane.WARNING_MESSAGE);
                    txtDataLimiteRegisto.requestFocus();
                } catch (NrMaxTopicosInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogCriarEvento.this,
                            "Tem que introduzir um Número de Tópicos válido.",
                            "Criação de Evento Cientifico.",
                            JOptionPane.WARNING_MESSAGE);
                    txtNrMaxTopicos.requestFocus();
                } catch (NumberFormatException excecao) {
                    JOptionPane.showMessageDialog(DialogCriarEvento.this,
                            "Formato inválido, deve introduzir as Datas e o Número Máximo de Tópicos em formato numérico.",
                            "Criação de Evento Cientifico.",
                            JOptionPane.WARNING_MESSAGE);
                } catch (OrdemDatasInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogCriarEvento.this,
                            "Ordem das datas inválida!",
                            "Criação de Evento Cientifico.",
                            JOptionPane.WARNING_MESSAGE);
                 } catch (IDInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogCriarEvento.this,
                            "ID inválido!",
                            "Adicionar Organizador",
                            JOptionPane.WARNING_MESSAGE);
                } catch (EscolhaCanceladaException excecao) {
                    JOptionPane.showMessageDialog(DialogCriarEvento.this,
                            "Tem de introduzir pelo menos um organizador!",
                            "Adicionar Organizador",
                            JOptionPane.WARNING_MESSAGE);
                }

            }
        });
        return btn;
    }

    /**
     * Valida as datas
     * @param data
     * @return true caso as datas sejam validas
     */
    public boolean validarDatas(String data) {

        Evento e = new Evento();
        String aux[] = data.split("/");
        Integer Dia = Integer.parseInt(aux[0]);
        Integer Mes = Integer.parseInt(aux[1]);
        Integer Ano = Integer.parseInt(aux[2]);

        return ((Mes.equals(1) || Mes.equals(3) || Mes.equals(5) || Mes.equals(7) || Mes.equals(8) || Mes.equals(10) || Mes.equals(12)) && (Dia >= 1 && Dia <= 31))
                || ((Mes.equals(6) || Mes.equals(4) || Mes.equals(9) || Mes.equals(11)) && (Dia >= 1 && Dia <= 30))
                || ((Dia >= 1 && Dia <= 29) && (Mes.equals(2)) && (e.isAnoBissexto(Ano)))
                || ((Mes.equals(2)) && !(e.isAnoBissexto(Ano)) && (Dia >= 1 && Dia <= 28));
    }

    /**
     * Cria o botão cancelas
     * @return botão cancelas
     */
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

   

   

        }
                


