/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 *Excepção criada caso não seja introduzida data de fim
 * @author Sofia
 */
class DataFimInvalidoException extends IllegalArgumentException {

    public DataFimInvalidoException() {
        super("Data de Fim Inválida!");
    }
    
    public DataFimInvalidoException(String mensagem) {
        super(mensagem);
    }
    
}
