/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 * *Excepção criada caso não seja introduzida descrição

 * @author Sofia
 */
class DescricaoInvalidoException extends IllegalArgumentException {

    public DescricaoInvalidoException() {
        super("Descrição Inválida!");
    }
    
    public DescricaoInvalidoException(String mensagem) {
        super(mensagem);
    }
    
}
