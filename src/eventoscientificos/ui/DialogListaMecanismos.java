/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.MecanismosDecisao;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Rita
 */
public class DialogListaMecanismos extends JDialog {

    private MecanismosDecisao m_mecanismo;
    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;

    public DialogListaMecanismos(Frame window, List<MecanismosDecisao> lm) {
        super(window, "Decidir Sobre Artigo", true);
        JPanel p1 = selecionarMecanismo(lm);
        JPanel p2 = painelBotoes();
        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);
        setLocation(window.getX() + DIALOGO_DESVIO_X, window.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);

    }

    private JPanel selecionarMecanismo(final List<MecanismosDecisao> lm) {
        JLabel lbl = new JLabel("Mecanismo: ");
        String[] mec = new String[lm.size()];
        int i = 0;
        for (MecanismosDecisao m : lm) {
            mec[i] = m.getTitulo();
            i++;
        }
        final JComboBox<String> cmbSubmissoes = new JComboBox<String>(mec);
        cmbSubmissoes.setSelectedIndex(-1);
        cmbSubmissoes.setMaximumRowCount(3);
        cmbSubmissoes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String t_mec = (String) (cmbSubmissoes.getSelectedItem());
                for (MecanismosDecisao m : lm) {
                    if (m.getTitulo().equalsIgnoreCase(t_mec)) {
                        m_mecanismo = m;
                    }
                }
            }
        });

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(lbl);
        p.add(cmbSubmissoes);

        return p;
    }

    private JPanel painelBotoes() {
        JButton btnOk = new JButton("OK");
        getRootPane().setDefaultButton(btnOk);
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOk);

        return p;
    }

    public MecanismosDecisao getMecanismo() {
        return this.m_mecanismo;
    }

}
