/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

import eventoscientificos.Empresa;
import eventoscientificos.controller.RegistarAutorEventoController;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author ASUS
 */
public class DialogRegistarAutorEvento extends JDialog {
    /**
         * Data de validade do cartão, nº do cartão, valor a pagar, data limite
         * de pagamento.
         */
        private JTextField txtValidade, txtNumCC, txtValor,
                txtLimite;
        
        private JButton btnOk;
        private Empresa empresa;
        private static final int DIALOGO_DESVIO_X = 10, DIALOGO_DESVIO_Y = 10;
        private final Dimension LBL_TAMANHO = new JLabel("Data de validade do cartão (aaaa-mm-dd): ").getPreferredSize();
        private float vp;
        private Frame pai;
        private boolean Pago;
        private RegistarAutorEventoController controller;
        
        
        public DialogRegistarAutorEvento(Frame pai, Empresa empresa, float vp) {
            super(pai, "Registo no Evento", true);
            this.pai =pai;
            this.empresa=empresa;
            this.vp=vp;
            this.Pago=false;
            final int LINHAS = 5, COLUNAS = 1;
            setLayout(new BorderLayout());
            
            JPanel p1 = criarPainelNorte();
            JPanel p2 = criarPainelCentro();
            JPanel p3 = criarPainelBotao();
            add(p1, BorderLayout.NORTH);
            add(p2, BorderLayout.CENTER);
            add(p3, BorderLayout.SOUTH);
            
            getRootPane().setDefaultButton(btnOk);
            setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
            pack();
            setResizable(false);
            setVisible(true);
        }
        
        private JPanel criarPainelNorte() {
            JPanel painel = new JPanel(new BorderLayout());
            
            painel.add(criarPainelValidade(), BorderLayout.NORTH);
            painel.add(criarPainelNumCC(), BorderLayout.SOUTH);
            
            return painel;
        }
        
        private JPanel criarPainelCentro() {
            JPanel painel = new JPanel(new BorderLayout());
            
            painel.add(criarPainelValor(), BorderLayout.NORTH);
            painel.add(criarPainelLimite(), BorderLayout.CENTER);
            
            return painel;
        }
        
        private JPanel criarPainelValidade() {
            final int CAMPO_LARGURA = 20;
            final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
            final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
            
            JLabel lbl = new JLabel("Data de validade do cartão (aaaa-mm-dd): ", FlowLayout.RIGHT);
            lbl.setPreferredSize(LBL_TAMANHO);
            
            this.txtValidade = new JTextField(CAMPO_LARGURA);
            
            JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
            p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                    MARGEM_INFERIOR, MARGEM_DIREITA));
            
            p.add(lbl);
            p.add(txtValidade);
            
            return p;
        }
        
        private JPanel criarPainelNumCC() {
            final int CAMPO_LARGURA = 20;
            final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
            final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
            
            JLabel lbl = new JLabel("Numero cartão crédito: ", FlowLayout.RIGHT);
            lbl.setPreferredSize(LBL_TAMANHO);
            
            this.txtNumCC = new JTextField(CAMPO_LARGURA);
            
            JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
            p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                    MARGEM_INFERIOR, MARGEM_DIREITA));
            p.add(lbl);
            p.add(txtNumCC);
            
            return p;
        }
        
        private JPanel criarPainelValor() {
            final int CAMPO_LARGURA = 20;
            final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
            final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
            
            JLabel lbl = new JLabel("Valor a pagar (€): ", FlowLayout.RIGHT);
            lbl.setPreferredSize(LBL_TAMANHO);
            
            this.txtValor = new JTextField(CAMPO_LARGURA);
            this.txtValor.setText(String.valueOf(vp));
            this.txtValor.setEnabled(false);
            JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
            p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                    MARGEM_INFERIOR, MARGEM_DIREITA));
            
            p.add(lbl);
            p.add(txtValor);
            
            return p;
        }
        
        private JPanel criarPainelLimite() {
            final int CAMPO_LARGURA = 20;
            final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
            final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
            
            JLabel lbl = new JLabel("Data limite de pagamento (aaaa-mm-dd): ", FlowLayout.RIGHT);
            lbl.setPreferredSize(LBL_TAMANHO);
            
            this.txtLimite = new JTextField(CAMPO_LARGURA);
            
            JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
            p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                    MARGEM_INFERIOR, MARGEM_DIREITA));
            
            p.add(lbl);
            p.add(txtLimite);
            
            return p;
        }
        
        private JPanel criarPainelBotao() {
            btnOk = criarBotaoOK();
            
            JPanel p = new JPanel();
            final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
            final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
            p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                    MARGEM_INFERIOR, MARGEM_DIREITA));
            p.add(btnOk);
            
            return p;
        }
        
        public JButton criarBotaoOK() {
            btnOk = new JButton("OK");
            btnOk.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        if (confirmaPagamento()) {
                            String validade = txtValidade.getText();
                            String numCC = txtNumCC.getText();
                            String limite = txtLimite.getText();
                            String mensagem = controller.pagamento(validade, numCC, vp, limite);
                            if (mensagem != null) {
                                JOptionPane.showMessageDialog(pai,
                                        "Informação da autorização de pagamento: \n" + mensagem,
                                        "Registo no Evento",
                                        JOptionPane.INFORMATION_MESSAGE);
                                Pago = true;
                            } else {
                                JOptionPane.showMessageDialog(pai,
                                        "O seu pedido de pagamento não foi aceite.",
                                        "Registo no Evento",
                                        JOptionPane.WARNING_MESSAGE);
                            }
                            dispose();
                        }
                    } catch (ElementoInvalidoException excecao) {
                        JOptionPane.showMessageDialog(pai,
                                excecao.getMessage(),
                                "Registo no Evento",
                                JOptionPane.ERROR_MESSAGE);
                    } catch (ParseException ex) {
                        Logger.getLogger(DialogRegistarAutorEvento.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            return btnOk;
        }
         private boolean confirmaPagamento() {
            String[] opSimNao = {"Sim", "Não"};
            final int SIM = 0;
            
            int resposta = JOptionPane.showOptionDialog(pai, "Confirma pagamento?",
                    "Registo no Evento", SIM, JOptionPane.QUESTION_MESSAGE, null, opSimNao,
                    opSimNao[1]);
            
            return resposta == SIM;
        }
}
