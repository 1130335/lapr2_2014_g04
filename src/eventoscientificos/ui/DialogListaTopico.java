/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;


import eventoscientificos.Topico;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Vaio
 */
class DialogListaTopico extends JDialog{
private Topico m_topico;

    DialogListaTopico(Window aThis, List<Topico> lt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
   public DialogListaTopico(JFrame window, List<Topico> lt) {
        super(window, "Definir Tópicos de Pericia", true);
        JPanel p1 = selecionarTopico(lt);
        JPanel p2 = painelBotoes();
        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);
        setPreferredSize(new Dimension(500, 300));
        setLocation(0, 0);
        setVisible(true);
        pack();

    }

    
    private JPanel selecionarTopico(final List<Topico> lt) {
        JLabel lbl = new JLabel("Topico: ");
        String[] tp = new String[lt.size()];
        int i = 0;
        for (Topico t : lt) {
            tp[i] = t.getCodigoACM();
            i++;
        }
        final JComboBox<String> cmbTopicos = new JComboBox<String>(tp);
        cmbTopicos.setSelectedIndex(-1);
        cmbTopicos.setMaximumRowCount(3);
        cmbTopicos.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent t) {
                String t_topico = (String) (cmbTopicos.getSelectedItem());
                for (Topico topico : lt) {
                    if (topico.getCodigoACM().equalsIgnoreCase(t_topico)) {
                        m_topico = topico;
                    }
                }
            }
        });
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(lbl);
        p.add(cmbTopicos);

        return p;
    }
        
      private JPanel painelBotoes() {
        JButton btnOk = new JButton("OK");
        getRootPane().setDefaultButton(btnOk);
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOk);

        return p;
    }

    public Topico getTopico() {
        return this.m_topico;
    }  
}

