/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.Artigo;
import eventoscientificos.Submissao;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Rita
 */
public class DialogListaSubmissoes extends JDialog {

    private Submissao m_submissao;
    private Artigo m_artigo;
    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;

    public DialogListaSubmissoes(Frame window, List<Submissao> ls) {
        super(window, "Decidir Sobre Artigo", true);
        JPanel p1 = selecionarSubmissao(ls);
        JPanel p2 = painelBotoes();
        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);
        setLocation(window.getX() + DIALOGO_DESVIO_X, window.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);

    }

    private JPanel selecionarSubmissao(final List<Submissao> ls) {
        JLabel lbl = new JLabel("Submissão: ");
        String[] sub = new String[ls.size()];
        int i = 0;
        for (Submissao s : ls) {
            sub[i] = s.getArtigo().getTitulo();
            i++;
        }
        final JComboBox<String> cmbSubmissoes = new JComboBox<String>(sub);
        cmbSubmissoes.setSelectedIndex(-1);
        cmbSubmissoes.setMaximumRowCount(3);
        cmbSubmissoes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String t_artigo = (String) (cmbSubmissoes.getSelectedItem());
                for (Submissao s : ls) {
                    if (s.getArtigo().getTitulo().equalsIgnoreCase(t_artigo)) {
                        m_submissao = s;
                        m_artigo = s.getArtigo();
                    }
                }
            }
        });

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(lbl);
        p.add(cmbSubmissoes);

        return p;
    }

    private JPanel painelBotoes() {
        JButton btnOk = new JButton("OK");
        getRootPane().setDefaultButton(btnOk);
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOk);

        return p;
    }

    public Submissao getSubmissao() {
        return this.m_submissao;
    }

    public Artigo getArtigo() {
        return this.m_artigo;
    }

}
