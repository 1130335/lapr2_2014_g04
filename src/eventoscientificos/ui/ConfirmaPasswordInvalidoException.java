/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 * Excepção criada caso as duas passwords introduzidas não sejam iguais
 * @author Sofia
 */

class ConfirmaPasswordInvalidoException extends IllegalArgumentException {

    public ConfirmaPasswordInvalidoException() {
        super("Password de confirmação inválida!");
    }
    
    public ConfirmaPasswordInvalidoException(String mensagem) {
        super(mensagem);
    }
    
}
