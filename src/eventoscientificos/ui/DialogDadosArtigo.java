/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Rita
 */
public class DialogDadosArtigo extends JDialog {

    private String m_titulo;
    private String m_tipo;
    private String m_resumo;
    private String m_ficheiro;
    private JTextField jtf1;
    private JTextField jtf2;
    private JTextField jtf3;
    private JComboBox<String> m_jcb;
    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;

    public DialogDadosArtigo(Frame frame) {
        super(frame, "Submeter Artigo para Revisão", true);
        JPanel p1 = preenchimentoDados();
        JPanel p2 = painelBotoes();
        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);
        setLocation(frame.getX() + DIALOGO_DESVIO_X, frame.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);
    }

    public JPanel preenchimentoDados() {
        JPanel p = new JPanel(new GridLayout(4, 1));
        JPanel p1 = painelTitulo();
        JPanel p2 = painelTipo();
        JPanel p3 = painelResumo();
        JPanel p4 = painelFicheiro();
        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        return p;
    }

    public JPanel painelTitulo() {
        JPanel p = new JPanel();
        this.jtf1 = new JTextField(10);
        JLabel jl = new JLabel("Título do Artigo: ");
        p.add(jl);
        p.add(jtf1);
        return p;
    }

    public JPanel painelResumo() {
        JPanel p = new JPanel();
        this.jtf2 = new JTextField(20);
        JLabel jl = new JLabel("Resumo do Artigo: ");
        p.add(jl);
        p.add(jtf2);
        return p;
    }

    public JPanel painelFicheiro() {
        JPanel p = new JPanel();
        this.jtf3 = new JTextField(20);
        JLabel jl = new JLabel("Ficheiro do Artigo: ");
        p.add(jl);
        p.add(jtf3);
        return p;
    }

    public JPanel painelTipo() {
        JPanel p = new JPanel();
        String[] tipo = {"Full", "Short", "Poster"};
        m_jcb = new JComboBox<String>(tipo);
        m_jcb.setSelectedIndex(-1);
        m_jcb.setMaximumRowCount(3);
        m_jcb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                m_tipo = (String) (m_jcb.getSelectedItem());
            }
        });
        JLabel jl = new JLabel("Tipo: ");
        p.add(jl);
        p.add(m_jcb);
        return p;
    }

    public JPanel painelBotoes() {
        JPanel p = new JPanel();
        JButton bOK = botaoOK();
        JButton bCan = botaoCancelar();
        p.add(bOK);
        p.add(bCan);
        return p;
    }

    public JButton botaoOK() {
        JButton b1 = new JButton("OK");
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                m_titulo = jtf1.getText();
                m_resumo = jtf2.getText();
                m_ficheiro = jtf3.getText();
                dispose();
            }
        });
        return b1;
    }

    public JButton botaoCancelar() {
        JButton b1 = new JButton("Cancelar");
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return b1;

    }

    public String getTituto() {
        return this.m_titulo;
    }

    public String getTipo() {
        return this.m_tipo;
    }

    public String getResumo() {
        return this.m_resumo;
    }

    public String getFicheiro() {
        return this.m_ficheiro;
    }

}
