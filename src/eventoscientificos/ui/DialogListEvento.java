/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

import eventoscientificos.Evento;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Vaio
 */
public class DialogListEvento extends JDialog {
    private Evento m_evento;
    
    public DialogListEvento(JFrame window, List<Evento> le) {
        super(window, "Definir Tópicos de Pericia", true);
        JPanel p1 = selecionarEvento(le);
        JPanel p2 = painelBotoes();
        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);
        setPreferredSize(new Dimension(500, 300));
        setLocation(0, 0);
        setVisible(true);
        pack();

    }

    DialogListEvento(Window aThis, boolean b, List<Evento> le) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private JPanel selecionarEvento(final List<Evento> le) {
        JLabel lbl = new JLabel("Evento: ");
        String[] ev = new String[le.size()];
        int i = 0;
        for (Evento e : le) {
            ev[i] = e.getTitulo();
            i++;
        }
        final JComboBox<String> cmbEventos = new JComboBox<String>(ev);
        cmbEventos.setSelectedIndex(-1);
        cmbEventos.setMaximumRowCount(3);
        cmbEventos.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String t_evento = (String) (cmbEventos.getSelectedItem());
                for (Evento event : le) {
                    if (event.getTitulo().equalsIgnoreCase(t_evento)) {
                        m_evento = event;
                    }
                }
            }
        });
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(lbl);
        p.add(cmbEventos);

        return p;
    }
        
      private JPanel painelBotoes() {
        JButton btnOk = new JButton("OK");
        getRootPane().setDefaultButton(btnOk);
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOk);

        return p;
    }

    public Evento getEvento() {
        return this.m_evento;
    }  
        
}
