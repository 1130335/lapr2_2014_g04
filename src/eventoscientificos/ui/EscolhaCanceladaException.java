/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 * Excepção criada caso seja escolhida a opção cancelas
 * @author Sofia
 */
class EscolhaCanceladaException extends IllegalArgumentException {

    public EscolhaCanceladaException() {
        super ("Escolha cancelada!");
    }
    
    public EscolhaCanceladaException (String mensagem) {
        super(mensagem);
    }
}
