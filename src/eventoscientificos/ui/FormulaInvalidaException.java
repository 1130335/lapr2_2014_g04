/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

/**
 *
 * @author ASUS
 */
public class FormulaInvalidaException extends IllegalArgumentException {

    public FormulaInvalidaException() {
        super("Erro.");
    }

    public FormulaInvalidaException(String mensagem) {
        super(mensagem);
    }
}
