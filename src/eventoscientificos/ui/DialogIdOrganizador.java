/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.Evento;
import eventoscientificos.Organizador;
import eventoscientificos.RegistarEvento;
import eventoscientificos.RegistarUtilizador;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Sofia
 */
public class DialogIdOrganizador extends JDialog {

    /**
     * Id do Organizador.
     */
    private static String idOrg;
    /**
     * Campo onde ser irá introduzir o id do organizador.
     */
    private JTextField txtIDOrganizador;
    /**
     * Desvio em relação à janela pai.
     */
    private static final int DIALOGO_DESVIO_X = 75, DIALOGO_DESVIO_Y = 150;
    /**
     * Tamanho da label.
     */
    private static final Dimension LABEL_TAMANHO = new JLabel("ID Organizador:").getPreferredSize();

    /**
     * Cria a janela de dialogo.
     *
     * @param pai janela pai.
     */
    public DialogIdOrganizador(Frame pai) {

        super(pai, "Distribuir Revisões de Artigo", true);

        JPanel p1 = criarPainelID();
        JPanel p2 = criarPainelBotoes();

        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);
        setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
        setSize(275, 125);
        setResizable(false);
        setVisible(true);
    }

    /**
     * Cria o painel do ID, onde se vai introduzir o ID do organizador.
     *
     * @return painel.
     */
    private JPanel criarPainelID() {
        JLabel lbl = new JLabel("ID Organizador:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 10;
        txtIDOrganizador = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(lbl);
        p.add(txtIDOrganizador);

        return p;
    }

    /**
     * Criar o painel dos botões.
     *
     * @return painel.
     */
    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    /**
     * Criar o botão ok da janela.
     *
     * @return botão.
     */
    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {

                    idOrg = txtIDOrganizador.getText();
                    if (idOrg.isEmpty()) {
                        throw new IDInvalidoException();
                    }

                    dispose();
                } catch (IDInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogIdOrganizador.this,
                            "Tem que introduzir o ID do Organizador!",
                            "Distribuir Revisões de Artigo",
                            JOptionPane.WARNING_MESSAGE);
                    txtIDOrganizador.requestFocus();
                }
            }
        });
        return btn;
    }

    /**
     * Criar o botão cancelar da janela.
     *
     * @return botão.
     */
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

    /**
     * Devolve o id do organizador.
     *
     * @return id do organizador.
     */
    public static String getIdOrg() {
        return idOrg;
    }

    /**
     * Verifica se o id do organizador é válido.
     *
     * @param id id do organizador.
     * @return true se for válido, false caso contrário.
     */
    public static boolean verificarIDOrganizador(String id) {
        for (Evento e : Window.getEmpresa().getRegistarEventos().getListaEventos()) {
            for (Organizador o : e.getListaOrganizadores()) {
                if (o.getUtilizador().getUsername().equalsIgnoreCase(id)) {
                    return true;
                }
            }
        }
        return false;
    }

}
