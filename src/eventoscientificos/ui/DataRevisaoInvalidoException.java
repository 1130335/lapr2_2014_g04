/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 * *Excepção criada caso não seja introduzida data de revisão

 * @author Sofia
 */
class DataRevisaoInvalidoException extends IllegalArgumentException {

    public DataRevisaoInvalidoException() {
        super("Data de Revisão inválida!");
    }
    
    public DataRevisaoInvalidoException (String mensagem) {
        super(mensagem);
    }
    
}
