/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;


import eventoscientificos.Empresa;

import eventoscientificos.controller.SubmeterRevisaoArtigoController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author 1130429
 */
public class DialogReverArtigo extends JDialog {

    private JTextField txtAdequacao, txtOriginalidade, txtConfianca,
            txtApresentacao;
    private JTextArea txtJustificacao;
    private JButton btnOk;
    private String avaliacao = "Aceite";
    private Empresa empresa;

    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private static final Dimension LBL_TAMANHO = new JLabel("Qualidade de apresentação (0-5): ").getPreferredSize();

    public DialogReverArtigo(Frame pai, Empresa empresa) {

        super(pai, "Rever Artigo", true);
        this.empresa = empresa;
        JPanel p1 = criarPainelNorte();
        JPanel p2 = criarPainelCentro();
        JPanel p3 = criarPainelBotoes();

        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.CENTER);
        add(p3, BorderLayout.SOUTH);
        getRootPane().setDefaultButton(btnOk);
        setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);
        getRootPane().setDefaultButton(btnOk);

    }

    private JPanel criarPainelNorte() {
        JPanel painel = new JPanel(new BorderLayout());

        painel.add(criarPainelAdequacao(), BorderLayout.NORTH);
        painel.add(criarPainelOriginalidade(), BorderLayout.CENTER);
        painel.add(criarPainelConfianca(), BorderLayout.SOUTH);

        return painel;
    }

    private JPanel criarPainelCentro() {
        JPanel painel = new JPanel(new BorderLayout());

        painel.add(criarPainelApresentacao(), BorderLayout.NORTH);
        painel.add(criarPainelAvaliacao(), BorderLayout.CENTER);
        painel.add(criarPainelJustificacao(), BorderLayout.SOUTH);

        return painel;
    }

    private JPanel criarPainelAdequacao() {
        final int CAMPO_LARGURA = 20;
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;

        JLabel lbl = new JLabel("Adequação ao evento (0-5): ", FlowLayout.RIGHT);
        lbl.setPreferredSize(LBL_TAMANHO);

        this.txtAdequacao = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtAdequacao);

        return p;
    }

    private JPanel criarPainelOriginalidade() {
        final int CAMPO_LARGURA = 20;
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;

        JLabel lbl = new JLabel("Originalidade(0-5): ", FlowLayout.RIGHT);
        lbl.setPreferredSize(LBL_TAMANHO);

        this.txtOriginalidade = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(lbl);
        p.add(txtOriginalidade);

        return p;
    }

    private JPanel criarPainelConfianca() {
        final int CAMPO_LARGURA = 20;
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;

        JLabel lbl = new JLabel("Confiança do revisor (0-5): ", FlowLayout.RIGHT);
        lbl.setPreferredSize(LBL_TAMANHO);

        this.txtConfianca = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtConfianca);

        return p;
    }

    private JPanel criarPainelApresentacao() {
        final int CAMPO_LARGURA = 20;
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;

        JLabel lbl = new JLabel("Qualidade de apresentação (0-5): ", FlowLayout.RIGHT);
        lbl.setPreferredSize(LBL_TAMANHO);

        this.txtApresentacao = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtApresentacao);

        return p;
    }

    private JPanel criarPainelAvaliacao() {
        final int CAMPO_LARGURA = 20;
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;

        JLabel lbl = new JLabel("Avaliação: ", FlowLayout.RIGHT);
        lbl.setPreferredSize(LBL_TAMANHO);

        JRadioButton radioAceite = new JRadioButton("Aceite");
        radioAceite.setActionCommand("Aceite");
        radioAceite.setSelected(true);
        radioAceite.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                avaliacao = "Aceite";
            }
        });

        JRadioButton radioRejeitado = new JRadioButton("Rejeitado");
        radioRejeitado.setActionCommand("Rejeitado");
        radioRejeitado.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                avaliacao = "Rejeitado";
            }
        });

        ButtonGroup btnGroup = new ButtonGroup();
        btnGroup.add(radioAceite);
        btnGroup.add(radioRejeitado);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(radioAceite);
        p.add(radioRejeitado);

        return p;
    }

    private JPanel criarPainelJustificacao() {
        final int CAMPO_LARGURA = 20;
        final int LINHAS = 5, COLUNAS = 20;
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;

        JLabel lbl = new JLabel("Justificação: ", FlowLayout.RIGHT);
        lbl.setPreferredSize(LBL_TAMANHO);

        this.txtJustificacao = new JTextArea(LINHAS, COLUNAS);
        this.txtJustificacao.setPreferredSize(new Dimension(100, 100));
        JScrollPane scrollPane = new JScrollPane(this.txtJustificacao,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        this.txtJustificacao.setLineWrap(true);
        this.txtJustificacao.setWrapStyleWord(true);
        this.txtJustificacao.setBorder(BorderFactory.createLineBorder(Color.GRAY));

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(scrollPane);

        return p;
    }

    private JPanel criarPainelBotao() {
        btnOk = criarBotaoOK();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOk);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    public JButton criarBotaoOK() {
        btnOk = new JButton("OK");
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                   
                    String strJust = txtJustificacao.getText();
                    String adeq = txtAdequacao.getText();
                    String orig = txtOriginalidade.getText();
                    String conf = txtConfianca.getText();
                    String qual = txtApresentacao.getText();
                    int GrauAdeq = Integer.parseInt(txtAdequacao.getText());
                    int GrauOrig = Integer.parseInt(txtOriginalidade.getText());
                    int GrauConfianca = Integer.parseInt(txtConfianca.getText());
                    int GrauQual = Integer.parseInt(txtApresentacao.getText());

                    if (0 > GrauConfianca || 5 < GrauConfianca) {
                        throw new ConfiancaInvalidaException();
                    }
                    if (0 > GrauAdeq || 5 < GrauAdeq) {
                        throw new AdequacaoInvalidaException();
                    }
                    if (0 > GrauOrig || 5 < GrauOrig) {
                        throw new OriginalidadeInvalidaException();
                    }
                    if (0 > GrauQual || 5 < GrauQual) {
                        throw new QualidadeInvalidaException();
                    }
                    if (strJust.isEmpty()) {
                        throw new JustificacaoInvalidaException();
                    }
                    if (conf.isEmpty()) {
                        throw new JustificacaoInvalidaException();
                    }
                    if (orig.isEmpty()) {
                        throw new JustificacaoInvalidaException();
                    }
                    if (qual.isEmpty()) {
                        throw new JustificacaoInvalidaException();
                    }
                    if (adeq.isEmpty()) {
                        throw new JustificacaoInvalidaException();
                    }
                    SubmeterRevisaoArtigoController m_controller = new SubmeterRevisaoArtigoController(empresa);
                    m_controller.setDados(GrauConfianca, GrauAdeq, GrauOrig, GrauQual, strJust);
                    dispose();
                } catch (AdequacaoInvalidaException excecao) {
                    JOptionPane.showMessageDialog(DialogReverArtigo.this,
                            "Tem que introduzir um valor entre 0 e 5.",
                            "Rever Artigo",
                            JOptionPane.WARNING_MESSAGE);
                    txtAdequacao.requestFocus();
                } catch (OriginalidadeInvalidaException excecao) {
                    JOptionPane.showMessageDialog(DialogReverArtigo.this,
                            "Tem que introduzir um valor entre 0 e 5.",
                            "Rever Artigo",
                            JOptionPane.WARNING_MESSAGE);
                    txtOriginalidade.requestFocus();
                } catch (ConfiancaInvalidaException excecao) {
                    JOptionPane.showMessageDialog(DialogReverArtigo.this,
                            "Tem que introduzir um valor entre 0 e 5.",
                            "Rever Artigo",
                            JOptionPane.WARNING_MESSAGE);
                    txtConfianca.requestFocus();
                } catch (QualidadeInvalidaException excecao) {
                    JOptionPane.showMessageDialog(DialogReverArtigo.this,
                            "Tem que introduzir um valor entre 0 e 5.",
                            "Rever Artigo",
                            JOptionPane.WARNING_MESSAGE);
                    txtAdequacao.requestFocus();
                } catch (JustificacaoInvalidaException excecao) {
                    JOptionPane.showMessageDialog(DialogReverArtigo.this,
                            "Tem que introduzir um valor entre 0 e 5.",
                            "Rever Artigo",
                            JOptionPane.WARNING_MESSAGE);
                    txtJustificacao.requestFocus();
                }

            }
        });
        return btnOk;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

  
}
