/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 *
 * @author Vaio
 */
class ResumoInvalidoException extends Exception {

    public ResumoInvalidoException() {
        super("Resumo do Artigo Final inválido!");
    }
    public ResumoInvalidoException(String mensagem) {
        super(mensagem);
    }
    
}
