/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

import eventoscientificos.Topico;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Rita
 */
public class DialogTopArtigo extends JDialog{

    private Topico m_topico;
    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;

    public DialogTopArtigo(Frame window, List<Topico> lte) {
        super(window, "Selecionar Topicos", true);
        JPanel p1 = selecionarTopico(lte);
        JPanel p2 = painelBotoes();
        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);
        setLocation(window.getX() + DIALOGO_DESVIO_X, window.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);

    }

    private JPanel selecionarTopico(final List<Topico> lte) {
        JLabel lbl = new JLabel("Tópico: ");
        String[] top = new String[lte.size()];
        int i = 0;
        for (Topico topico : lte) {
            top[i] = topico.getM_strDescricao();
            i++;
        }
        final JComboBox<String> cmbEventos = new JComboBox<String>(top);
        cmbEventos.setSelectedIndex(-1);
        cmbEventos.setMaximumRowCount(3);
        cmbEventos.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String t_desc = (String) (cmbEventos.getSelectedItem());
                for (Topico topico : lte) {
                    if (topico.getM_strDescricao().equalsIgnoreCase(t_desc)) {
                        m_topico = topico;
                    }
                }
            }
        });

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(lbl);
        p.add(cmbEventos);

        return p;
    }

    private JPanel painelBotoes() {
        JButton btnOk = new JButton("OK");
        getRootPane().setDefaultButton(btnOk);
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOk);

        return p;
    }

    public Topico getTopicoArtigo() {
        return m_topico;
    }
    
}
