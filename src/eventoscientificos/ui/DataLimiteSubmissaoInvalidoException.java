/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 *  *Excepção criada caso não seja introduzida data limite de submissão

 * @author Sofia
 */
class DataLimiteSubmissaoInvalidoException extends IllegalArgumentException {

    public DataLimiteSubmissaoInvalidoException() {
        super("Data Limite de Submissão Inválida!");
    }
    
    public DataLimiteSubmissaoInvalidoException (String mensagem) {
        super(mensagem);
    }
    
}
