/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

import eventoscientificos.Autor;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Rita
 */
public class DialogACorrespondente extends JDialog {
    
    private Autor m_autor;
    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;

    public DialogACorrespondente(Frame window, List<Autor> la) {
        super(window, "Escolher Autor Correspondente", true);
        JPanel p1 = selecionarAutor(la);
        JPanel p2 = painelBotoes();
        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);
        setLocation(window.getX() + DIALOGO_DESVIO_X, window.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);

    }

    private JPanel selecionarAutor(final List<Autor> la) {
        JLabel lbl = new JLabel("Autor: ");
        String[] au = new String[la.size()];
        int i = 0;
        for (Autor aut : la) {
            au[i] = aut.getNome();
            i++;
        }
        final JComboBox<String> cmbEventos = new JComboBox<String>(au);
        cmbEventos.setSelectedIndex(-1);
        cmbEventos.setMaximumRowCount(3);
        cmbEventos.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String t_nome = (String) (cmbEventos.getSelectedItem());
                for (Autor autor : la) {
                    if (autor.getNome().equalsIgnoreCase(t_nome)) {
                        m_autor = autor;
                    }
                }
            }
        });

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(lbl);
        p.add(cmbEventos);

        return p;
    }

    private JPanel painelBotoes() {
        JButton btnOk = new JButton("OK");
        getRootPane().setDefaultButton(btnOk);
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOk);

        return p;
    }

    public Autor getAutor() {
        return this.m_autor;
    }
    
}
