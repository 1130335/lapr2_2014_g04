/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 *Excepção criada caso a lista de utilizadores não seja valida
 * @author Sofia
 */
class ListaUtilizadoresInvalidoException extends IllegalArgumentException {

    public ListaUtilizadoresInvalidoException() {
        super ("Lista de utilizadores inválida!");
    }
    
    public ListaUtilizadoresInvalidoException(String mensagem) {
        super(mensagem);
    }
}
