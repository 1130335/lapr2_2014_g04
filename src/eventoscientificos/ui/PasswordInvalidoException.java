/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

/**
 *Excepção criada caso não seja introduzida password
 * @author Sofia
 */
class PasswordInvalidoException extends IllegalArgumentException {

    public PasswordInvalidoException() {
        super ("Password Inválida!");
    }
    
    public PasswordInvalidoException (String mensagem) {
        super(mensagem);
    }
    
}
