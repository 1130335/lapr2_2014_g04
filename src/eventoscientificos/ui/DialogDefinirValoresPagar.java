/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.Empresa;
import eventoscientificos.Evento;
import eventoscientificos.RegistarEvento;
import eventoscientificos.controller.*;
import java.awt.BorderLayout;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author 1130429
 */

   
    

    /**
     * Classe que representa uma JDialog onde é pedida informação sobre o valor
     * a pagar pelos artigos e respectiva formula de pagamento.
     */
    public class DialogDefinirValoresPagar extends JDialog {

        private JTextField txtFull, txtShort, txtPoster,
                txtFormula;

        private JButton btnOk;
        private Empresa empresa;

        private static final int DIALOGO_DESVIO_X = 10, DIALOGO_DESVIO_Y = 10;
        private final Dimension LBL_TAMANHO = new JLabel("Valor de um artigo de formato Poster : ").getPreferredSize();

        public DialogDefinirValoresPagar(JFrame pai, Empresa empresa) {
            super(pai, "Definir Valores a Pagar", true);
            this.empresa=empresa;

            final int LINHAS = 7, COLUNAS = 1;
            setLayout(new BorderLayout());

            JPanel p1 = criarPainelNorte();
            JPanel p2 = criarPainelCentro();
            JPanel p3 = criarPainelBotao();
            add(p1, BorderLayout.NORTH);
            add(p2, BorderLayout.CENTER);
            add(p3, BorderLayout.SOUTH);

            getRootPane().setDefaultButton(btnOk);
            setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
            pack();
            setResizable(false);
            setVisible(true);
        }

        private JPanel criarPainelNorte() {
            JPanel painel = new JPanel(new BorderLayout());

            painel.add(criarPainelFull(), BorderLayout.NORTH);
            painel.add(criarPainelShort(), BorderLayout.CENTER);
            painel.add(criarPainelPoster(), BorderLayout.SOUTH);

            return painel;
        }

        private JPanel criarPainelCentro() {
            JPanel painel = new JPanel(new BorderLayout());

            painel.add(criarPainelFull(), BorderLayout.NORTH);
            painel.add(criarPainelShort(), BorderLayout.CENTER);
            painel.add(criarPainelPoster(), BorderLayout.SOUTH);

            return painel;
        }

        private JPanel criarPainelFull() {
            final int CAMPO_LARGURA = 20;
            final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
            final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;

            JLabel lbl = new JLabel("Valor a pagar por um artigo do tipo full: ", FlowLayout.RIGHT);
            lbl.setPreferredSize(LBL_TAMANHO);

            this.txtFull = new JTextField(CAMPO_LARGURA);

            JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
            p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                    MARGEM_INFERIOR, MARGEM_DIREITA));

            p.add(lbl);
            p.add(txtFull);

            return p;
        }

        private JPanel criarPainelShort() {
            final int CAMPO_LARGURA = 20;
            final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
            final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;

            JLabel lbl = new JLabel("Valor a pagar por um artigo do formato Short: ", FlowLayout.RIGHT);
            lbl.setPreferredSize(LBL_TAMANHO);

            this.txtShort = new JTextField(CAMPO_LARGURA);

            JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
            p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                    MARGEM_INFERIOR, MARGEM_DIREITA));
            p.add(lbl);
            p.add(txtShort);

            return p;
        }

        private JPanel criarPainelPoster() {
            final int CAMPO_LARGURA = 20;
            final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
            final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;

            JLabel lbl = new JLabel("Valor a pagar por um artigo do formato poster: ", FlowLayout.RIGHT);
            lbl.setPreferredSize(LBL_TAMANHO);

            this.txtShort = new JTextField(CAMPO_LARGURA);

            JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
            p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                    MARGEM_INFERIOR, MARGEM_DIREITA));

            p.add(lbl);
            p.add(txtShort);

            return p;
        }

        private JPanel criarPainelFormula() {
            final int CAMPO_LARGURA = 20;
            final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
            final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;

            JLabel lbl = new JLabel("Formula de pagamento (1/2): ", FlowLayout.RIGHT);
            lbl.setPreferredSize(LBL_TAMANHO);

            this.txtFormula = new JTextField(CAMPO_LARGURA);

            JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
            p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                    MARGEM_INFERIOR, MARGEM_DIREITA));

            p.add(lbl);
            p.add(txtFormula);

            return p;
        }

        private JPanel criarPainelBotao() {
            btnOk = criarBotaoOK();

            JPanel p = new JPanel();
            final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
            final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
            p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                    MARGEM_INFERIOR, MARGEM_DIREITA));
            p.add(btnOk);

            return p;
        }

        public JButton criarBotaoOK() {
            btnOk = new JButton("OK");
            btnOk.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    String vFull = txtFull.getText();
                    String vShort = txtShort.getText();
                    String vPoster = txtPoster.getText();
                    String formula = txtFormula.getText();
                    DefinirValoresPagarController m_Controller = new DefinirValoresPagarController(empresa);
                    m_Controller.setValorFull(vFull);
                    m_Controller.setValorShort(vShort);
                    m_Controller.setValorPoster(vPoster);
                    m_Controller.setFormula(formula);
                    dispose();
                    if (vFull.isEmpty()) {
                        throw new FullInvalidoException();
                    }
                    if (vShort.isEmpty()) {
                        throw new ShortInvalidoException();
                    }
                    if (vPoster.isEmpty()) {
                        throw new PosterInvalidoException();
                    }
                    if (formula.isEmpty()) {
                        throw new FormulaInvalidaException();
                    }

                }
            });
            return btnOk;
        }
    }

