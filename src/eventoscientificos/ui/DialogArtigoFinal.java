/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

import eventoscientificos.CriarEventoCientificoController;
import eventoscientificos.Empresa;
import eventoscientificos.Evento;
import eventoscientificos.controller.SubmeterArtigoFinalController;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Vaio
 */
class DialogArtigoFinal extends JDialog {
    private JTextField txtTitulo, txtResumo, txtTipoArtigo;
    
    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private static final Dimension LABEL_TAMANHO = new JLabel("Tipo de Artigo: ").getPreferredSize();
    private Empresa m_empresa;
    
     public DialogArtigoFinal(Frame pai, Empresa empresa) {

        super(pai, "Submeter Artigo Final", true);

        JPanel p1 = painelPrincipal();
        JPanel p2 = criarPainelBotoes();
        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.SOUTH);

        setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
        pack();
        setResizable(false);
        setVisible(true);
        this.m_empresa = empresa;

    }
     private JPanel painelPrincipal() {
        JPanel p = new JPanel(new GridLayout(13, 1));

        JPanel p1 = criarPainelTítulo();
        JPanel p2 = criarPainelResumo();
        JPanel p3 = criarPainelTipoArtigo();
      

        p.add(p1);
        p.add(p2);
        p.add(p3);
    

        return p;
    }

    private JPanel criarPainelTítulo() {
        JLabel lbl = new JLabel("Título:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtTitulo = new JTextField(CAMPO_LARGURA);
        txtTitulo.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtTitulo);

        return p;
    }
    
    private JPanel criarPainelResumo() {
        JLabel lbl = new JLabel("Resumo:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtResumo = new JTextField(CAMPO_LARGURA);
        txtResumo.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtResumo);

        return p;
    }
    
    
    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String titulo = txtTitulo.getText();
                    if (titulo.isEmpty()) {
                        throw new TituloInvalidoException();
                    }
                    String resumo = txtResumo.getText();
                    if (resumo.isEmpty()) {
                        throw new ResumoInvalidoException();
                    }
                    String tipoArtigo = txtTipoArtigo.getText();
                    if (tipoArtigo.isEmpty()) {
                        throw new TipoArtigoInvalidoException();
                    }
                   
SubmeterArtigoFinalController saf = new SubmeterArtigoFinalController(m_empresa);
                    saf.setTitulo(titulo);
                    saf.setResumo(resumo);
                    saf.setTipoArtigo(tipoArtigo);
                   

                } catch (TituloInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogArtigoFinal.this,
                            "Tem que introduzir um Título válido.",
                            "Submeter Artigo Final.",
                            JOptionPane.WARNING_MESSAGE);
                    txtTitulo.requestFocus();
                } catch (ResumoInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogArtigoFinal.this,
                            "Tem que introduzir um resumo válido.",
                            "Submeter Artigo Final.",
                            JOptionPane.WARNING_MESSAGE);
                    txtResumo.requestFocus();
                } catch (TipoArtigoInvalidoException excecao) {
                    JOptionPane.showMessageDialog(DialogArtigoFinal.this,
                            "Tem que introduzir um tipo de Artigo válido.",
                            "Submeter Artigo Final.",
                            JOptionPane.WARNING_MESSAGE);
                    txtTipoArtigo.requestFocus();
                } 

            }
        });
        return btn;
    }
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

    private JPanel criarPainelTipoArtigo() {
        JLabel lbl = new JLabel("Tipo de Artigo:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtTipoArtigo = new JTextField(CAMPO_LARGURA);
        txtTipoArtigo.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtTipoArtigo);

        return p;
    }
    
}
