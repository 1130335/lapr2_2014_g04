/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Artigo {

    private String m_idArtigo;
    private String m_strTitulo;
    private String m_strResumo;
    private String m_strTipo;
    private String m_strAutor;
    private List<Autor> m_listaAutores;
    private Autor m_autorCorrespondente;
    private String m_strFicheiro;
    private List<Topico> m_listaTopicos;
    private List<Revisor> m_listaRevisores;
    private List<Revisao> m_listaRevisoes;
    private boolean m_decisao;
    private int m_numeroRevisao = 0;
    private boolean notificacao;

    /**
     * Cria artigo sem parametros
     */
    public Artigo() {
        m_listaAutores = new ArrayList<Autor>();
        m_listaTopicos = new ArrayList<Topico>();
        m_listaRevisoes = new ArrayList<Revisao>();
        notificacao = false;
    }

    /**
     * Devolve a lista de autores do artigo
     * @return lista de autores
     */
    public List<Autor> getListaAutores() {
        return m_listaAutores;
    }

    /**
     * Altera a notificaçao sobre a decisao sobre o artigo
     * @param not notificacao do artigo
     */
    public void setNotificado(boolean not) {
        this.notificacao = not;
    }

    /**
     * ALtera o id do artigo
     * @param idArtigo id do artigo
     */
    public void setIDArtigo(String idArtigo) {
        this.m_idArtigo = idArtigo;
    }

    /**
     * Devolve o id do artigo
     * @return id do artigo
     */
    public String getIDArtigo() {
        return this.m_idArtigo;
    }

    /**
     * Altera o tipo de artigo
     *
     * @param m_strTipo novo tipo de artigo
     */
    public void setTipo(String m_strTipo) {
        this.m_strTipo = m_strTipo;
    }

    /**
     * Altera o titulo do artigo
     *
     * @param strTitulo novo titulo
     */
    public void setTitulo(String strTitulo) {
        this.m_strTitulo = strTitulo;
    }

    /**
     * Devolve o titulo do artigo
     * @return titulo
     */
    public String getTitulo() {
        return this.m_strTitulo;
    }

    /**
     * Altera o resumo do artigo
     *
     * @param strResumo novo resumo
     */
    public void setResumo(String strResumo) {
        this.m_strResumo = strResumo;
    }

    /**
     * Devolve o tipo do artigo
     * @return the m_strTipo
     */
    public String getTipo() {
        return m_strTipo;
    }

    /**
     * Devolve o resumo do artigo
     * @return resumo
     */
    public String getResumo() {
        return this.m_strResumo;
    }

    /**
     * Cria uma nova instancia de autor com dois atributos
     *
     * @param strNome nome do autor
     * @param strAfiliacao afiliacao do autor
     * @return novo autor
     */
    public Autor novoAutor(String strNome, String strAfiliacao) {
        Autor autor = new Autor();
        autor.setNome(strNome);
        autor.setAfiliacao(strAfiliacao);

        return autor;
    }

    /**
     * Cria uma nova instancia de autor com 3 atributos
     *
     * @param nome nome do autor
     * @param afiliacao afliacao do autor
     * @param email email do autor
     * @return novo autor
     */
    public Autor novoAutor(String nome, String afiliacao, String email) {
        Autor autor = new Autor();
        autor.setNome(nome);
        autor.setAfiliacao(afiliacao);
        autor.setEMail(email);
        return autor;
    }

    /**
     * Cria uma instancia de Autor com 4 atributos
     * @param strNome nome do autor
     * @param strAfiliacao afiliacao do autor
     * @param strEmail email do autor
     * @param utilizador utilizador correspondente ao autor
     * @return novo autor
     */
    public Autor novoAutor(String strNome, String strAfiliacao, String strEmail, Utilizador utilizador) {
        Autor autor = new Autor();
        autor.setNome(strNome);
        autor.setAfiliacao(strAfiliacao);
        autor.setEMail(strEmail);
        autor.setUtilizador(utilizador);

        return autor;
    }

    /**
     * Adiciona o autor passado como parametro à lista de autores do artigo
     *
     * @param autor do artigo
     * @return true se o autor for adicionado
     */
    public boolean addAutor(Autor autor) {
        if (validaAutor(autor)) {
            return m_listaAutores.add(autor);
        } else {
            return false;
        }

    }

    /**
     * Valida o autor
     * @param autor
     * @return validaçao
     */
    private boolean validaAutor(Autor autor) {
        return autor.valida();
    }

    /**
     * Devolve a lista de autores do artigo que também são utilizadore do
     * sistema
     *
     * @return lista de autores
     */
    public List<Autor> getPossiveisAutoresCorrespondentes() {
        List<Autor> la = new ArrayList<Autor>();

        for (Autor autor : this.m_listaAutores) {
            if (autor.podeSerCorrespondente() && !autor.equals(null)) {
                la.add(autor);
            } else {
                return null;
            }
        }
        return la;
    }

    /**
     * Altera o autor correspondente para o autor passado como parametro
     *
     * @param autor autor correspondente
     */
    public void setAutorCorrespondente(Autor autor) {
        this.m_autorCorrespondente = autor;
    }

    /**
     * Devolve o autor correspondente do artigo
     * @return autor correspondente
     */
    public Autor getAutorCorrespondente() {
        return this.m_autorCorrespondente;
    }

    /**
     * Altera o ficheiro referente ao artigo
     *
     * @param strFicheiro novo ficheiro do artigo
     */
    public void setFicheiro(String strFicheiro) {
        this.m_strFicheiro = strFicheiro;
    }

    /**
     * Altera a lista de tópicos de artigo
     * @param listaTopicos lista de tópicos de artigo
     *
     * adicionado na iteração 2
     */
    public void setListaTopicos(List<Topico> listaTopicos) {
        this.m_listaTopicos.addAll(listaTopicos);
    }

    /**
     * Devolve a informacao sobre o artigo
     *
     * @return informacao
     */
    public String getInfo() {
        return this.toString();
    }

    /**
     * Devolve a lista de tópicos do artigo
     * @return lista de tópicos
     */
    public List<Topico> getListaTopicos() {
        return m_listaTopicos;
    }

    /**
     * Valida o artigo
     * @return true
     */
    public boolean valida() {
        return true;
    }

    /**
     * Devolve o titulo do artigo
     * @return titulo
     */
    @Override
    public String toString() {
        return this.m_strTitulo;
    }

    /**
     * Adiciona um topico aos lista de tópicos de artigo
     * @param topico novo topicos
     * @return true se o topico for adicionado
     */
    public boolean addTopico(Topico topico) {
        if (ValidaTopico(topico)) {
            return m_listaTopicos.add(topico);
        } else {
            return false;
        }

    }

    /*
    * Valida o topico
    */
    private boolean ValidaTopico(Topico topico) {
        return topico.valida();

    }

    /**
     *
     * @param obj
     * @return
     *
     * Quando é que dois artigos são iguais? Quando têm o mesmo título e mesmo
     * autor correspondente? Ou quando todos os autores forem também os mesmos?
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else {
            if (obj instanceof Artigo) {
                Artigo aux = (Artigo) obj;
                return this.m_autorCorrespondente.equals(aux.m_autorCorrespondente)
                        && this.m_strTitulo.equals(aux.m_strTitulo);
            } else {
                return false;
            }
        }
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + (this.m_strTitulo != null ? this.m_strTitulo.hashCode() : 0);
        hash = 19 * hash + (this.m_strResumo != null ? this.m_strResumo.hashCode() : 0);
        return hash;
    }

    /**
     * Devolve a lista de revisores do artigo
     * @return the m_listaRevisores
     */
    public List<Revisor> getListaRevisores() {
        return m_listaRevisores;
    }

    /**
     * Altera a lista de revisores do artigo
     * @param listaRevisores the m_listaRevisores to set
     */
    public void setListaRevisores(List<Revisor> listaRevisores) {
        this.m_listaRevisores = listaRevisores;
    }

    /**
     * Altera a lista de revisoes do artigo
     * @param listaRevisoes lista de revisoes
     */
    public void setListaRevisoes(List<Revisao> listaRevisoes) {
        this.m_listaRevisoes = listaRevisoes;
        this.m_numeroRevisao = this.m_numeroRevisao + this.m_listaRevisoes.size();
    }

    /**
     * Devolve a lista de revisores atribuidos a um artigo
     *
     * @return lista de revisores
     */
    public List<Revisao> getListaRevisoes() {
        return this.m_listaRevisoes;
    }

    /**
     * Adiciona uma nova revisao à lista de revisoes
     * @param revisao
     */
    public void addRevisao(Revisao revisao) {
        this.m_listaRevisoes.add(revisao);
        this.m_numeroRevisao++;
    }

    /**
     * Altera a decisao tomada para o artigo
     *
     * @param decisao nova decisao
     */
    public void setDecisao(Decisao decisao) {
        this.m_decisao = decisao.getDecisao();
    }

    /**
     * Devolve a decisao tomada para o artigo
     * @return decisao
     */
    public boolean getDecisao() {
        return this.m_decisao;
    }
}
