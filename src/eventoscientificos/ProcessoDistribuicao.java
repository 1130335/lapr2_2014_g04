package eventoscientificos;

import java.util.ArrayList;
import java.util.List;


public class ProcessoDistribuicao {
    private MecanismoDistribuicao m_md;
    private List<Distribuicao> m_ld;
    private Evento m_evento;
    
    public ProcessoDistribuicao(MecanismoDistribuicao md, Evento e){
      m_md=md;
      m_evento=e;
    }
    
    public List<Revisor> getListaRevisores(){
        List<Revisor> listaRevisores=m_evento.getCP().getListaRevisores();
        return listaRevisores;
    }
    
    public List<Artigo> getListaArtigos(){
        List<Artigo> listaArtigos = new ArrayList();
        for (int i=0;i<m_evento.getListaSubmissoes().size();i++){
            listaArtigos.add(m_evento.getListaSubmissoes().get(i).getArtigo());
        }
        return listaArtigos;
    }
    
    public Distribuicao novaDistribuicao(){
        Distribuicao d = new Distribuicao();
        return d;
    }
    
    public Distribuicao novaDistribuicao(List<Revisor> listaRevisores, Artigo art){
        Distribuicao d = new Distribuicao(listaRevisores, art);
        return d;
    }
    
    public void setListaDistribuicao(List<Distribuicao>  d){
        m_ld=d;
    }
    
    public List<Distribuicao> distribui(){
        setListaDistribuicao(m_md.distribui(this));
        return m_md.distribui(this);
    }                                                                                                                 
}

