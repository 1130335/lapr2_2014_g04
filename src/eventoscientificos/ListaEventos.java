/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Sofia
 */
public class ListaEventos {
    
    private List<Evento> m_registoEventos;
    private Empresa m_empresa;
    private RegistarUtilizador m_registarUtilizador;
    private ArrayList<Evento> m_listaEventos;
    private RegistarUtilizador m_listaUtilizadores;
    
    /**
     * Construtor de ListaEventos que recebe empresa como parametro
     * @param empresa 
     */
    public ListaEventos(Empresa empresa){
        m_registoEventos = new ArrayList<>();
        m_empresa = empresa;
    }

/**
 * Consulta a lista de eventos 
 * @return lista de eventos
 */    
    public List<Evento> getRegistoEventos(){
        return m_registoEventos;
    }
    
    /**
     * Adiciona um evento à lista
     * @param e 
     */
    public void addEvento(Evento e){
        m_registoEventos.add(e);
    }
    
    /**
     * Adiciona a lista de eventos
     * @param lista 
     */
    public void addListaEventos(List<Evento> lista){
        m_registoEventos.addAll(lista);
    }   
    
    /**
     * Consulta a lista de eventos do organizador
     * @param u
     * @return lista de eventos do organizador
     */
    public List<Evento> getEventosOrganizador(Utilizador u) {
        List<Evento> leOrganizador = new ArrayList<Evento>();

  

        if (u != null) {
            for (Iterator<Evento> it = getRegistoEventos().listIterator(); it.hasNext();) {
                Evento e = it.next();
                List<Organizador> lOrg = e.getListaOrganizadores();

                boolean bRet = false;
                for (Organizador org : lOrg) {
                    if (org.getUtilizador().equals(u)) {
                        bRet = true;
                        break;
                    }
                }
                if (bRet) {
                    leOrganizador.add(e);
                }
            } 
        }
        return leOrganizador;
    }

    /**
     * Cria uma nova CP
     * @return 
     */
    public CP novaCP(){
        return new CP();
    }
    
}
