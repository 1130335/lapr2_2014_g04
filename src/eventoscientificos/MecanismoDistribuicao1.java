package eventoscientificos;

import java.util.ArrayList;
import java.util.List;


public class MecanismoDistribuicao1 implements MecanismoDistribuicao {
        
    public MecanismoDistribuicao1(){}
        
    public List<Distribuicao> distribui(ProcessoDistribuicao pd){
        Boolean topicoIgual=false;
        List<Revisor> listaRevisores = pd.getListaRevisores();
        List<Artigo> listaArtigos = pd.getListaArtigos();
        List<Distribuicao> listaDistribuicao = new ArrayList<Distribuicao>();
        for(Artigo a: listaArtigos){
            int x=0;
            List<Topico> listaTopArtigo = a.getListaTopicos();
            List<Revisor> listaRevArtigo = new ArrayList<Revisor>();
            for(Revisor r: listaRevisores){
                List<Topico> listaTopRevisor = r.getListaTopicos();
                for(Topico ta: listaTopArtigo){
                    for(Topico tr: listaTopRevisor){
                        if(ta == tr && ta!= null && tr!= null){
                            topicoIgual = true;
                        }
                    }
                }   
                if(topicoIgual && x!=3){
                    listaRevArtigo.add(r);
                    x++;
                }
            }
            Distribuicao d = pd.novaDistribuicao();
            d.setArtigo(a);
            d.setRevisores(listaRevArtigo);
            listaDistribuicao.add(d);
        }
        return listaDistribuicao;
    }
    }
    

