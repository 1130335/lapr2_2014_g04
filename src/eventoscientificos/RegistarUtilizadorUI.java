package eventoscientificos;

import eventoscientificos.controller.RegistarUtilizadorController;
import utils.*;

/**
 *
 * @author Nuno Silva
 */
public class RegistarUtilizadorUI {

    private Empresa m_empresa;
    private RegistarUtilizadorController m_controllerRU;
   

    public RegistarUtilizadorUI(Empresa empresa) {
        m_empresa = empresa;
        m_controllerRU = new RegistarUtilizadorController(m_empresa);
    }

    public void run(Utilizador utilizador)
    {
        novoUtilizador();

        introduzDadosUtilizador();
        
        

        apresentaUtilizador(utilizador);
    }

    private Utilizador novoUtilizador() {
        return m_controllerRU.novoUtilizador();
    }

    private void introduzDadosUtilizador()
    {
        String strUsername = Utils.readLineFromConsole("Introduza Username: ");

        String strPassword = Utils.readLineFromConsole("Introduza Password: ");

        String strNome = Utils.readLineFromConsole("Introduza Nome: ");

        String strEmail = Utils.readLineFromConsole("Introduza Email: ");

        String strPassConfirm = Utils.readLineFromConsole("Introduza Confimação Password: ");

        m_controllerRU.setDados(strUsername, strPassword, strNome, strEmail);
    }

    private void apresentaUtilizador(Utilizador utilizador) {
        if (utilizador == null) {
            System.out.println("Utilizador não registado.");
        } else {
            System.out.println(utilizador.toString());
        }
    }
}
