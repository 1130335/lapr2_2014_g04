/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import java.text.ParseException;

/**
 *
 * @author 1130429
 */
public interface ModoPagamento {
     /**
     * Executa um pagamento através de uma sistema externo e devolve uma
     * mensagem com informação sobre o pagamento.
     * 
     * @param dataValidade data de validade do cartão
     * @param strNumCC número do cartão
     * @param fValorADCC valor 
     * @param dataLimite data limite de pagamento
     * @return mensagem com informação sobre o pagamento
     * @throws ParseException
     * @throws IllegalArgumentException
     */
    String pagamento(String dataValidade, String strNumCC, float fValorADCC, 
            String dataLimite)throws ParseException;
}
