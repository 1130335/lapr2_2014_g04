/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

/**
 *
 * @author Rita
 */
public class Decisao {

    private Artigo m_artigo;
    private boolean m_decisao;

    /**
     * Construtor de Decisão predefinido para quando não são introduzidos parâmetros.
     */
    public Decisao() {
        this.m_decisao = false;
    }

    /**
     * Contrutor de Decisão que recebe um parâmetro.
     * @param decisao decisão tomada pelo Organizador relativa ao artigo
     */
    public Decisao(boolean decisao) {
        this.m_decisao = decisao;
    }

    /**
     * Altera o artigo referente à decisão
     * @param a artigo relativo à decisão tomada
     */
    public void setArtigo(Artigo a) {
        this.m_artigo = a;
    }
    
    /**
     * Altera a decisão tomada pelo Organizador
     * @param decisao nova decisão
     */
    public void setDecisao(boolean decisao) {
        this.m_decisao = decisao;
    }
    
    /**
     * Devolve a decisão de um certo artigo
     * @return decisão tomada
     */
    public boolean getDecisao() {
        return this.m_decisao;
    }
    
    /**
     * Devolve o artigo referente à decisão feita
     * @return artigo referente à decisão
     */
    public Artigo getArtigo() {
        return this.m_artigo;
    }
    
}
