/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

//import com.thoughtworks.xstream.XStream;
//import com.thoughtworks.xstream.io.xml.StaxDriver;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rita
 */
public class GerarNotificacao {

    private String paper_title;
    private String paper_type;
    private String author_name;
    private String author_email;
    private String decision;
    private int review_number;
    private List<String> reviewer_comments;

    public GerarNotificacao() {
        this.paper_title = "";
        this.paper_type = "";
        this.author_name = "";
        this.author_email = "";
        this.decision = "";
        this.review_number = 0;
        this.reviewer_comments = new ArrayList<String>();

    }

    public void preencheNotificacao(Artigo artigo) {
        this.paper_title = artigo.getTitulo();
        this.paper_type = artigo.getTipo();
        Autor autor = artigo.getAutorCorrespondente();
        this.author_name = autor.getNome();
        Utilizador utilizador = autor.getUtilizador();
        this.author_email = utilizador.getEmail();
        boolean dec = artigo.getDecisao();
        if (dec == true) {
            this.decision = "Aceite";
        } else {
            this.decision = "Rejeitado";
        }
        this.review_number++;
        List<Revisao> revisoes = artigo.getListaRevisoes();
        for (Revisao r : revisoes) {
            this.reviewer_comments.add(r.getJustificacao());
        }
    }

//    public String criarXML(Artigo artigo) {
//        XStream xstream = new XStream(new StaxDriver());
//        this.preencheNotificacao(artigo);
//        xstream.alias("submission", Submissao.class);
//        xstream.alias("comments", Revisao.class);
//        String xml = xstream.toXML(this);
//        return xml;
//    }
//
//    public static void main(String[] args) {
//        Artigo artigo = new Artigo();
//        Autor autor = new Autor();
//        Utilizador u = new Utilizador();
//        u.setEmail("email@isep.ipp.pt");
//        autor.setUtilizador(u);
//        autor.setNome("Nome");
//        artigo.setAutorCorrespondente(autor);
//        Decisao dec = new Decisao();
//        dec.setDecisao(true);
//        artigo.setDecisao(dec);
//        artigo.setTipo("Poster");
//        artigo.setTitulo("Titulo");
//        List<Revisao> rev = new ArrayList<Revisao>();
//        Revisao r = new Revisao();
//        r.setJustificacao("Justificacao");
//        rev.add(r);
//        artigo.setListaRevisoes(rev);
//        GerarNotificacao n1 = new GerarNotificacao();
//        System.out.println(n1.criarXML(artigo));
//    }

}
