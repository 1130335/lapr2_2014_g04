/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Sofia
 */
public class RegistarEvento {

    private Empresa m_empresa;
    private Evento m_evento;
    private List<Evento> m_listaEventos;
    private List<Utilizador> m_listaUtilizadores;
    
    /**
     * Construtor de RegistarEvento que recebe a empresa como parametro
     * @param empresa 
     */
    public RegistarEvento(Empresa empresa) {
        this.m_listaEventos = new ArrayList<Evento>();
        m_empresa = empresa;
    }

/**
 * Adiciona um evento à lista de eventos
 * @param e
 * @return 
 */
    public boolean addEvento(Evento e) {
        return this.getListaEventos().add(e);
    }
/**
 * Consulta a lista de eventos do organizador 
 * @param u
 * @return lista de eventos do organizador
 */
    public List<Evento> getEventosOrganizador(Utilizador u) {
        List<Evento> leOrganizador = new ArrayList<>();

        if (u != null) {
            for (Evento e : this.getListaEventos()) {
                List<Organizador> lOrg = e.getListaOrganizadores();

                boolean bRet = false;
                for (Organizador org : lOrg) {
                    if (org.getUtilizador().equals(u)) {
                        bRet = true;
                        break;
                    }
                }
                if (bRet) {
                    leOrganizador.add(e);
                }
            }
        }
        return leOrganizador;
    }

    /**
     * Consulta os eventos do organizador possiveis de decidir
     * @param u
     * @return lista de eventos do organizador
     */
    public List<Evento> getEventosOrganizadorPossiveisDecidir(Utilizador u) {
        List<Evento> leOrganizador = new ArrayList<>();

        if (u != null) {
            for (Evento e : this.getListaEventos()) {
                List<Organizador> lOrg = e.getListaOrganizadores();

                boolean bRet = false;
                if (e.getState().setRevisto()) {
                    for (Organizador org : lOrg) {
                        if (org.getUtilizador().equals(u)) {
                            bRet = true;
                            break;
                        }
                    }
                }
                if (bRet) {
                    leOrganizador.add(e);
                }
            }
        }
        return leOrganizador;
    }
    /**
     * Cria um novo Evento
     * @return novo evento
     */
    public Evento novoEvento() {
        return new Evento();
    }
    
    /**
     * Regista o evento
     * @param e
     * @return evento adicionado caso o evento seja valido ou false caso o evento não seja adicionado
     */
    public boolean registaEvento(Evento e) {
        if (e.valida() && validaEvento(e)) {
            return addEvento(e);
        } else {
            return false;
        }
    }

    /**
     * Valida o evento 
     * @param e
     * @return false caso exista um evento igual e true caso contrário
     */
    private boolean validaEvento(Evento e) {
        for (Evento evento : m_listaEventos) {
            if (evento.equals(e)) {
                return false;
            }
        }
        return true;
    }

   

    
    
 /**
  * Consulta a lista de eventos
  * @return lista eventos
  */
    public List<Evento> getListaEventos() {
        return m_listaEventos;
    }
    
    /**
     * Consulta a lista de eventos
     * @param id
     * @return lista eventos
     */
    public List<Evento> getRegistoEventos (String id) {
        return m_listaEventos;
        
    }

    /**
     * Devolve a lista de eventos de um determinado organizador
     * @param strId id (email) do organizador
     * @return lista de eventos do organizador
     */
    public List<Evento> getListaEventosOrganizador(String strId) {
        List<Evento> leOrganizador = new ArrayList<Evento>();

        Utilizador u = getUtilizador(strId);

        if (u != null) {
            for (Iterator<Evento> it = m_listaEventos.listIterator(); it.hasNext();) {
                Evento e = it.next();
                List<Organizador> lOrg = e.getListaOrganizadores();

                boolean bRet = false;
                for (Organizador org : lOrg) {
                    if (org.getUtilizador().equals(u)) {
                        bRet = true;
                        break;
                    }
                }
                if (bRet) {
                    leOrganizador.add(e);
                }
            }
        }
        return leOrganizador;
    }

    /**
     * Consulta a lista de eventos do revisor
     * @param strId
     * @return 
     */
    public List<Evento> getListaEventosRevisor(String strId) {
        List<Evento> leRevisor = new ArrayList<Evento>();
        Utilizador u = getUtilizador(strId);
        if (u != null) {
            for (Iterator<Evento> it = m_listaEventos.listIterator(); it.hasNext();) {
                Evento e = it.next();
                List<Revisor> lRev = e.getListaRevisores();

                boolean bRet = false;
                for (Revisor rev : lRev) {
                    if (rev.getUtilizador().equals(u)) {
                        bRet = true;
                        break;
                    }
                }
                if (bRet) {
                    leRevisor.add(e);
                }
            }
        }
        return leRevisor;
    }

    /**
     * Consulta o evento correspondente com o id introduzido
     * @param strId
     * @return evento correspondente com o id introduzido
     */
    public Evento getEventoID(String strId) {
        for (Evento e :m_listaEventos) {
            if (e.getIDEvento().equalsIgnoreCase(strId)) {
                return e;
            }
        }
        return null;
    }

    /**
     * Consulta lista de eventos nos quais pode submeter
     * @return lista de eventos
     */
    public List<Evento> getListaEventosPodeSubmeter() {
        List<Evento> le = new ArrayList<Evento>();

        for (Evento e : m_listaEventos) {
            if (e.aceitaSubmissoes()) {
                le.add(e);
            }
        }

        return le;
    }

    /**
     * Consulta os eventos quais o autor submeteu
     * @param id
     * @return lista de eventos do autor
     */
    public List<Evento> getEventosAutorSubmeteu(String id) {
        List<Evento> le = new ArrayList<Evento>();
        Utilizador u = getUtilizador(id);
        if (u != null) {
            for (Iterator<Evento> it = m_listaEventos.listIterator(); it.hasNext();) {
                Evento e = it.next();
                List<Autor> lAut = e.getListaAutores(u);

                boolean flag = false;
                for (Autor aut : lAut) {
                    if (aut.getUtilizador().equals(u)) {
                        flag = true;
                        break;
                    }
                }
                if (flag) {
                    le.add(e);
                }
            }
        }
        return le;

    }

    /**
     * Consulta o utilizador
     * @param strId
     * @return utilizador que corresponde ao id introduzido, caso contrário false
     */
    private Utilizador getUtilizador(String strId) {
       for (Utilizador u : m_empresa.getRegistarUtilizadores().getListaUtilizadores()) {
            String s1 = u.getEmail();
            if (s1.equalsIgnoreCase(strId)) {
                return u;
            }
        }
        return null;

    }
    /**
     * Lista nomes de eventos
     * @param listaEventos
     * @return nomes do eventos
     */
        public static String[] listaNomesDeEventos(List<Evento> listaEventos) {
        String[] nomes = new String[listaEventos.size()];

        for (int i = 0; i < listaEventos.size(); i++) {
            Evento e = listaEventos.get(i);

            if (e != null) {
                nomes[i] = "Evento: " + e.getTitulo();
            }
        }

        return nomes;
    }
    
}
    

