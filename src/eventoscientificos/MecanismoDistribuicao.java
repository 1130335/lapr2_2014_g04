package eventoscientificos;

import java.util.List;

/**
 *
 * @author Rita
 */
public interface MecanismoDistribuicao {

    /**
     * Método a ser usado em todas as classes em que a interface for implementada
     * @param pd processo de distribuicao
     * @return lista de distribuicoes
     */
    public abstract List<Distribuicao> distribui(ProcessoDistribuicao pd);
}
