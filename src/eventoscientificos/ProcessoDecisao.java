/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rita
 */
public class ProcessoDecisao {

    private Artigo artigo;

    /**
     * Construtor do Processo de Decisao que recebe como parâmetro o artigo para
     * ser decidido sobre
     *
     * @param artigo artigo para se decidir sobre
     */
    public ProcessoDecisao(Artigo artigo) {
        this.artigo = artigo;
    }

    /**
     * Construtor de Processo de Decisao sem parametros
     */
    public ProcessoDecisao() {
        this.artigo = new Artigo();
    }

    /**
     * Devolve o artigo referente ao processo de decisao
     *
     * @return artigo do processo de decisao
     */
    public Artigo getArtigo() {
        return this.artigo;
    }

    /**
     * Cria uma nova instância de decisão e devolve-a
     *
     * @return nova instância de decisão
     */
    public Decisao novaDecisao() {
        Decisao decisao = new Decisao();
        return decisao;
    }

}
