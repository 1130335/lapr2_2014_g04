/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;
import java.io.BufferedReader;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author Vaio
 */
public class ImportarXML {
  /**
   *Registar Evento
   */
    private RegistarEvento m_registarEvento;
    /**
     * Registar Utilizador
     */
    private RegistarUtilizador m_registarUtilizador;
    /**
     * Ficheiro XML
     */
    
    private InputStream fXmlFile;
/**
 * Construtor
 * @param regU
 * @param regE 
 */
    public ImportarXML(RegistarUtilizador regU, RegistarEvento regE) {
        this.m_registarUtilizador = regU;
        this.m_registarEvento = regE;
    } 
   /**
    * Lê o ficheiro Xml e adiciona a informação à lista de Eventos.
    * @return a lista de evento
    * @throws FileNotFoundException 
    */ 
    public List<Evento> ImportarEventoXML() throws FileNotFoundException {
         List<Evento> listaEventos = new ArrayList<Evento>();
         
         
    try {
        Scanner xml = new Scanner(new BufferedReader(new FileReader("C:\\Users\\Vaio\\Desktop\\XML.xml")));
       
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(fXmlFile);
        doc.getDocumentElement().normalize();
        System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
        NodeList nList = doc.getElementsByTagName("Evento");
        System.out.println("----------------------------");
        for (int temp = 0; temp < nList.getLength(); temp++) {
 
		Node nNode = nList.item(temp);
                Evento e = m_registarEvento.novoEvento();
		if (nNode.getNodeType() == Node.ELEMENT_NODE) {
 
			Element eElement = (Element) nNode;
 
			System.out.println("Ano: " + eElement.getAttribute("ano"));
			System.out.println("Nome: " + eElement.getElementsByTagName("nome").item(0).getTextContent());
			System.out.println("Host : " + eElement.getElementsByTagName("host").item(0).getTextContent());
			System.out.println("Cidade : " + eElement.getElementsByTagName("cidade").item(0).getTextContent());
			System.out.println("País : " + eElement.getElementsByTagName("país").item(0).getTextContent());
                        System.out.println("Data de Inicio : " + eElement.getElementsByTagName("datadeinicio").item(0).getTextContent());
                        System.out.println("Data de Fim : " + eElement.getElementsByTagName("datadefim").item(0).getTextContent());
                        System.out.println("Website: " + eElement.getElementsByTagName("website").item(0).getTextContent());
                        System.out.println("Nome do Organizador : " + eElement.getElementsByTagName("nomedoorganizadror").item(0).getTextContent());
                        System.out.println("Email : " + eElement.getElementsByTagName("email").item(0).getTextContent());
       
                        listaEventos.add(e);
                        m_registarEvento.addEvento(e);
		}
	}
    } catch (Exception e) {
	e.printStackTrace();
    }
       
     
    
        return listaEventos;
  }
     
  }
 

  


 


