/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sofia
 */
public class RegistarUtilizador {
    
    private List<Utilizador> m_listaUtilizadores;
    
    /**
     * Criar um construtor de RegistarUtilizador vazio
     */
    public RegistarUtilizador() {
        m_listaUtilizadores = new ArrayList<Utilizador>();
    }
    
    /**
     * Cria um novo Utilizador
     * @return 
     */
     public Utilizador novoUtilizador() {
        return new Utilizador();
    }
     
     /**
      * Consulta a lista de Utilizadores
      * @return 
      */
     public List<Utilizador> getListaUtilizadores() {
        return m_listaUtilizadores;
    }

     /**
      * Modificador da Lista de Utilizadores
      * @param m_listaUtilizadores 
      */
    public void setListaUtilizadores(List<Utilizador> m_listaUtilizadores) {
        this.m_listaUtilizadores = m_listaUtilizadores;
    }

    /**
     * Regista o utilizador
     * @param u
     * @return false caso o utilizador não seja adicionado
     */
    public boolean registaUtilizador(Utilizador u) {
        if (u.valida() && validaUtilizador(u)) {
            return addUtilizador(u);
        } else {
            return false;
        }
    }

    /**
     * Valida o utilizador introduzido como parâmetro
     * @param u
     * @return true caso não exista nenhum utilizador igual
     */
    private boolean validaUtilizador(Utilizador u) {
        for (Utilizador uExistente : m_listaUtilizadores) {
            if (uExistente.mesmoQueUtilizador(u)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Consulta o Utilizador do id introduzido como parâmetro
     * @param strId
     * @return utilizador que corresponde ao id introduzido 
     */
    public Utilizador getUtilizador(String strId) {
        for (Utilizador u : this.m_listaUtilizadores) {
            String s1 = u.getUsername();
            if (s1.equalsIgnoreCase(strId)) {
                return u;
            }
        }
        return null;
    }

    
    /**
     * Consulta o Utilizador correspondente ao email introduzido
     * @param strEmail
     * @return Utilizador que corresponde ao email
     */
    public Utilizador getUtilizadorEmail(String strEmail) {
        for (Utilizador u : this.m_listaUtilizadores) {
            String s1 = u.getEmail();
            if (s1.equalsIgnoreCase(strEmail)) {
                return u;
            }
        }

        return null;
    }

    /**
     * Adiciona o Utilizador à lista de Utilizadores
     * @param u
     * @return lista de utilizadores com o utilizador adicionado
     */
    public boolean addUtilizador(Utilizador u) {
        return m_listaUtilizadores.add(u);
    }

   
}
