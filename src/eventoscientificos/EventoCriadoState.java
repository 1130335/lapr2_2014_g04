/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

/**
 *
 * @author Sofia
 */
public class EventoCriadoState implements EventoState {
    
    private Evento e;
    
    /**
     * Altera o estado de registado do evento
     * @param e
     * @return true caso o evento tenha sido registado
     */
    @Override
    public boolean setRegistado(Evento e){
        if (valida()) {
            e.setState(new EventoRegistadoState(e));
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Valida o estado do evento
     * @return 
     */
    public boolean valida() {
        return true;
    }

    @Override
    public boolean setCameraReady(Evento e) {
        return false;
    }

    @Override
    public boolean setRevisto() {
        return false;
    }

}
