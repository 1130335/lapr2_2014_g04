/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;
import eventoscientificos.controller.DefinirTopicosPericiaController;
import java.util.List;
import java.util.ListIterator;
import utils.Utils;
/**
 *
 * @author Vaio
 */
public class DefinirTopicosPericiaUI {

    private Empresa m_empresa;
    private DefinirTopicosPericiaController m_controllerDTP;


    public DefinirTopicosPericiaUI( RegistarEvento registarEvento ) {
        RegistarEvento m_registarEvento = registarEvento;
        m_controllerDTP= new DefinirTopicosPericiaController(m_empresa);
   
    }

   public void run() {
        String strIdRevisor = introduzIdRevisor();
        List<Evento> le = m_controllerDTP.getListaEventos();
        apresentaEventos();

        Evento e = selecionaEvento(le);

        if (e != null) {
            m_controllerDTP.selectEvento(e);

            removeTopicos();
            adicionaTopicos();

            System.out.println("Terminado.");
        } else {
            System.out.println("Definição de tópicos de perícia cancelada.");
        }
    }

    private String introduzIdRevisor() {
        return Utils.readLineFromConsole("Introduza o ID do Revisor: ");
    }

    private void apresentaEventos() {
        System.out.println("Todos os Eventos: ");
        int index = 0;
        for (ListIterator<Evento> it = m_controllerDTP.getListaEventos().listIterator(); it.hasNext();) {
            index++;
            Evento e = it.next();
            System.out.println(index + ". " + e.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }

    private Evento selecionaEvento(List<Evento> le) {
        String opcao;
        int nOpcao;
        do {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        } while (nOpcao < 0 || nOpcao > le.size());

        if (nOpcao == 0) {
            return null;
        } else {
            return le.get(nOpcao - 1);
        }
    }


    
    private void apresentaTopicosEvento() {
        System.out.println("Todos os Topicos: ");
        int index = 0;
        for (ListIterator<Topico> it = m_controllerDTP.getListaTopicosEvento().listIterator(); it.hasNext();) {
            index++;
            Topico t = it.next();
            System.out.println(index + ". " + t.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }

    
    private Topico selecionaTopico(List<Topico> t) {
        String opcao;
        int nOpcao;
        do {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        } while (nOpcao < 0 || nOpcao > t.size());

        if (nOpcao == 0) {
            return null;
        } else {
            return t.get(nOpcao - 1);
        }
    }

    private void removeTopicos() {
        String strResposta;

        strResposta = Utils.readLineFromConsole("Remover Topico (S/N)? ");

        while (strResposta.equalsIgnoreCase("S")) {
            System.out.println("Qual o topico que deseja remover?");
            Topico t = selecionaTopico(m_controllerDTP.getListaTopicosAtuais());

            m_controllerDTP.removerTopico(t);

            strResposta = Utils.readLineFromConsole("Remover Topico (S/N)? ");

        }
    }

    
    private void adicionaTopicos() {
        String strResposta;

        strResposta = Utils.readLineFromConsole("Adicionar Topico (S/N)? ");

        while (strResposta.equalsIgnoreCase("S")) {
            System.out.println("Qual o topico que deseja adicionar?");
            Topico t = selecionaTopico(m_controllerDTP.getListaTopicosEvento());

            m_controllerDTP.addTopico(t);

            strResposta = Utils.readLineFromConsole("Adicionar Topico (S/N)? ");

        }

}
}
