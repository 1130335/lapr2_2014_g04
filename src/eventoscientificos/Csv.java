/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1130429
 */
public class Csv implements Importar {
    private List<String> ano;
    private List<String> nome;
    private List<String> host;
    private List<String> cidade;
    private List<String> pais;
    private List<String> dataInicio;
    private List<String> dataFim;
    private List<String> website;
    private List<Organizador> organizador;
    private List<Integer> n;
    private RegistarUtilizador m_registarUtilizador;
        
        private String csvFile = "EventList.csv";
	private BufferedReader br = null;
	private String line = "";
	private String cvsSplit = ";";
        private Empresa empresa;
   
    
    public Csv (Empresa empresa) throws FileNotFoundException{
        this.empresa = empresa;
        this.ano= ImportarAno();
        this.nome=ImportarNome();
        this.host=ImportHost();
        this.cidade=ImportarCidade();
        this.pais=ImportarPais();
        this.dataInicio=ImportarDataInicio();
        this.dataFim=ImportarDataFim();
        this.website=ImportarWebsite();
        this.n=ImportNorgs();
        this.organizador=ImportarOrganizador();
    
    }

    public List<Integer> getN() {
        return n;
    }
    
    
    
    public List<String> getAno() {
        return ano;
    }

    public List<String> getNome() {
        return nome;
    }

    public List<String> getHost() {
        return host;
    }

    public List<String> getCidade() {
        return cidade;
    }

    public List<String> getPais() {
        return pais;
    }

    public List<String> getData() {
        return dataInicio;
    }

    public List<String> getDuracao() {
        return dataFim;
    }

    public List<String> getWebsite() {
        return website;
    }

    public List<Organizador> getOrganizador() {
        return organizador;
    }

    @Override
    public List<String> ImportarAno() throws FileNotFoundException  {
        br = new BufferedReader(new FileReader(csvFile));
        List<String> year = new ArrayList<String>();
        try {
            while ((line = br.readLine()) != null) {
                
                String[] aux = line.split(cvsSplit);
                year.add(aux[0]);
                
            }   } catch (IOException ex) {
                
                System.out.println("Erro");
                
        }
        year.remove(0);
        return year;
    }

    @Override
    public List<String> ImportarNome() throws FileNotFoundException {
        br = new BufferedReader(new FileReader(csvFile));
        List<String> name = new ArrayList<String>();
        try {
            while ((line = br.readLine()) != null) {
                
                String[] aux = line.split(cvsSplit);
                name.add(aux[1]);
                
                
            }   } catch (IOException ex) {
            Logger.getLogger(Csv.class.getName()).log(Level.SEVERE, null, ex);
        }
        name.remove(0);
        return name;
    }

    @Override
    public List<String> ImportHost() throws FileNotFoundException {
        br = new BufferedReader(new FileReader(csvFile));
        List<String> host = new ArrayList<String>();
        try {
            while ((line = br.readLine()) != null) {
                
                String[] aux = line.split(cvsSplit);
                host.add(aux[2]);
                
                
            }   } catch (IOException ex) {
            Logger.getLogger(Csv.class.getName()).log(Level.SEVERE, null, ex);
        }
        host.remove(0);
        return host;
    }

    @Override
    public List<String> ImportarCidade() throws FileNotFoundException {
        br = new BufferedReader(new FileReader(csvFile));
        List<String> city = new ArrayList<String>();
        try {
            while ((line = br.readLine()) != null) {
                
                String[] aux = line.split(cvsSplit);
                city.add(aux[3]);
                
                
            }   } catch (IOException ex) {
            Logger.getLogger(Csv.class.getName()).log(Level.SEVERE, null, ex);
        }
        city.remove(0);
        return city;
    }

    @Override
    public List<String> ImportarPais()throws FileNotFoundException {
        br = new BufferedReader(new FileReader(csvFile));
        List<String> country = new ArrayList<String>();
        try {
            while ((line = br.readLine()) != null) {
                
                String[] aux = line.split(cvsSplit);
                country.add(aux[4]);
                
                
            }   } catch (IOException ex) {
            Logger.getLogger(Csv.class.getName()).log(Level.SEVERE, null, ex);
        }
        country.remove(0);
        return country;
    }

    @Override
    public List<String> ImportarDataInicio()throws FileNotFoundException{
        br = new BufferedReader(new FileReader(csvFile));
        List<String> date = new ArrayList<String>();
        try {
            while ((line = br.readLine()) != null) {
                
                String[] aux = line.split(cvsSplit);
                date.add(aux[5]);
                
                
            }   } catch (IOException ex) {
            Logger.getLogger(Csv.class.getName()).log(Level.SEVERE, null, ex);
        }
        date.remove(0);
        return date;
    }

    @Override
    public List<String> ImportarDataFim() throws FileNotFoundException {
        br = new BufferedReader(new FileReader(csvFile));
        List<String> duration = new ArrayList<String>();
        try {
            while ((line = br.readLine()) != null) {
                
                String[] aux = line.split(cvsSplit);
                duration.add(aux[6]);
                
                
            }   } catch (IOException ex) {
            Logger.getLogger(Csv.class.getName()).log(Level.SEVERE, null, ex);
        }
        duration.remove(0);
        return duration;
    }

    @Override
    public List<String> ImportarWebsite() throws FileNotFoundException {
        br = new BufferedReader(new FileReader(csvFile));
        List<String> website = new ArrayList<String>();
        try {
            while ((line = br.readLine()) != null) {
                
                String[] aux = line.split(cvsSplit);
                website.add(aux[7]);
                
                
            }   } catch (IOException ex) {
            Logger.getLogger(Csv.class.getName()).log(Level.SEVERE, null, ex);
        }
        website.remove(0);
        return website;
    }

    @Override
    public List<Organizador> ImportarOrganizador() throws FileNotFoundException {
        br = new BufferedReader(new FileReader(csvFile));
        List<String> organizer = new ArrayList<String>();
        List<String> email = new ArrayList<String>();
        try {
            while ((line = br.readLine()) != null) {
                
                String[] aux = line.split(cvsSplit);
                for(int i=8;i<aux.length;i=i+2){
                    organizer.add(aux[i]);
                    email.add(aux[i+1]);
                }
                
            }   } catch (IOException ex) {
            Logger.getLogger(Csv.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        List<Organizador> org = new ArrayList<Organizador>();
        List<Integer> in = getN();
        int i = in.get(0);
        for(int n=i;n<organizer.size();n++){
           if(m_registarUtilizador.getUtilizador(organizer.get(n))!=null){
            Utilizador u = m_registarUtilizador.getUtilizador(organizer.get(n));
            Organizador o = new Organizador(u.getNome(),u);
            org.add(o);
           }
           }  

        return org;
    }

    @Override
    public List<Integer> ImportNorgs() throws FileNotFoundException {
       br = new BufferedReader(new FileReader(csvFile));
        List<Integer> num = new ArrayList<Integer>();
        int n=0;
        try {
            while ((line = br.readLine()) != null) {
                 String[] aux = line.split(cvsSplit);
               for(int i=8;i<aux.length;i=i+2){
                    n++;
               }
               
               num.add(n);
               n=0;
               
            }   } catch (IOException ex) {
                
                System.out.println("Erro");
                
        }
        return num;
    }
}
