/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;



/**
 *
 * @author Nuno Silva
 */

public class Organizador
{
    private final String m_strNome;
    private Utilizador m_utilizador;

    /**
     * Cria um construtor de organizador que recebe o id e o utilizador como parametro
     * @param strId
     * @param u 
     */
    public Organizador(String strId, Utilizador u )
    {
        m_strNome = u.getNome();
        this.setUtilizador(u);
    }

    /**
     * Altera o utilizador
     * @param u 
     */
    private void setUtilizador(Utilizador u)
    {
        m_utilizador = u;
    }
    
    /**
     * Valida o organizador
    */
    public boolean valida()
    {
        return true;
    }
    
    /**
     * Consulta o nome
     * @return 
     */
    public String getNome()
    {
        return m_strNome;
    }
    
    /**
     * Consulta o utilizador
     * @return 
     */
    public Utilizador getUtilizador()
    {
        return m_utilizador;
    }

    /**
     * Apresenta textualmente o utilizador
     * @return 
     */
    @Override
    public String toString()
    {
        return m_utilizador.toString();
    }
}
