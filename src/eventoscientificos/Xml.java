/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1130429
 */
public class Xml implements Importar {
    
    private List<String> ano;
    private List<String> nome;
    private List<String> host;
    private List<String> cidade;
    private List<String> pais;
    private List<String> DataInicio;
    private List<String> DataFim;
    private List<String> website;
    private List<Organizador> organizador;
    private List<Integer> n;
    private Empresa m_empresa;
    
        private String xmlFile = "EventList.xml";
	private BufferedReader br = null;
	private String line = "";
        private String xmlSplit = " ";
    
    
    public Xml(Empresa empresa) throws FileNotFoundException{
        this.m_empresa = empresa;
        this.ano= ImportarAno();
        this.nome=ImportarNome();
        this.host=ImportHost();
        this.cidade=ImportarCidade();
        this.pais=ImportarPais();
        this.DataInicio=ImportarDataInicio();
        this.DataFim=ImportarDataFim();
        this.website=ImportarWebsite();
        this.n=ImportNorgs();
        this.organizador=ImportarOrganizador();
}

    public List<String> getAno() {
        return ano;
    }

    public List<String> getNome() {
        return nome;
    }

    public List<String> getHost() {
        return host;
    }

    public List<String> getCidade() {
        return cidade;
    }

    public List<String> getPais() {
        return pais;
    }

    public List<String> getDataInicio() {
        return DataInicio;
    }

    public List<String> getDataFim() {
        return DataFim;
    }

    public List<String> getWebsite() {
        return website;
    }

    public List<Organizador> getOrg() {
        return organizador;
    }

    public List<Integer> getN() {
        return n;
    }
    
    @Override
    public List<String> ImportarAno() throws FileNotFoundException {
        br = new BufferedReader(new FileReader(xmlFile));
        List<String> ano = new ArrayList<String>();
        String auxStr;
        
        try {        
            while ((line=br.readLine()) != null) {
               auxStr=line.replaceAll("<", " ").replaceAll(">", " ");
                String[] aux = auxStr.split(xmlSplit);
                for(int i =0; i<aux.length;i++){
                    if(aux[i].equalsIgnoreCase("year")){
                        ano.add(aux[i+1]);
                    }
                }
                
                
            }
        } catch (IOException ex) {
            System.out.println("Erro");
        }
        return ano;
    }

    @Override
    public List<String> ImportarNome() throws FileNotFoundException {
        br = new BufferedReader(new FileReader(xmlFile));
        List<String> nome = new ArrayList<String>();
        String auxStr;
        String auxName;
        
        try {        
            while ((line=br.readLine()) != null) {
               auxStr=line.replaceAll("<", " ").replaceAll(">", " ");
                String[] aux = auxStr.split(xmlSplit);
                for(int i =0; i<aux.length;i++){
                    if(aux[i].equalsIgnoreCase("name")){
                        auxName=line.replaceAll("<name>", "").replaceAll("</name>","").replaceAll("    ","");
                        nome.add(auxName);
                    }
                }               
            }
        } catch (IOException ex) {
            System.out.println("Erro");
        }
        return nome;
    }

    @Override
    public List<String> ImportHost() throws FileNotFoundException {
        br = new BufferedReader(new FileReader(xmlFile));
        List<String> host = new ArrayList<String>();
        String auxStr;
        String auxName;
        
        try {        
            while ((line=br.readLine()) != null) {
               auxStr=line.replaceAll("<", " ").replaceAll(">", " ");
                String[] aux = auxStr.split(xmlSplit);
                for(int i =0; i<aux.length;i++){
                    if(aux[i].equalsIgnoreCase("host")){
                        auxName=line.replaceAll("<host>", "").replaceAll("</host>","").trim();
                        host.add(auxName);
                    }
                }               
            }
        } catch (IOException ex) {
            System.out.println("Erro");
        }
        return host;
    }
    

    @Override
    public List<String> ImportarCidade() throws FileNotFoundException {
                br = new BufferedReader(new FileReader(xmlFile));
        List<String> cidade = new ArrayList<String>();
        String auxStr;
        String auxName;
        
        try {        
            while ((line=br.readLine()) != null) {
               auxStr=line.replaceAll("<", " ").replaceAll(">", " ");
                String[] aux = auxStr.split(xmlSplit);
                for(int i =0; i<aux.length;i++){
                    if(aux[i].equalsIgnoreCase("city")){
                        auxName=line.replaceAll("<city>", "").replaceAll("</city>","")
                                .trim();
                        cidade.add(auxName);
                    }
                }               
            }
        } catch (IOException ex) {
            System.out.println("Erro");
        }
        return cidade;
    }

    @Override
    public List<String> ImportarPais() throws FileNotFoundException {
        br = new BufferedReader(new FileReader(xmlFile));
        List<String> pais = new ArrayList<String>();
        String auxStr;
        String auxName;
        
        try {        
            while ((line=br.readLine()) != null) {
               auxStr=line.replaceAll("<", " ").replaceAll(">", " ");
                String[] aux = auxStr.split(xmlSplit);
                for(int i =0; i<aux.length;i++){
                    if(aux[i].equalsIgnoreCase("country")){
                        auxName=line.replaceAll("<country>", "").replaceAll("</country>","").trim();
                        pais.add(auxName);
                    }
                }               
            }
        } catch (IOException ex) {
            System.out.println("Erro");
        }
        return pais;
    
    }

    @Override
    public List<String> ImportarDataInicio() throws FileNotFoundException {
        br = new BufferedReader(new FileReader(xmlFile));
        List<String> data = new ArrayList<String>();
        String auxStr;
        String auxDate;
        
        try {        
            while ((line=br.readLine()) != null) {
               auxStr=line.replaceAll("<", " ").replaceAll(">", " ");
                String[] aux = auxStr.split(xmlSplit);
                for(int i =0; i<aux.length;i++){
                    if(aux[i].equalsIgnoreCase("begin_date")){
                        auxDate=line.replaceAll("<begin_date>", "")
                                .replaceAll("</begin_date>","").trim();
                        data.add(auxDate);
                    }
                }               
            }
        } catch (IOException ex) {
            System.out.println("Erro");
        }
        return data;
    
    }

    @Override
    public List<String> ImportarDataFim() throws FileNotFoundException {
        br = new BufferedReader(new FileReader(xmlFile));
        List<String> data = new ArrayList<String>();
        String auxStr;
        String auxDate;
        
        try {        
            while ((line=br.readLine()) != null) {
               auxStr=line.replaceAll("<", " ").replaceAll(">", " ");
                String[] aux = auxStr.split(xmlSplit);
                for(int i =0; i<aux.length;i++){
                    if(aux[i].equalsIgnoreCase("end_date")){
                        auxDate=line.replaceAll("<end_date>", "")
                                .replaceAll("</end_date>","").trim();
                        data.add(auxDate);
                    }
                }               
            }
        } catch (IOException ex) {
          
        }
        return data;
    }

    @Override
    public List<String> ImportarWebsite() throws FileNotFoundException {
        br = new BufferedReader(new FileReader(xmlFile));
        List<String> website = new ArrayList<String>();
        String auxStr;
        String auxName;
        
        try {        
            while ((line=br.readLine()) != null) {
               auxStr=line.replaceAll("<", " ").replaceAll(">", " ");
                String[] aux = auxStr.split(xmlSplit);
                for(int i =0; i<aux.length;i++){
                    if(aux[i].equalsIgnoreCase("web_page")){
                        auxName=line.replaceAll("<web_page>", "").replaceAll("</web_page>","").trim();
                        website.add(auxName);
                    }
                }               
            }
        } catch (IOException ex) {
            System.out.println("Erro");
        }
        return website;
    
    }

    @Override
    public List<Organizador> ImportarOrganizador() throws FileNotFoundException {
     br = new BufferedReader(new FileReader(xmlFile));
        List<Organizador> orgs = new ArrayList<Organizador>();
        List<String> email = new ArrayList<String>();
        List<String> nome = new ArrayList<String>();
        String auxStr;
        String auxName;
        
        try {        
            while ((line=br.readLine()) != null) {
               auxStr=line.replaceAll("<", " ").replaceAll(">", " ");
                String[] aux = auxStr.split(xmlSplit);
                for(int i =0; i<aux.length;i++){
                    if(aux[i].equalsIgnoreCase("org_name")){
                        auxName=line.replaceAll("<org_name>", "")
                                .replaceAll("</org_name>","").trim();
                        nome.add(auxName);
                    }
                    if(aux[i].equalsIgnoreCase("email")){
                        auxName=line.replaceAll("<email>", "")
                                .replaceAll("</email>","").trim();
                        email.add(auxName);
                    }
                
                }               
            }
        
        
        } catch (IOException ex) {
            System.out.println("Erro");
        }
        for(int n=0;n<nome.size();n++){
            Utilizador u = new Utilizador();
            u.setNome(nome.get(n));
            u.setEmail(email.get(n));
            Organizador o = new Organizador(nome.get(n),u);
            orgs.add(o);
        }
        return orgs;
    
    }
    
    @Override
    public List<Integer> ImportNorgs() throws FileNotFoundException {
        br = new BufferedReader(new FileReader(xmlFile));
        List<Integer> n_orgs = new ArrayList<Integer>();
        String auxStr;
        String auxName;
        int n=0;
        try {
            while ((line=br.readLine()) != null) {
                auxStr=line.replaceAll("<", " ").replaceAll(">", " ");
                String[] aux = auxStr.split(xmlSplit);
                for(int i =0; i<aux.length;i++){
                    if(aux[i].equalsIgnoreCase("organizer")){
                        n++;
                    }
                    if(aux[i].equalsIgnoreCase("/event")){
                        n_orgs.add(n);
                        n=0;
                    }
                    
                    
                }
                
            }
        } catch (IOException ex) {
            Logger.getLogger(Xml.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n_orgs;
    }

}