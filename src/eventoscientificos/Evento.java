/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author 1130429/1130406
 */
public class Evento {

    private String m_ano;
    private String m_idEvento;
    private String m_Host;
    private String m_Cidade;
    private String m_Pais;
    private String m_Website;
    private String m_Email;
    private String m_strDataLimiteRegistoAutor;
    private String m_strDataLimiteRevisao;
    private String m_strTitulo;
    private String m_strDescricao;
    private Local m_local;
    private String m_strDataInicio;
    private String m_strDataFim;
    private String m_Duracao;
    private String m_Organizador;
    private String m_strDataLimiteSubmissao;
    public int nrMaxTopicos;
    private String dataRevisao;
    private String dataLimiteSubmissaoFinal;
    private String dataLimiteRegisto;
    private float m_preçoUnitarioFullPaper;
    private float m_preçoUnitarioShortPaper;
    private float m_preçoUnitarioPosterPaper;
    private List<Organizador> m_listaOrganizadores;
    private List<Submissao> m_listaSubmissoes;
    private CP m_cp;
    private List<Topico> m_listaTopicos;
    private List<Artigo> m_listaArtigos;
    private List<Revisor> m_listaRevisores;
    private Submissao m_submissao;
    private List<Autor> m_listaAutores;

    private EventoState state;
    private static int[] diasPorMes = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31};

    public String vF;
    public String vS;
    public String vP;
    private int m_formula;

    /**
     * Construtor de Evento caso não sejam introduzidos quaisquer dados como
     * parâmetro
     */
    public Evento() {
        m_local = new Local();
        m_listaOrganizadores = new ArrayList<Organizador>();
        m_listaSubmissoes = new ArrayList<Submissao>();
        m_listaTopicos = new ArrayList<Topico>();
    }

    /**
     * Construtor de Evento que recebe o titulo e a descrição como parâmetro
     *
     * @param titulo
     * @param descricao
     */
    public Evento(String titulo, String descricao) {
        m_local = new Local();
        m_listaOrganizadores = new ArrayList<Organizador>();
        m_listaSubmissoes = new ArrayList<Submissao>();
        m_listaTopicos = new ArrayList<Topico>();
        this.setTitulo(titulo);
        this.setDescricao(descricao);
    }

    /**
     * Consulta os artigos existentes
     */
    public void getArtigos() {
        for (Iterator<Submissao> it = m_listaSubmissoes.listIterator(); it.hasNext();) {
            Submissao s = it.next();
            Artigo a = s.getArtigo();
            m_listaArtigos.add(a);
        }
    }

    /**
     * Cria uma nova CP
     *
     * @return cp
     */
    public CP novaCP() {
        m_cp = new CP();

        return m_cp;
    }

    /**
     * Modificador do id do evento
     *
     * @param idEvento
     */
    public void setIDEvento(String idEvento) {
        this.m_idEvento = idEvento;
    }

    /**
     * Consulta o id do evento
     *
     * @return idEvento
     */
    public String getIDEvento() {
        return this.m_idEvento;
    }

    /**
     * Modifica o host
     *
     * @param Host
     */
    public void setHost(String Host) {
        this.m_Host = Host;
    }

    /**
     * Consulta o host
     *
     * @return host
     */
    public String getHost() {
        return this.m_Host;
    }

    /**
     * Modifica o ano
     *
     * @param ano
     */
    public void setAno(String ano) {
        this.m_ano = ano;
    }

    /**
     * Consulta o ano
     *
     * @return ano
     */
    public String getAno() {
        return this.m_ano;
    }

    /**
     * Consulta o título
     *
     * @return título
     */
    public String getTitulo() {
        return this.m_strTitulo;
    }

    /**
     * Modifica a cidade
     *
     * @param Cidade
     */
    public void setCidade(String Cidade) {
        this.m_Cidade = Cidade;
    }

    /**
     * Consulta a cidade
     *
     * @return cidade
     */
    public String getCidade() {
        return this.m_Cidade;
    }

    /**
     * Modificador do país
     *
     * @param Pais
     */
    public void setPais(String Pais) {
        this.m_Pais = Pais;
    }

    /**
     * Consulta o país
     *
     * @return país
     */
    public String getPais() {
        return this.m_Pais;
    }

    /**
     * Modificador do website
     *
     * @param Website
     */
    public void setWebsite(String Website) {
        this.m_Website = Website;
    }

    /**
     * Consulta o website
     *
     * @return website
     */
    public String getWebsite() {
        return this.m_Website;
    }

    /**
     * Modificador do Email
     *
     * @param Email
     */
    public void setEmail(String Email) {
        this.m_Email = Email;
    }

    /**
     * Consulta o Email
     *
     * @return email
     */
    public String getEmail() {
        return this.m_Email;
    }

    /**
     * Modificador da Data de limite de registo do autor no evento
     *
     * @param strDataLimiteRegistoAutor
     */
    public void setDataLimiteRegistoAutor(String strDataLimiteRegistoAutor) {
        this.m_strDataLimiteRegistoAutor = strDataLimiteRegistoAutor;
    }

    /**
     * Consulta a Data de limite de registo do autor no evento
     *
     * @return Data de limite de registo do autor no evento
     */
    public String getDataLimiteDataRegistoAutor() {
        return this.m_strDataLimiteRegistoAutor;
    }

    /**
     * Modificado da Data Limite de Revisão
     *
     * @param strDataLimiteRevisao
     */
    public void setDataLimiteRevisao(String strDataLimiteRevisao) {
        this.m_strDataLimiteRevisao = strDataLimiteRevisao;
    }
    
    /**
     * Consulta a data limite de revisao
     *
     * @return data limite de revisao
     */
    public String getDataLimiteDataRevisao() {
        return this.m_strDataLimiteRevisao;
    }
   
    /**
     * Modificador da duração do evento
     *
     * @param Duracao
     */
    public void setDuracao(String Duracao) {
        this.m_Duracao = Duracao;
    }

    /**
     * Consulta a duração do evento
     *
     * @return duração
     */
    public String getDuracao() {
        return this.m_Duracao;
    }

    /**
     * Modificador do Organizador
     *
     * @param Organizador
     */
    public void setOrganizador(String Organizador) {
        this.m_Organizador = Organizador;
    }

    /**
     * Consulta o Organizador
     *
     * @return organizador
     */
    public String getOrganizador() {
        return this.m_Organizador;

    }

    /**
     * Modificador do título do evento
     *
     * @param strTitulo
     */
    public final void setTitulo(String strTitulo) {
        this.m_strTitulo = strTitulo;
    }

    /**
     * Modificador da descriçao do evento
     *
     * @param strDescricao
     */
    public final void setDescricao(String strDescricao) {
        this.m_strDescricao = strDescricao;
    }

    /**
     * Modificador da data de inicio do evento
     *
     * @param strDataInicio
     */
    public void setDataInicio(String strDataInicio) {
        this.m_strDataInicio = strDataInicio;
    }

    /**
     * Modificador da data final do evento
     *
     * @param strDataFim
     */
    public void setDataFim(String strDataFim) {
        this.m_strDataFim = strDataFim;
    }

    /**
     * Modificador da data de submissao do artigo para revisao
     *
     * @param strDataLimiteSubmissao
     */
    // adicionada na iteração 2
    public void setDataLimiteSubmissao(String strDataLimiteSubmissao) {
        this.m_strDataLimiteSubmissao = strDataLimiteSubmissao;
    }

    /**
     * Modificador do local do evento
     *
     * @param strLocal
     */
    public void setLocal(String strLocal) {
        this.m_local.setLocal(strLocal);
    }

    /**
     * Modificador do nr de topicos
     *
     * @param nrMaxTopicos
     */
    public void setNrMaxTopicos(int nrMaxTopicos) {
        this.nrMaxTopicos = nrMaxTopicos;
    }

    /**
     * Modificador da data de revisao do artigo para revisao
     *
     * @param dataRevisao
     */
    public void setDataRevisao(String dataRevisao) {
        this.dataRevisao = dataRevisao;
    }

    /**
     * Modificador da data de submissao do artigo final
     *
     * @param dataLimiteSubmissaoFinal
     */
    public void setDataLimiteSubmissaoFinal(String dataLimiteSubmissaoFinal) {
        this.dataLimiteSubmissaoFinal = dataLimiteSubmissaoFinal;
    }

    /**
     * Modificador da data limite de registo do autor no evento
     *
     * @param dataLimiteRegisto
     */
    public void setDataLimiteRegisto(String dataLimiteRegisto) {
        this.dataLimiteRegisto = dataLimiteRegisto;
    }

    /**
     * Consulta a lista de Organizadores
     *
     * @return lista de organizadores
     */
    public List<Organizador> getListaOrganizadores() {
        List<Organizador> lOrg = new ArrayList<Organizador>();

        for (ListIterator<Organizador> it = m_listaOrganizadores.listIterator(); it.hasNext();) {
            lOrg.add(it.next());
        }

        return lOrg;
    }

    /**
     * Consulta a lista de revisores
     *
     * @return lista de revisores
     */
    public List<Revisor> getListaRevisores() {
        List<Revisor> lRev = new ArrayList<Revisor>();

        for (ListIterator<Revisor> it = m_listaRevisores.listIterator(); it.hasNext();) {
            lRev.add(it.next());
        }

        return lRev;
    }

    /**
     * Adiciona um Organizador novo
     *
     * @param strId
     * @param u
     * @return método para adicionar o organizador à lista
     */
    public boolean addOrganizador(String strId, Utilizador u) {
        Organizador o = new Organizador(strId, u);

        o.valida();

        return addOrganizador(o);
    }

    /**
     * Adiciona o organizador
     *
     * @param o
     * @return lista de organizadores com o organizador introduzido adicionado
     */
    public boolean addOrganizador(Organizador o) {
        return m_listaOrganizadores.add(o);
    }

    /**
     * Valida
     *
     * @return true caso seja valido
     */
    public boolean valida() {
        return true;
    }

    /**
     * Modificador da CP
     *
     * @param cp
     */
    public void setCP(CP cp) {
        m_cp = cp;
    }

    /**
     * Modificador do valor do artigo full
     * @param F 
     */
    public void setValorFull(String F) {
        this.vF = F;
    }
/**
 * Modificador do valor do artigo short
 * @param S 
 */
    public void setValorShort(String S) {
        this.vS = S;
    }
/**
 * Modificador do valor do artigo poster
 * @param P 
 */
    public void setValorPoster(String P) {
        this.vP = P;
    }
/**
 * Apresenta textualmente o titulo do evento
 * @return titulo do evento
 */
    @Override
    public String toString() {
        return this.m_strTitulo;
    }
/**
 * Verifica se aceita submissoes
 * @return true caso aceite
 */
    public boolean aceitaSubmissoes() {
        return true;
    }
/**
 * Cria uma nova submissao
 * @return nova submissao
 */
    public Submissao novaSubmissao() {
        return new Submissao();
    }
/**
 * Adiciona a submissao à lista de submissoes
 * @param submissao
 * @return lista de submissoes com a submissao adicionada caso esta seja valida, caso contrario retorna false
 */
    public boolean addSubmissao(Submissao submissao) {
        if (validaSubmissao(submissao)) {
            return this.m_listaSubmissoes.add(submissao);
        } else {
            return false;
        }
    }
/**
 * Valida a submissao
 * @param submissao
 * @return submissao validada
 */
    private boolean validaSubmissao(Submissao submissao) {
        return submissao.valida();
    }
/**
 * Consulta a lista de submissoes
 * @return  lista de submissoes
 */
    public List<Submissao> getListaSubmissoes() {
        return this.m_listaSubmissoes;
    }
/**
 * Consulta a cp
 * @return cp
 */
    public CP getCP() {
        return this.m_cp;
    }

    /**
     * Consulta os topicos
     * @return lista de topicos
     */
    public List<Topico> getTopicos() {
        return m_listaTopicos;
    }
/**
 * Cria um novo topico
 * @return novo topico
 */
    public Topico novoTopico() {
        return new Topico();
    }
/**
 * Adiciona topicos À lista de topicos
 * @param t
 * @return lista de topicos
 */
    public boolean addTopico(Topico t) {
        return this.m_listaTopicos.add(t);
    }
/**
 * Valida o topico
 * @param t 
 * @return true caso o topico seja valido e false caso contrario
 */
    public boolean validaTopico(Topico t) {
        if (t.valida() && validaGlobalTopico(t)) {
            return true;
        } else {
            return false;
        }
    }
/**
 * Validaçao global do topico
 * @param t
 * @return true caso seja valido
 */
    private boolean validaGlobalTopico(Topico t) {
        return true;
    }

    /**
     * Consulta a lista de artigos de revisores
     * @param u
     * @return lista de artigos do revisor
     */
    public List<Artigo> getListaArtigosRevisor(Utilizador u) {
        List<Artigo> laRevisor = new ArrayList<Artigo>();

        if (u != null) {
            for (Iterator<Artigo> it = m_listaArtigos.listIterator(); it.hasNext();) {
                Artigo a = it.next();
                List<Revisor> lRev = a.getListaRevisores();

                boolean bRet = false;
                for (Revisor rev : lRev) {
                    if (rev.getUtilizador().equals(u)) {
                        bRet = true;
                        break;
                    }
                }
                if (bRet) {
                    laRevisor.add(a);
                }
            }
        }
        return laRevisor;
    }

    /**
     * Altera o estado criado do evento
     * @return false
     */
    public boolean SetCriado() {
        return false;
    }

    /**
     * Altera o estado registado do evento
     * @return  false
     */
    public boolean SetRegistado() {
        return false;
    }

    /**
     * Altera o estado de camera ready do evento
     * @return false
     */
    public boolean setCameraReady() {
        return false;
    }
/**
 * Altera a submissão
 * @param s 
 */
    public void setSubmissao(Submissao s) {
        this.m_submissao = s;
    }
/**
 * Altera o estado do evento
 * @param s 
 */
    public void setState(EventoState st) {
        state = st;
    }

    /**
     * Consulta o estado do evento
     * @return estado do evento
     */
    public EventoState getState() {
        return state;
    }
/**
 * Valida a ordem das datas introduzidas 
 * @param m_strDataInicio
 * @param m_strDataFim
 * @param dataRevisao
 * @param dataLimiteSubmissaoFinal
 * @param dataLimiteRegisto
 * @param m_strDataLimiteSubmissao
 * @return true caso as datas estejam na ordem correta
 */
    public boolean validaDatas(String m_strDataInicio, String m_strDataFim, String dataRevisao, String dataLimiteSubmissaoFinal, String dataLimiteRegisto, String m_strDataLimiteSubmissao) {

        String aux[] = m_strDataLimiteSubmissao.split("/");
        int dia = Integer.parseInt(aux[0]);
        int mes = Integer.parseInt(aux[1]);
        int ano = Integer.parseInt(aux[2]);

        int totalDias = contaDias(ano, dia, mes);

        String aux1[] = dataRevisao.split("/");
        int dia1 = Integer.parseInt(aux1[0]);
        int mes1 = Integer.parseInt(aux1[1]);
        int ano1 = Integer.parseInt(aux1[2]);

        int totalDias1 = contaDias(ano1, dia1, mes1);

        String aux2[] = dataLimiteSubmissaoFinal.split("/");
        int dia2 = Integer.parseInt(aux2[0]);
        int mes2 = Integer.parseInt(aux2[1]);
        int ano2 = Integer.parseInt(aux2[2]);

        int totalDias2 = contaDias(ano2, dia2, mes2);

        String aux3[] = dataLimiteRegisto.split("/");
        int dia3 = Integer.parseInt(aux3[0]);
        int mes3 = Integer.parseInt(aux3[1]);
        int ano3 = Integer.parseInt(aux3[2]);

        int totalDias3 = contaDias(ano3, dia3, mes3);

        String aux4[] = m_strDataInicio.split("/");
        int dia4 = Integer.parseInt(aux4[0]);
        int mes4 = Integer.parseInt(aux4[1]);
        int ano4 = Integer.parseInt(aux4[2]);

        int totalDias4 = contaDias(ano4, dia4, mes4);

        String aux5[] = m_strDataFim.split("/");
        int dia5 = Integer.parseInt(aux5[0]);
        int mes5 = Integer.parseInt(aux5[1]);
        int ano5 = Integer.parseInt(aux5[2]);

        int totalDias5 = contaDias(ano5, dia5, mes5);

        if (totalDias < totalDias1) {
            if (totalDias1 < totalDias2) {
                if (totalDias2 < totalDias3) {
                    if (totalDias3 < totalDias4) {
                        if (totalDias4 <= totalDias5) {
                            return true;
                        }

                    }
                }
            }
        }
        return false;
    }

    /**
     * Conta os dias de cada data de forma a poder compara-las no método ValidaDatatas
     * @param ano
     * @param dia
     * @param mes
     * @return totalDias
     */
    private int contaDias(int ano, int dia, int mes) {
        int totalDias = 0;

        for (int i = 1; i < ano; i++) {
            totalDias += isAnoBissexto(i) ? 366 : 365;
        }
        for (int i = 1; i < mes; i++) {
            totalDias += diasPorMes[i];
        }
        totalDias += (isAnoBissexto(ano) && mes > 2) ? 1 : 0;
        totalDias += dia;

        return totalDias;

    }

    /**
     * Verifica se o ano é bissexto
     * @param ano
     * @return true caso o ano seja bissexto
     */
    public static boolean isAnoBissexto(int ano) {
        return ano % 4 == 0 && ano % 100 != 0 || ano % 400 == 0;
    }

    /**
     * Consulta a Lista de Autores
     * @param u
     * @return lista de autores
     */
    public List<Autor> getListaAutores(Utilizador u) {
        getArtigos();
        List<Autor> lAutores = new ArrayList<Autor>();

        for (Iterator<Artigo> it = m_listaArtigos.listIterator(); it.hasNext();) {
            Artigo a = it.next();
            List<Autor> lAutor = a.getListaAutores();

            boolean bRet = false;
            for (Autor aut : lAutor) {
                if (aut.getUtilizador().equals(u)) {
                    lAutores.add(aut);

                }
            }

        }

        return lAutores;
    }

    /**
     * Altera a lista de Artigos
     * @param listaArtigos 
     */
    public void setListaArtigos(List<Artigo> listaArtigos) {
        this.m_listaArtigos.addAll(listaArtigos);
    }

    /**
     * Adiciona o artigo à lista de artigos
     * @param a 
     */
    public void addArtigo(Artigo a) {
        this.m_listaArtigos.add(a);
    }

    /**
     * Consulta os artigos que correspondem ao ID introduzido
     * @param strId
     * @return artigos correspondentes ao id
     */
    public Artigo getArtigoID(String strId) {
        Artigo artigo = null;
        for (Iterator<Artigo> it = m_listaArtigos.listIterator(); it.hasNext();) {
            Artigo a = it.next();
            if (a.getIDArtigo().equalsIgnoreCase(strId)) {
                artigo = a;
            }
        }
        return artigo;
    }

    /**
     * Consulta o valor do artigo full
     * @return valor do artigo full
     */
    public String getValorFull() {
        return this.vF;
    }
/**
 * Consulta o valor do artigo short
 * @return valor do artigo short
 */
    public String getValorShort() {
        return this.vS;
    }
/**
 * Consilta o valor do artigo poster
 * @return valor do artigo poster
 */
    public String getValorPoster() {
        return this.vP;
    }
/**
 * Altera a formula de pagamento
 * @param F 
 */
    public void setFormula(String F) {
        int f = Integer.parseInt(F);
        this.m_formula = f;
    }
/**
 * Consulta a formula de pagamento
 * @return 
 */
    public int getFormula() {
        return this.m_formula;
    }
/**
 * Consulta a lista de artigos do autor
 * @param u
 * @return lista de artigos do autor
 */
    public List<Artigo> getListaArtigosAutor(Utilizador u) {
        getArtigos();
        List<Artigo> laAutor = new ArrayList<Artigo>();

        if (u != null) {
            for (Iterator<Artigo> it = m_listaArtigos.listIterator(); it.hasNext();) {
                Artigo a = it.next();
                List<Autor> lAut = a.getListaAutores();

                boolean bRet = false;
                for (Autor aut : lAut) {
                    if (aut.getUtilizador().equals(u)) {
                        bRet = true;
                        break;
                    }
                }
                if (bRet) {
                    laAutor.add(a);
                }
            }
        }
        return laAutor;
    }

    public void novoProcessoDistribuicao(MecanismoDistribuicao m) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
    }
    
    /**
     * Lista os nomes dos artigos
     * @param listaArtigos
     * @return 
     */
    public String[] listaNomesDeArtigos(List<Artigo> listaArtigos) {
        String[] nomes = new String[listaArtigos.size()];

        for (int i = 0; i < listaArtigos.size(); i++) {
            Artigo a = listaArtigos.get(i);

            if (a != null) {
                nomes[i] = "Artigo: " + a.getTitulo();
            }
        }

        return nomes;
    }
}
