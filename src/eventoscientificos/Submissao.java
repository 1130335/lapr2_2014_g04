/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Submissao {

    private Artigo m_artigo;

    /**
     *
     */
    public Submissao() {

    }

    /**
     * Cria uma nova instância de artigo
     * @return nova instancia de artigo
     */
    public Artigo novoArtigo() {
        return new Artigo();
    }

    /**
     * Devolve a informacao sobre a submissao
     * @return informacao
     */
    public String getInfo() {
        return this.toString();
    }

    /**
     * Altera o artigo submetido
     * @param artigo artigo submetido
     */
    public void setArtigo(Artigo artigo) {
        this.m_artigo = artigo;
    }

    /**
     * Devolve o artigo de uma determinada submissao
     * @return artigo da submissao
     */
    public Artigo getArtigo() {
        return this.m_artigo;
    }

    /**
     *
     * @return
     */
    public boolean valida() {
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Submissão:\n";
    }
}
