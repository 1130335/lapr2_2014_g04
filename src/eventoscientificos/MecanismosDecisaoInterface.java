/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.util.List;

/**
 *
 * @author Rita
 */
public interface MecanismosDecisaoInterface {

    /**
     * Interface que contém o método de decisao a ser implementado em todas as
     * classes que a implementem
     *
     * @param pd processo de decisao
     * @return decisao tomada pelo mecanismo
     */
    public abstract Decisao decide(ProcessoDecisao pd);
}
