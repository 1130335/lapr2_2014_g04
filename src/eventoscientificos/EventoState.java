/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

/**
 *
 * @author Sofia
 */
public interface EventoState {
    
    public boolean setRegistado (Evento e);
    
    public boolean setCameraReady (Evento e);
    
    public boolean valida();

    public boolean setRevisto();
    
    
    
}
