/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

/**
 *
 * @author Lapr2_g04
 */
public class Autor {

    /**
     * O utilizador.
     */
    private Utilizador m_Utilizador;
    /**
     * O nome do autor.
     */
    private String m_strNome;
    /**
     * A afiliacao do auto.
     */
    private String m_strAfiliacao;
    /**
     * O email do autor.
     */
    private String m_strEMail;

    /**
     * Costroi uma instancia de autor.
     */
    public Autor() {
        this.m_Utilizador = null;
    }

    /**
     * Modifica o nome do autor.
     *
     * @param strNome o novo nome
     */
    public void setNome(String strNome) {
        this.m_strNome = strNome;
    }

    /**
     * Devolve o nome do autor.
     *
     * @return o nome do autor
     */
    public String getNome() {
        return this.m_strNome;
    }

    /**
     * Modifica a afiliacao do autor.
     *
     * @param strAfiliacao a nova afiliacao
     */
    public void setAfiliacao(String strAfiliacao) {
        this.m_strAfiliacao = strAfiliacao;
    }

    /**
     * Modifica o email do autor
     *
     * @param strEMail o novo email
     */
    public void setEMail(String strEMail) {
        this.m_strEMail = strEMail;
    }
    
    public String getEmail() {
        return m_strEMail;
    }

    /**
     * Modifica o Utilizador
     *
     * @param utilizador o novo utilizador
     */
    public void setUtilizador(Utilizador utilizador) {
        this.m_Utilizador = utilizador;
    }

    /**
     * Devolve o utilizador
     *
     * @return
     */
    public Utilizador getUtilizador() {
        return this.m_Utilizador;
    }

    /**
     * Devolve a validacao.
     * @return resultado da validacao
     */
    public boolean valida() {
        return true;
    }

    boolean podeSerCorrespondente() {
        return (m_Utilizador != null);
    }

    @Override
    public String toString() {
        return this.m_strNome + " - " + this.m_strAfiliacao + " - " + this.m_strEMail;
    }

    /**
     *
     * @param obj
     * @return
     *
     * Um Autor é identificado pelo seu endereço de e-mail que deve ser único no
     * artigo, mas não tem que corresponder a um Utilizador.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj instanceof Autor) {
            Autor aux = (Autor) obj;
            return this.m_strEMail.equals(aux.m_strEMail);
        } else {
            return false;
        }
    }

}
