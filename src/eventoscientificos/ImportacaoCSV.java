package eventoscientificos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

/**
 *
 * @author Rita
 */
public class ImportacaoCSV {

    private List<Artigo> listaArtigo;

    private Empresa m_empresa;

    public ImportacaoCSV(Empresa empresa) {
        this.m_empresa = empresa;
    }

    public List<Evento> ImportarEventoCompleto(String caminho) throws FileNotFoundException {
        List<Evento> listaEventos = new ArrayList<Evento>();
        Scanner csv = new Scanner(new BufferedReader(new FileReader(caminho)));
        String InputFile = csv.nextLine();
        while (csv.hasNextLine()) {
            Evento e = new Evento();
            InputFile = csv.nextLine();
            String[] info = InputFile.split(";");
            e.setAno(info[0]);
            e.setTitulo(info[1]);
            e.setIDEvento(info[2]);
            e.setHost(info[3]);
            e.setCidade(info[4]);
            e.setPais(info[5]);
            e.setDataLimiteSubmissao(info[6]);
            e.setDataLimiteRevisao(info[7]);
            e.setDataLimiteSubmissaoFinal(info[8]);
            e.setDataLimiteRegistoAutor(info[9]);
            e.setDataInicio(info[10]);
            e.setDuracao(info[11]);
            e.setWebsite(info[12]);
            String idOrg1 = info[13];
            String email1 = info[14];
            Utilizador u1 = m_empresa.getRegistarUtilizadores().novoUtilizador();
            u1.setEmail(email1);
            u1.setUsername(idOrg1);
            m_empresa.getRegistarUtilizadores().addUtilizador(u1);
            e.addOrganizador(idOrg1, u1);
            String idOrg2 = info[15];
            String email2 = info[16];
            Utilizador u2 = m_empresa.getRegistarUtilizadores().novoUtilizador();
            u2.setUsername(idOrg2);
            u2.setEmail(email2);
            m_empresa.getRegistarUtilizadores().addUtilizador(u2);
            e.addOrganizador(idOrg2, u2);

            listaEventos.add(e);

        }
        for (Evento evento : listaEventos) {
            System.out.println(evento.getIDEvento());
        }
        return listaEventos;

    }

    public List<Artigo> ImportarArtigoCSV(String caminho) throws FileNotFoundException {
        this.listaArtigo = new ArrayList<Artigo>();
        Scanner csv = new Scanner(new BufferedReader(new FileReader(caminho)));
        String InputFile = csv.nextLine();
        while (csv.hasNextLine()) {
            InputFile = csv.nextLine();
            String[] info = InputFile.split(";");
            String idEvento = info[0];
            Evento evento = null;
            
            for (Evento event : m_empresa.getRegistarEventos().getListaEventos()) {
                System.out.println(event.getIDEvento());
                if (event.getIDEvento().equalsIgnoreCase(idEvento));
                evento = event;
            }

            if (evento != null) {
                Artigo art = new Artigo();
                art.setIDArtigo(info[1]);
                art.setTipo(info[2]);
                art.setTitulo(info[3]);
                art.addAutor(art.novoAutor(info[4], info[5], info[6]));
                art.addAutor(art.novoAutor(info[7], info[8], info[9]));
                art.addAutor(art.novoAutor(info[10], info[11], info[12]));
                art.addAutor(art.novoAutor(info[13], info[14], info[15]));
                listaArtigo.add(art);
                evento.addArtigo(art);

            }

        }
        for (Artigo artigo : listaArtigo) {
            System.out.println(artigo.getTitulo());
        }

        return listaArtigo;

    }

    public List<Revisao> ImportarRevisaoCSV(String caminho) throws FileNotFoundException {
        List<Revisao> listaRevisoes = new ArrayList<Revisao>();
        Scanner csv = new Scanner(new BufferedReader(new FileReader(caminho)));
        String InputFile = csv.nextLine();
        while (csv.hasNextLine()) {
            InputFile = csv.nextLine();
            String[] info = InputFile.split(";");
            String idEvento = info[0];
            Evento evento = m_empresa.getRegistarEventos().getEventoID(idEvento);
            if (evento != null) {
                String idArtigo = info[1];
                Artigo art = evento.getArtigoID(idArtigo);
                if (art != null) {
                    Revisao rev = new Revisao();
                    rev.setRevisor(info[2], art);
                    rev.setConfianca(Integer.parseInt(info[3]));
                    rev.setAdequacao(Integer.parseInt(info[4]));
                    rev.setOriginalidade(Integer.parseInt(info[5]));
                    rev.setQualidade(Integer.parseInt(info[6]));
                    String decisao = info[7];
                    if (decisao.equalsIgnoreCase("Accept")) {
                        rev.setRecomendacao("Aceite");
                    } else {
                        rev.setRecomendacao("Rejeitado");
                    }
                    listaRevisoes.add(rev);
                    art.addRevisao(rev);
                }
            }
        }
        return listaRevisoes;
    }

}
