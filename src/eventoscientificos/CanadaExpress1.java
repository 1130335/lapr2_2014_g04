/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import pt.ipp.isep.dei.eapli.canadaexpress.Pedido;
import pt.ipp.isep.dei.eapli.canadaexpress.CanadaExpress;

/**
 *
 * @author 1130429
 */
public class CanadaExpress1 implements ModoPagamento{
    /**
     * Constrói uma instância de  CanadaExpress.
     */
    public CanadaExpress1() {
    }

    /**
     * Executa um pagamento através de uma sistema externo e devolve uma
     * mensagem com informação sobre o pagamento.
     *
     * @param dataValidade data de validade do cartão
     * @param strNumCC número do cartão
     * @param fValorADCC valor 
     * @param dataLimite data limite
     * @return mensagem com informação sobre o pagamento
     * @throws ParseException
     * @throws IllegalArgumentException
     */
    @Override
    public String pagamento(String dataValidade, String strNumCC, float fValorADCC, String dataLimite) throws ParseException {
        CanadaExpress c = new CanadaExpress();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        
        Date dValidade = (Date)formatter.parse(dataValidade);
        Date dLimite = (Date)formatter.parse(dataLimite);
        Pedido pedido = new Pedido(dValidade, strNumCC, fValorADCC, dLimite);
        
        c.Init("#CANADA#EXPRESS#EAPLI#");
        String mensagem = c.ValidaPedido(pedido);
        c.Finish();
        return mensagem;
    }

    /**
     * Devolve uma descrição textual do adaptador de serviço externo.
     */
    @Override
    public String toString() {
        return "Canada Express";
    }

    /**
     * Compara o adaptador com o objeto recebido.
     *
     * @param outroObjeto o objeto a comparar com adaptador
     * @return true se o objeto recebido representar outro adaptador equivalente
     * ao adaptador. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        return !(outroObjeto == null || this.getClass() != outroObjeto.getClass());
    }
}
