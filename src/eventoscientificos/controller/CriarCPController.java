package eventoscientificos.controller;

import eventoscientificos.*;
import java.util.List;

/**
 *
 * @author Nuno Silva
 */
public class CriarCPController {

    private Empresa m_empresa;
    private Evento m_evento;
    private CP m_cp;
    private RegistarEvento m_registarEvento;
    private RegistarUtilizador m_registarUtilizador;

    /**
     * Controi uma instancia de CriarCPController que recebe como parametro a
     * Empresa.
     *
     * @param empresa a empresa
     */
    public CriarCPController(Empresa empresa) {
        m_empresa = empresa;
    }

    /**
     * Devolver a Lista de eventos do Organizador
     *
     * @param strId
     * @return a lista de eventos do organizador
     */
    public List<Evento> getEventosOrganizador(String strId) {
        return m_registarEvento.getListaEventosOrganizador(strId);
    }

    /**
     * Devolve a lista de topicos de evento
     *
     * @return
     */
    public List<Topico> getTopicosEvento() {
        if (m_evento != null) {
            return m_evento.getTopicos();
        } else {
            return null;
        }
    }

    /**
     * Seleciona o Evento para o qual se vai criar a cp
     *
     * @param e
     */
    public void selectEvento(Evento e) {
        m_evento = e;

        m_cp = m_evento.novaCP();
    }

    /**
     * Adiciona um membro de cp ao evento
     *
     * @param strId
     * @return
     */
    public Revisor addMembroCP(String strId) {
        Utilizador u = m_registarUtilizador.getUtilizador(strId);

        if (u != null) {
            return m_cp.addMembroCP(strId, u);
        } else {
            return null;
        }
    }

    /**
     * regista o novo membro da cp
     *
     * @param r
     * @return
     */
    public boolean registaMembroCP(Revisor r) {
        return m_cp.registaMembroCP(r);
    }

    /**
     * modifica a cp
     */
    public void setCP() {
        m_evento.setCP(m_cp);
    }

    /**
     * modifica a Lista de topicos do revisor
     *
     * @param r
     * @param listaTopicosRevisor
     */
    public void setListaTopicosRevisor(Revisor r, List<Topico> listaTopicosRevisor) {
        if (m_cp.getListaRevisores().contains(r)) {
            r.setListaTopicos(listaTopicosRevisor);
        }
    }
}
