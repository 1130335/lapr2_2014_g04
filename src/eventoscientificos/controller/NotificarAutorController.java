/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.*;
import java.io.File;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Sofia
 */
public class NotificarAutorController {
    
    private Empresa m_empresa;
    private Evento evento;
    private String pathfile;
    
/**
 * Cria um construtor de NotificarAutorController que recebe a empresa como parametro
 * @param empresa 
 */
    public NotificarAutorController(Empresa empresa) {
        m_empresa = empresa;
    }

    /**
     * Altera o evento
     * @param evento 
     */
    public void setEvento(Evento evento) {
        this.evento = evento;
    }
    
    /**
     * Altera o file
     * @param file
     */
    public void setFile(String file) {
        this.pathfile = file;
    }
    
    /**
     * Cria uma notificação
     */
    public void criarNotificacao() {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
 
            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("notification_list");
            doc.appendChild(rootElement);
            
            List<Submissao> listSubmissoes = this.evento.getListaSubmissoes();
                    
            for (Submissao s : listSubmissoes) {
                Element submissao = doc.createElement("submission");
                rootElement.appendChild(submissao);
                s.getArtigo().setNotificado(true);
                criarNotificacaoArtigo(s, submissao, doc);
            }
 
            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(this.pathfile));

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);

            transformer.transform(source, result);
	  } catch (ParserConfigurationException pce) {
		pce.printStackTrace();
	  } catch (TransformerException tfe) {
		tfe.printStackTrace();
	  }
    }
    
    /**
     * Cria uma notificação de artigo
     * @param s
     * @param submissao
     * @param doc 
     */
    private void criarNotificacaoArtigo(Submissao s, Element submissao, Document doc) {
            Artigo a = s.getArtigo();
            
            Element paperTitle = doc.createElement("paper_title");
            submissao.appendChild(doc.createTextNode(a.getTitulo()));
            
            Element paperType = doc.createElement("paper_type");
            submissao.appendChild(doc.createTextNode(a.getTipo()));
            
            Element authorName = doc.createElement("author_name");
            submissao.appendChild(doc.createTextNode(a.getAutorCorrespondente().getNome()));
            
            Element authorEmail = doc.createElement("author_email");
            submissao.appendChild(doc.createTextNode(a.getAutorCorrespondente().getUtilizador().getEmail()));
            
            Element decision = doc.createElement("decision");
            submissao.appendChild(doc.createTextNode((a.getDecisao()) ? "Aceite" : "Rejeitado"));
            
            Element comment = doc.createElement("comment");
            submissao.appendChild(comment);
            
            for (int i = 0; i < a.getListaRevisoes().size(); i++) {
                  Revisao m_revisao = a.getListaRevisoes().get(i);
                  Element reviewNr = doc.createElement("review_number");
                  comment.appendChild(doc.createTextNode(String.format("%d", i)));
                  Element reviewComment = doc.createElement("reviewer_comments");
                  comment.appendChild(doc.createTextNode(m_revisao.getJustificacao()));
            }
    }
 }

    
