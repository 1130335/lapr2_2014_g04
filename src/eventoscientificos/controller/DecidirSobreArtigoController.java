/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.*;
import java.util.List;

/**
 *
 * @author Rita
 */
public class DecidirSobreArtigoController {
    
    private Empresa m_empresa;
    private Evento m_evento;
    private MecanismosDecisao m_mecanismo;
    private Decisao m_decisao;
    private Submissao m_submissao;
    private Artigo m_artigo;
    private ImportacaoCSV m_icsv;
    
    /**
     * Construtor de DecidirSobreArtigoController que recebe um objeto como parametro
     * @param empresa empresa criada no inicia da aplicaçao
     */
    public DecidirSobreArtigoController(Empresa empresa) {
        this.m_empresa = empresa;
    }
    
    /**
     * Devolve a lista de eventos do organizador com o id passado como parametro
     * @param orgId id (email) do organizador
     * @return lista de eventos do organizador
     */
    public List<Evento> novaDecisao(String orgId) {
        return m_empresa.getRegistarEventos().getListaEventosOrganizador(orgId);
    }
    
    /**
     * Altera o evento para aquele passado como parâmetro
     * @param e evento selecionado
     */
    public void selectEvento(Evento e) {
        this.m_evento = e;
    }
    
    /**
     * Devolve o evento selecionado
     * @return evento selecionado
     */
    public Evento getEvento() {
        return this.m_evento;
    }
    
    /**
     * Devolve a lista de submissões do evento selecionado
     * @return lista de submissoes
     */
    public List<Submissao> getSubmissoesEvento() {
        return this.m_evento.getListaSubmissoes();
    }
    
    /**
     * Altera a submissao para aquela passada como parametro
     * @param s submissao selecionada
     */
    public void setSubmissao(Submissao s) {
        this.m_submissao = s;
    }
    
    /**
     * Devolve artigo da submissao selecionada
     * @return artigo 
     */
    public Artigo getArtigoSubmissao() {
        this.m_artigo = this.m_submissao.getArtigo();
        return this.m_artigo;
    }
    
    /**
     * Devolve a lista de mecanismos de decisao
     * @return lista de mecanismos de decisao
     */
    public List<MecanismosDecisao> getMecanismosDecisao() {
        return m_empresa.getMecanismosDecisao();
    }
    
    /**
     * Altera o atual mecanismo para aquele passado como parametro
     * @param m mecanismo selecionado
     */
    public void setMecanismo(MecanismosDecisao m) {
        this.m_mecanismo = m;
    }
    
    /**
     * Altera a decisao do artigo da submissao escolhida
     * @param decisao decisao tomada
     */
    public void registaDecisao(Decisao decisao) {
        this.m_artigo.setDecisao(decisao);
    }
    
    /**
     * Devolve a decisao tomada pelo mecanismo de decisao escolhido
     * @param m mecanismo selecionado
     * @param a artigo para o qual a decisao vai ser tomada
     * @return decisao tomada
     */
    public Decisao setMecanismoDecisao(MecanismosDecisao m, Artigo a) {
        return m.decide(new ProcessoDecisao(a));
        
    }
    
}
