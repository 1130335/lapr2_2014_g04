/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.*;
import java.util.List;

/**
 *
 * @author grupo 04
 */
public class SubmeterArtigoController {

    /**
     * a empresa
     */
    private Empresa m_empresa;
    /**
     * o evento
     */
    private Evento m_evento;
    /**
     * a submissao
     */
    private Submissao m_submissao;
    /**
     * o artigo
     */
    private Artigo m_artigo;
    /**
     * o topico
     */
    private Topico m_topico;
    /**
     * objecto do tipo RegistarEvento
     */
    private RegistarEvento m_registarEvento;
    /**
     * Objecto do tipo RegistarUtilizador
     */
    private RegistarUtilizador m_registarUtilizador;

    /**
     * Controi uma instancia de SubmeterArtigoCOntroller recebendo como
     * parametro a empresa.
     *
     * @param empresa
     */
    public SubmeterArtigoController(Empresa empresa) {
        m_empresa = empresa;
    }

    /**
     * Inicia a submissao retornando a lista de eventos para os quais se podem
     * submeter Artigos
     *
     * @return
     */
    public List<Evento> iniciarSubmissao() {
        return this.m_empresa.getRegistarEventos().getListaEventosPodeSubmeter();
    }

    /**
     * modifica o tipo de artigo
     *
     * @param tipo
     */
    public void setTipo(String tipo) {
        m_artigo.setTipo(tipo);
    }

    /**
     * seleciona o evento para o qual se vao submeter artigos
     *
     * @param e
     */
    public void selectEvento(Evento e) {
        m_evento = e;
        this.m_submissao = this.m_evento.novaSubmissao();
        this.m_artigo = this.m_submissao.novoArtigo();
    }

    /**
     * retorna a lista de topicos de evento
     *
     * @return
     */
    public List<Topico> getTopicosEvento() {
        if (m_evento != null) {
            return m_evento.getTopicos();
        } else {
            return null;
        }
    }

    /**
     * modifica os dados do artigo, recebendo-os como parametro
     *
     * @param strTitulo
     * @param strResumo
     * @param strTipo
     * @param strFicheiro
     */
    public void setDados(String strTitulo, String strResumo, String strTipo, String strFicheiro) {
        this.m_artigo.setTitulo(strTitulo);
        this.m_artigo.setResumo(strResumo);
        this.m_artigo.setTipo(strTipo);
        this.m_artigo.setFicheiro(strFicheiro);
    }

    /**
     * seleciona o topico.
     *
     * @param t
     */
    public void selectTopico(Topico t) {

        m_topico = t;

    }

    /**
     * cria um novo autor, retornando-o.
     *
     * @param strNome
     * @param strAfiliacao
     * @return
     */
    public Autor novoAutor(String strNome, String strAfiliacao) {
        return this.m_artigo.novoAutor(strNome, strAfiliacao);
    }

    /**
     * cria um novo autor, retornando-o.
     *
     * @param strNome
     * @param strAfiliacao
     * @param strEmail
     * @return
     */
    public Autor novoAutor(String strNome, String strAfiliacao, String strEmail) {
        return this.m_artigo.novoAutor(strNome, strAfiliacao, strEmail);
    }

    /**
     * adiciona um autor à lista de autores dos artigos
     *
     * @param autor
     * @return
     */
    public boolean addAutor(Autor autor) {
        return this.m_artigo.addAutor(autor);
    }

    /**
     * Retorna os possiveis autores correspondentes do artigo.
     *
     * @return
     */
    public List<Autor> getPossiveisAutoresCorrespondentes() {
        return this.m_artigo.getPossiveisAutoresCorrespondentes();
    }

    /**
     * modifica o autor correspondente
     *
     * @param autor
     */
    public void setCorrespondente(Autor autor) {
        this.m_artigo.setAutorCorrespondente(autor);
    }

    /**
     * modifica o artigo
     *
     * @param strFicheiro
     */
    public void setFicheiro(String strFicheiro) {
        this.m_artigo.setFicheiro(strFicheiro);
    }

    /**
     * devolve o resumo
     *
     * @return
     */
    public String getInfoResumo() {
        return this.m_submissao.getInfo() + this.m_artigo.getInfo();
    }

    /**
     * Regista a submissao
     *
     * @return
     */
    public boolean registarSubmissao() {
        this.m_submissao.setArtigo(m_artigo);
        return this.m_evento.addSubmissao(m_submissao);
    }

    /**
     * Modifica a lista de topicos do artigo.
     *
     * @param listaTopicosArtigo
     */
    public void setListaTopicosArtigo(List<Topico> listaTopicosArtigo) {
        m_artigo.setListaTopicos(listaTopicosArtigo);
    }

}
