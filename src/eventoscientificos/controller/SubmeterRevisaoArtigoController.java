/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.*;
import java.util.List;

/**
 *
 * @author 1130429
 */
public class SubmeterRevisaoArtigoController {

    private Empresa m_empresa;
    private Evento m_evento;
    private Submissao m_submissao;
    private Artigo m_artigo;
    private Revisao m_revisao;
    private RegistarEvento m_registarEvento;
    private RegistarUtilizador m_registarUtilizador;
    private List<Artigo> m_listaArtigos;

    public SubmeterRevisaoArtigoController(Empresa empresa) {
        m_empresa = empresa;
    }

    public List<Evento> iniciarRevisao(String strId) {
        return this.m_registarEvento.getListaEventosRevisor(strId);
    }

    public List<Artigo> iniciarRevisaoArtigo(String strId) {
        Utilizador u = m_registarUtilizador.getUtilizador(strId);
        return this.m_evento.getListaArtigosRevisor(u);
    }

    public void selectEvento(Evento e) {
        m_evento = e;

    }

    public void selectArtigo(Artigo a) {
        m_artigo = a;

    }

    public Evento getEvento(String titulo) {
        return this.m_registarEvento.getEventoID(titulo);
    }

    public void setDados(int GrauConfianca, int GrauAdeq, int GrauOrig, int GrauQual, String strJust) {
        this.m_revisao.setConfianca(GrauConfianca);
        this.m_revisao.setAdequacao(GrauAdeq);
        this.m_revisao.setOriginalidade(GrauOrig);
        this.m_revisao.setQualidade(GrauQual);
        this.m_revisao.setJustificacao(strJust);
    }

    public String getInfoResumo() {
        return this.m_submissao.getInfo() + this.m_artigo.getInfo();
    }

    public Artigo getArtigo(String titulo) {
        return this.m_evento.getArtigoID(titulo);
    }
}
