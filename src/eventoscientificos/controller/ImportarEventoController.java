/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.*;
import java.io.FileNotFoundException;
import java.util.List;

/**
 *
 * @author Vaio
 */
public class ImportarEventoController {
    /**
     * Registar Evento
     */
    private RegistarEvento m_registarEvento;
    /**
     * A empresa
     */
    private Empresa m_empresa;
    /**
     * O evento
     */
    private Evento m_evento;
    /**
     * mportar ficheiro CSV
     */
    private ImportacaoCSV m_importarCSV;
    /**
     * Importar ficheiro XML
     */
    private ImportarXML m_importarXML;
    
    /**
     * Constrói uma instancia de ImportarEventoController
     * @param empresa  a empresa
     */
    
     public ImportarEventoController(Empresa empresa) 
    {
        m_empresa = empresa;
    }
     
      /**
       * Cria um novo evento
       */
      public void novoEvento()
    {
        m_evento = m_registarEvento.novoEvento();
        
    }
      /**
       * Devolve para a lista de eventos o ficheiro CSV importado
       * @param caminho o caminho para o ficheiro
       * @returno evento importado
       * @throws FileNotFoundException 
       */
    public List<Evento> getImportarCSV(String caminho) throws FileNotFoundException{
          return m_importarCSV.ImportarEventoCompleto(caminho);
    }
    /**
     * Devolve para a lista de eventos o ficheiro XML importado.
     * @returno evento importado
     * @throws FileNotFoundException 
     */
    public List<Evento> getImportarXML() throws FileNotFoundException{
        return m_importarXML.ImportarEventoXML();
    }
    
    public void setImportar(MecanismoDistribuicao m){
        m_evento.novoProcessoDistribuicao(m);
    }
      
      
}
