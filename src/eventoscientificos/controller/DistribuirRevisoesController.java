package eventoscientificos.controller;

import eventoscientificos.*;
import java.util.List;


public class DistribuirRevisoesController {
    private Empresa m_empresa;
    private Evento m_e;
    private RegistarEvento m_registarEvento;
    
    public DistribuirRevisoesController(Empresa empresa){
        m_empresa=empresa;
    }
    
    public List<Evento> novaDistribuicao(String orgID){
        return m_registarEvento.getListaEventosOrganizador(orgID);
    }
    
    public void setEvento(Evento e){
        m_e=e;
    }
    
    public List<MecanismoDistribuicao> getMecanismosDistribuicao(){
        return m_empresa.getMecanismosDistribuicao();
    }
    
    public void setMecanismoDistribuicao(MecanismoDistribuicao m){
        m_e.novoProcessoDistribuicao(m);
    }
}
