/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.*;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Vaio
 */
public class DefinirTopicosPericiaController {
    /**
     * O evento
     */
    private Evento m_evento;
    /**
     * Registar Evento
     */
    private RegistarEvento m_registarEvento;
    /**
     * A empresa
     */
    private Empresa m_empresa;
    /**
     * O revisor
     */
    private Revisor m_revisor;
    /**
     * O tópico
     */
    private Topico m_topico;
    
    /**
     * Constroi uma instancia de DefinirTópicosPericiaController
     * @param registarEvento  Registar Evento
     */
     public DefinirTopicosPericiaController(Empresa empresa) {
      m_empresa = empresa;
    }

    
     /**
      * Devolvee a lista de Eventos
      * @return a lista de eventos
      */
     public List<Evento> getListaEventos() {
        return m_empresa.getRegistarEventos().getListaEventos();
    }
/**
 * Seleciona o evento
 * @param e o evento 
 */
    public void selectEvento(Evento e) {
        m_evento = e;
    }
/**
 * Devolve o revisor
 * @param nome o nome do revisor
 */
    public void getRevisor(String nome) {
        for (ListIterator<Revisor> it = m_evento.getCP().getListaRevisores().listIterator(); it.hasNext();) {
            Revisor rev = it.next();
            if (rev.getNome().equalsIgnoreCase(nome)) {
                m_revisor = rev;
            }
        }
    }
    /**
     * Modifica o topico
     * @param t o topico
     */
     public void setTopico(Topico t) {
        this.m_topico = t;
    }
/**
 * Devolve a lista com os topicos atuais
 * @return  lista de topicos
 */
    public List<Topico> getListaTopicosAtuais() {
        return m_revisor.getListaTopicos();
    }
/**
 * Remove o tópico ccomo tópico de pericia
 * @param t o topico
 */
    public void removerTopico(Topico t) {
        m_revisor.removerTopico(t);
    }
/**
 * Devolve a lista de Tópicos do Evento
 * @return os topicos do evento
 */
    public List<Topico> getListaTopicosEvento() {
        return m_evento.getTopicos();
    }
/**
 * Adiciona o tópico como tópico de pericia
 * @param t o tópico
 */
    public void addTopico(Topico t) {
        m_revisor.addTopico(t);
    }
/**
 * 
 * @param lta 
 */
    public void setTopico(List<Topico> lta) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
/**
 * 
 * @param lta
 * @return 
 */
    public boolean addTopico(List<Topico> lta) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  
}
