/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.*;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author 1130429
 */
public class RegistarAutorEventoController {

    /**
     * a empresa
     */
    private Empresa m_empresa;
    /**
     * o evento
     */
    private Evento m_evento;
    /**
     * a submissao
     */
    private Submissao m_submissao;
    /**
     * o artigo
     */
    private Artigo m_artigo;
    /**
     * o topico
     */
    private Topico m_topico;
    /**
     * objecto do tipo RegistarEvento
     */
    private RegistarEvento m_registarEvento;
    /**
     * objecto do tipo RegistarUtilizador
     */
    private RegistarUtilizador m_registarUtilizador;
    /**
     * o modo de pagamento
     */
    private ModoPagamento modopagamento;

    /**
     * constroi um instancia do tipo RegistarAutorEventoController
     *
     * @param empresa
     */
    public RegistarAutorEventoController(Empresa empresa) {
        m_empresa = empresa;
    }

    /**
     * seleciona o evento
     *
     * @param e
     */
    public void selectEvento(Evento e) {
        m_evento = e;

    }

    /**
     * devolve a lista de eventos que o autor submeteu
     *
     * @param id
     * @return
     */

    public List<Evento> getEventosAutorSubmeteu(String id) {
        return m_registarEvento.getEventosAutorSubmeteu(id);
    }

    /**
     * devolve o valor a pagar pelo registo
     *
     * @param id
     * @return
     */
    public float valorPagar(String id) {
        int formula = m_evento.getFormula();
        if (formula == 1) {
            return formula1(id);
        } else {
            return formula2(id);
        }
    }

    /**
     * devolve o valor a pagar calculado pela formula 1
     *
     * @param id
     * @return
     */
    public float formula1(String id) {

        Utilizador u = m_registarUtilizador.getUtilizador(id);

        List<Artigo> lArtigos = m_evento.getListaArtigosAutor(u);
        int nS = 0;
        int nF = 0;
        int nP = 0;

        for (Iterator<Artigo> it = lArtigos.listIterator(); it.hasNext();) {
            Artigo a = it.next();
            String tipo = a.getTipo();
            if (tipo.equalsIgnoreCase("Short")) {
                nS++;
            } else if (tipo.equalsIgnoreCase("full")) {
                nF++;
            } else {
                nP++;
            }
        }
        double vP = Double.parseDouble(m_evento.getValorPoster());
        double vF = Double.parseDouble(m_evento.getValorFull());
        double vS = Double.parseDouble(m_evento.getValorShort());
        return (float) (vP * nP + vF * nF + vS * nS);

    }

    /**
     * devolve o valor a pagar calculado pela formula 2
     *
     * @param id
     * @return
     */
    public float formula2(String id) {
        Utilizador u = m_registarUtilizador.getUtilizador(id);
        List<Artigo> lArtigos = m_evento.getListaArtigosAutor(u);
        int nS = 0;
        int nF = 0;
        int nP = 0;

        for (Iterator<Artigo> it = lArtigos.listIterator(); it.hasNext();) {
            Artigo a = it.next();
            String tipo = a.getTipo();
            if (tipo.equalsIgnoreCase("Short")) {
                nS++;
            } else if (tipo.equalsIgnoreCase("full")) {
                nF++;
            } else {
                nP++;
            }
        }
        double vP = Double.parseDouble(m_evento.getValorPoster());
        double vF = Double.parseDouble(m_evento.getValorFull());
        double vS = Double.parseDouble(m_evento.getValorShort());

        if (nS > 0 && nP > 0) {
            return (float) (nF * vF + vS * (nS - (nF / 2)) + vP * (nP - (nF / 2)));
        } else if (nS == 0 && nP > 0) {
            return (float) (nF * vF + vP * (nP - (nF / 2)));
        } else {
            return (float) (nF * vF + vS * (nS - nF / 2));
        }

    }

    /**
     * seleciona o servico atraves do qual se vai fazer o pagamento
     *
     * @param n
     */
    public void selecionaServico(int n) {
        if (n == 1) {
            this.modopagamento = new CanadaExpress1();
        } else {
            this.modopagamento = new VisaoLight();
        }

    }

    /**
     * retorna o estado do pagamento(aceite, rejeitado)
     *
     * @param dataValidade
     * @param strNumCC
     * @param fValorADCC
     * @param dataLimite
     * @return
     * @throws ParseException
     */
    public String pagamento(String dataValidade, String strNumCC,
            float fValorADCC, String dataLimite) throws ParseException {
        return this.modopagamento.pagamento(dataValidade, strNumCC, fValorADCC,
                dataLimite);
    }
}
