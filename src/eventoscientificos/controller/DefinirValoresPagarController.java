/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.*;
import java.util.List;

/**
 *
 * @author 1130429
 */
public class DefinirValoresPagarController {

    /**
     * a empresa
     */
    private Empresa m_empresa;
    /**
     * o evento
     */
    private Evento m_evento;
    /**
     * objecto do tipo registarEvento
     */
    private RegistarEvento m_registarEvento;

    /**
     * controi uma instancia do tipo DefinirValoresPagarController
     *
     * @param empresa
     */
    public DefinirValoresPagarController(Empresa empresa) {
        m_empresa = empresa;
    }

    /**
     * Retorna a lista de eventos do organizador
     *
     * @param strId
     * @return
     */
    public List<Evento> iniciarDefinirValores(String strId) {

        return this.m_registarEvento.getListaEventosOrganizador(strId);

    }

    /**
     * seleciona o evento para os quais se vao definir os valores a pagar
     *
     * @param e
     */
    public void selectEvento(Evento e) {
        m_evento = e;
    }

    /**
     * modifica o valor de um artigo do tipo Full
     *
     * @param F
     */
    public void setValorFull(String F) {
        this.m_evento.setValorFull(F);

    }

    /**
     * modifica o valor de um artigo do tipo short
     */
    public void setValorShort(String S) {
        this.m_evento.setValorShort(S);
    }

    /**
     * modifica o valor de um artigo do tipo poster
     *
     * @param P
     */
    public void setValorPoster(String P) {
        this.m_evento.setValorPoster(P);
    }

    /**
     * modifica a formula de pagamento selecionada
     *
     * @param formula
     */
    public void setFormula(String formula) {
        this.m_evento.setFormula(formula);
    }

    /**
     * retorna o evento
     *
     * @param titulo
     * @return
     */
    public Evento getEvento(String titulo) {
        return this.m_registarEvento.getEventoID(titulo);
    }
}
