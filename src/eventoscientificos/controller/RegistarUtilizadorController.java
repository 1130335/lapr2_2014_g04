package eventoscientificos.controller;

import eventoscientificos.*;


/**
 *
 * @author Nuno Silva
 */

public class RegistarUtilizadorController
{
    
    private Empresa m_empresa;
    private Utilizador m_utilizador;
    private RegistarUtilizador m_registarUtilizador;
    private RegistarUtilizador m_listaUtilizadores;

    /**
     * Constroi um construtor de RegistarUtilizadorController recebendo Empresa como parâmetro
     * @param empresa 
     */
    public RegistarUtilizadorController(Empresa empresa)
    {
        m_empresa = empresa;
    }

    /**
     * Cria um novo Utilizador
     */
    public Utilizador novoUtilizador()
    {

      return  m_utilizador = m_empresa.getRegistarUtilizadores().novoUtilizador();

    }


    public Utilizador setDados(String username, String password, String nome, String email) {
     m_utilizador.setUsername(username);
        m_utilizador.setPassword(password);
        m_utilizador.setNome(nome);
        m_utilizador.setEmail(email);
        
          if( m_empresa.getRegistarUtilizadores().registaUtilizador(m_utilizador) )
            return m_utilizador;
        else
            return null;
    }
}

