package eventoscientificos;


import java.util.*;

/**
 *
 * @author Nuno Silva
 */

public class CP
{
    List<Revisor> m_listaRevisores;

    /**
     * Construtor de cp sem parametros
     */
    public CP()
    {
        m_listaRevisores = new ArrayList();
    }

    /**
     * Adiciona um membro da cp 
     * @param strId id do revisor
     * @param u utilizador
     * @return revisor
     */
    public Revisor addMembroCP( String strId, Utilizador u )
    {
        Revisor r = new Revisor(u);
        
        if( r.valida() && validaMembroCP(r) )
            return r;
        else
            return null;
    }
    
    /*
    * Valida o membro da cp
    */
    
    private boolean validaMembroCP(Revisor r)
    {
        return true;
    }
    
    /**
     * Regista o membro da cp
     * @param r revisor
     * @return true se o revisor for adicionado à lista
     */
    public boolean registaMembroCP(Revisor r)
    {
        return m_listaRevisores.add(r);
    }
    
    /**
     * Devolve a forma textual da cp
     * @return texto sobre cp
     */
    @Override
    public String toString()
    {
        String strCP = "Membros de CP: ";
        for( ListIterator<Revisor> it = m_listaRevisores.listIterator(); it.hasNext(); )
        {
            strCP += it.next().toString();
            if( it.hasNext() )
                strCP += ", ";
        }
        return strCP;
    }

    /**
     * Devolve a lista de revisores da cp
     * @return lista de revisores
     */
    public List<Revisor> getListaRevisores() 
    {
        return this.m_listaRevisores;
    }
}