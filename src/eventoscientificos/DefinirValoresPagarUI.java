/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import eventoscientificos.controller.DefinirValoresPagarController;
import java.util.List;
import utils.Utils;

/**
 *
 * @author 1130429
 */
public class DefinirValoresPagarUI {

    private Empresa m_empresa;
    private DefinirValoresPagarController m_controllerDVP;

    public DefinirValoresPagarUI(Empresa empresa) {
        m_empresa = empresa;
        m_controllerDVP = new DefinirValoresPagarController(m_empresa);
    }

    // alterado na iteração 2
    public void run() {
        String strId = introduzIdRevisor();
        List<Evento> le = m_controllerDVP.iniciarDefinirValores(strId);

        apresentaEventos(le);

        Evento e = selecionaEvento(le);

        if (e != null) {
            m_controllerDVP.selectEvento(e);
            
            String F = Utils.readLineFromConsole("Introduza o valor a pagar por um artigo do formato Full :");
            String P = Utils.readLineFromConsole("Introduza o valor a pagar por um artigo do formato Full :");
            String S = Utils.readLineFromConsole("Introduza o valor a pagar por um artigo do formato Full  :");
            String formula = Utils.readLineFromConsole("Introduza a formula desejada : (1/2)");
            m_controllerDVP.setValorFull(F);
            m_controllerDVP.setValorShort(S);
            m_controllerDVP.setValorPoster(P);
            m_controllerDVP.setFormula(formula);

            
            System.out.println("Definição de valores de artigo concluida com sucesso.");

            System.out.println("Terminado.");
        }

    }
    private String introduzIdRevisor() {
        return Utils.readLineFromConsole("Introduza Id Revisor: ");
    }

    private void apresentaEventos(List<Evento> le) {
        System.out.println("Eventos: ");

        int index = 0;
        for (Evento e : le) {
            index++;

            System.out.println(index + ". " + e.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }

    private Evento selecionaEvento(List<Evento> le) {
        String opcao;
        int nOpcao;
        do {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        } while (nOpcao < 0 || nOpcao > le.size());

        if (nOpcao == 0) {
            return null;
        } else {
            return le.get(nOpcao - 1);
        }

    }
}