package eventoscientificos;

import eventoscientificos.Artigo;
import eventoscientificos.Revisor;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rita
 */
public class Distribuicao {
    private List<Revisor> m_listaRevisores;
    private Artigo m_art;
    
    /**
     * Construtor de distribuicao sem parametros
     */
    public Distribuicao()
    {
        m_listaRevisores = new ArrayList();
        m_art= new Artigo();
    }
    
    /**
     * Construtor de distribuicao com 2 parametros
     * @param listaRevisores lista de revisores
     * @param art artigo
     */
    public Distribuicao(List<Revisor> listaRevisores, Artigo art)
    {
        m_listaRevisores = listaRevisores;
        m_art= art;
    }
    
    /**
     * Altera o artigo da distribuicao
     * @param art artigo
     */
    public void setArtigo(Artigo art){
        m_art=art;
    }
    
    /**
     * Altera a lista de revisores da distribuicao
     * @param revs lista de revisores
     */
    public void setRevisores(List<Revisor> revs){
        m_listaRevisores= revs;
    }
}
