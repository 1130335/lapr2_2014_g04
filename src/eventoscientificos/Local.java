/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

/**
 *
 * @author Nuno Silva
 */

public class Local
{
    private String m_strLocal;

    /**
     * Cria um construtor vazio de local
     */
    public Local()
    {
    }
    
    /**
     * Altera o local
     * @param strLocal 
     */
    public void setLocal(String strLocal)
    {
        m_strLocal = strLocal;
    }

    /**
     * Apresenta textualmente o local
     * @return 
     */
    @Override
    public String toString()
    {
        return m_strLocal;
    }
}
