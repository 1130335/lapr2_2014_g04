/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

/**
 *
 * @author Sofia
 */
public class EventoRegistadoState implements EventoState {
    
    private Evento e1;
   
    /**
     * Construtor de eventoRegistadoState
     * @param e 
     */
    public EventoRegistadoState(Evento e){
        e1= e;
    }
    /**
     * Altera o estado de registado do evento
     * @param e
     * @return true caso seja registado
     */
    public boolean setRegistado (Evento e) {
        return true;
    }

    /**
     * Valida o estado do evento
     * @return 
     */
    public boolean valida() {
       return true;
    }

    @Override
    public boolean setCameraReady(Evento e) {
       return false;
    }

    @Override
    public boolean setRevisto() {
        return false;
    }
    
}
