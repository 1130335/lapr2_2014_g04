/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.util.List;

/**
 *
 * @author Rita
 */
public class Revisao {

    private Revisor m_revisor;
    private int m_confianca;
    private int m_adequacao;
    private int m_originalidade;
    private int m_qualidade;
    private boolean m_recomendacao;
    private String m_justificacao;

    /**
     *
     */
    public Revisao() {
        this.m_adequacao = 0;
        this.m_confianca = 0;
        this.m_originalidade = 0;
        this.m_qualidade = 0;
        this.m_justificacao = "sem justificacao";
        this.m_recomendacao = false;
    }

    /**
     *
     * @return
     */
    public int getConfianca() {
        return m_confianca;
    }

    /**
     *
     * @param GrauConfianca
     */
    public void setConfianca(int GrauConfianca) {
        this.m_confianca = GrauConfianca;
    }

    /**
     *
     * @return
     */
    public int getAdequacao() {
        return m_adequacao;
    }

    /**
     *
     * @param GrauAdeq
     */
    public void setAdequacao(int GrauAdeq) {
        this.m_adequacao = GrauAdeq;
    }

    /**
     *
     * @return
     */
    public int getOriginalidade() {
        return m_originalidade;
    }

    /**
     *
     * @param GrauOrig
     */
    public void setOriginalidade(int GrauOrig) {
        this.m_originalidade = GrauOrig;
    }

    /**
     *
     * @return
     */
    public int getQualidade() {
        return m_qualidade;
    }

    /**
     *
     * @param GrauQual
     */
    public void setQualidade(int GrauQual) {
        this.m_qualidade = GrauQual;
    }

    /**
     * Devolve a recomendação feita pelo revisor, como true ou false
     * @return recomendaçao de aceitaçao do artigo
     */
    public boolean getRecomendacao() {
        return m_recomendacao;
    }

    /**
     *
     * @param AceitRej
     */
    public void setRecomendacao(String AceitRej) {
        if (AceitRej.equalsIgnoreCase("Aceite")) {
            this.m_recomendacao = true;
        } else {
            this.m_recomendacao = false;
        }
    }

    /**
     *
     * @return
     */
    public String getJustificacao() {
        return m_justificacao;
    }

    /**
     *
     * @param Just
     */
    public void setJustificacao(String Just) {
        this.m_justificacao = Just;
    }

    /**
     *
     * @return
     */
    public Revisor getRevisor() {
        return this.m_revisor;
    }

    /**
     *
     * @param email
     * @param art
     */
    public void setRevisor(String email, Artigo art) {
        List<Revisor> revisores = art.getListaRevisores();
        for (Revisor r : revisores) {
            if (r.getUtilizador().getEmail().equalsIgnoreCase(email)) {
                this.m_revisor = r;
            }
        }

    }

}
