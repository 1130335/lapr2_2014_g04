package eventoscientificos;

import java.util.*;

/**
 *
 * @author Nuno Silva
 */
public class Empresa {

    private RegistarUtilizador m_listaUtilizadores;
    private RegistarEvento m_listaEventos;
    private List<MecanismosDecisao> m_listaMecanismos;
    private RegistarEvento m_registarEvento;

    /**
     * Constroi um construtor vazio de empresa
     */
    public Empresa() {
        m_listaUtilizadores = new RegistarUtilizador();
        m_listaEventos = new RegistarEvento(this);

//        fillInData();
    }

    /**
     * Altera a lista de utilizadores
     *
     * @param m_registarUtilizador
     */
    public void setRegistoUtilizadores(RegistarUtilizador m_registarUtilizador) {
        this.m_listaUtilizadores = m_registarUtilizador;
    }

    /**
     * Consulta a lista de utilizadores
     *
     * @return
     */
    public RegistarUtilizador getRegistarUtilizadores() {
        return this.m_listaUtilizadores;
    }

    /**
     * Altera a lista de eventos
     *
     * @param m_registarEvento
     */
    public void setRegistoEventos(RegistarEvento m_registarEvento) {
        this.m_listaEventos = m_registarEvento;
    }

    /**
     * Consulta a lista de eventos
     *
     * @return lista de eventos
     */
    public RegistarEvento getRegistarEventos() {
        return this.m_listaEventos;
    }

//    private void fillInData() {
//        int max_users = 50;
//        int max_organizadores = 5;
//        int max_revisores = 25;
//        int max_submissoes = 10;
//        for (int users = 0; users < max_users; users++) {
//            String id = "user" + users;
//            String ds = "Utilizador " + users;
//
//            Utilizador u = new Utilizador(id, "12345", ds, id + "@xxx.pt");
//            this.m_registarUtilizador.registaUtilizador(u);
//
//             System.out.println( u );
//        }
//
//        Evento e1 = new Evento("ESOFT Workshop", "Workshop sobre Engenharia de Software");
//        this.m_listaEventos.add(e1);
//
//        System.out.println("Organizadores de evento1");
//        for (int i = 0; i < max_organizadores; i++) {
//            Utilizador utl = this.m_listaUtilizadores.getListaUtilizadores().get(i);
//            e1.addOrganizador(utl.getUsername(), utl);
//
//            System.out.println(utl.toString());
//        }
//        CP cp = e1.novaCP();
//        for (int i = 0; i < max_revisores; i++) {
//            Utilizador utl = this.m_listaUtilizadores.getListaUtilizadores().get(i);
//            Revisor r = cp.addMembroCP(utl.getUsername(), utl);
//            cp.registaMembroCP(r);
//        }
//        e1.setCP(cp);
//
//        for (int submissoes = 0; submissoes < max_submissoes; submissoes++) {
//            Submissao sub = e1.novaSubmissao();
//            Artigo art = sub.novoArtigo();
//
//            art.setTitulo("Artigo " + submissoes);
//            sub.setArtigo(art);
//            e1.addSubmissao(sub);
//        }
//    }
    /**
     * Devolve a lista de mecanismos de distribuicao guardados pela empresa
     * @return lista de mecanismos de distribuicao
     */
    public List<MecanismoDistribuicao> getMecanismosDistribuicao() {
        List<MecanismoDistribuicao> lm = new ArrayList();
        MecanismoDistribuicao1 md1 = new MecanismoDistribuicao1();
        MecanismoDistribuicao2 md2 = new MecanismoDistribuicao2();
        lm.add(md1);
        lm.add(md2);
        return lm;
    }

    /**
     * Devolve a lista de mecanismos de decisao guardados pela Empresa
     *
     * @return lista de mecanismos de decisao
     */
    public List<MecanismosDecisao> getMecanismosDecisao() {
        List<MecanismosDecisao> lmd = new ArrayList<>();
        MecanismosDecisao md = new MecanismosDecisao();
        lmd.add(md);
        return lmd;
    }

    /**
     *
     * @return
     */
    public List<Importar> getImportar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
