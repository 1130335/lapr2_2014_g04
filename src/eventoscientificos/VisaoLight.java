/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

/**
 *
 * @author 1130429
 */
public class VisaoLight implements ModoPagamento{
   /**
     * Constrói uma instância de VisaoLightAdapter.
     */
    public VisaoLight() {}

    /**
     * Executa um pagamento através de uma sistema externo e devolve uma
     * mensagem com informação sobre o pagamento.
     *
     * @param dataValidade data de validade
     * @param strNumCC número do cartão
     * @param fValorADCC valor 
     * @param dataLimite data limite
     * @return mensagem com informação sobre o pagamento
     */
    @Override
    public String pagamento(String dataValidade, String strNumCC, float fValorADCC, String dataLimite) {
        return pt.ipp.isep.dei.eapli.visaolight.VisaoLight.getAutorizacaoDCC(strNumCC, dataValidade, fValorADCC, dataLimite);
    }

    /**
     * Devolve uma descrição textual do adaptador de serviço externo.
     */
    @Override
    public String toString() {
        return "Visão Light";
    }

    /**
     * Compara o adaptador com o objeto recebido.
     *
     * @param outroObjeto o objeto a comparar com adaptador
     * @return true se o objeto recebido representar outro adaptador equivalente
     * ao adaptador. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        return !(outroObjeto == null || this.getClass() != outroObjeto.getClass());
    }
}
